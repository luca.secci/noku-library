import "../src/all.scss";
import "./main.scss";
import i18nInit from "../src/i18n";
import { library } from "@fortawesome/fontawesome-svg-core";

// SOLID
import { faWallet, faUser, faSortDown, faUserCircle, faSignOutAlt, faTimes, faChevronDown, faFolder, faCartPlus } from "@fortawesome/free-solid-svg-icons";

i18nInit();
library.add(
  // SOLID
  faWallet,
  faUser,
  faSortDown,
  faUserCircle,
  faSignOutAlt,
  faTimes,
  faChevronDown,
  faFolder,
  faCartPlus
);
