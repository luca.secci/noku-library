# TODOs

Add here bugs and features to add to the library

Dynamic socket.io-client import in chainer.js

* Some chainer calls cannot be aborted:

  * getNFTInfo, getNFTImage, getNFTName, getNFTDescription

  * getTransaction

  * estimateGas

* Add props comments/explanation in the stories

* Add code block examples in the stories

* The theming/customization could be done better without the mixins (this is compatible ONLY with the official `Dart Sass` implementation), see the example below

```scss
// _library.scss
$black: #000 !default;
$border-radius: 0.25rem !default;
$box-shadow: 0 0.5rem 1rem rgba($black, 0.15) !default;

code {
  border-radius: $border-radius;
  box-shadow: $box-shadow;
}
```

```scss
// style.scss
@use 'library' with (
  $black: #222,
  $border-radius: 0.1rem
);
```
