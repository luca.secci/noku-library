module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: [
    "react-app",
    'plugin:react/recommended',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 11,
    sourceType: 'module'
  },
  plugins: [
    'react'
  ],
  rules: {
    'react/prop-types': [0],
    'react/display-name': [0],
    semi: [1, 'always']
  }
};
