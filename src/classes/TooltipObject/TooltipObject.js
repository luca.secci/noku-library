export class TooltipObject {
  /**
    * TooltipObject builder, used in SendFundsForm -> GasPrice
    * @param {number} estimated Estimated time or bytes to execute a transaction
    * @param {{currency: BigNumber}[]} needMulti Needed fee/gas converted in multiple currencies, including cryptocurrency, to execute a transaction
    */
  constructor (estimated, needMulti) {
    this.estimated = estimated;
    this.needMulti = needMulti;
  }
}
