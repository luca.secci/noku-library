/**
 * All available chains
 * @typedef {(
 *  "BCH"|"BCHTEST"
 *  "BTC"|"BTCTEST"|
 *  "DASH"|"DASHTEST"|
 *  "LTC"|"LTCTEST"|
 *  "ETH"|"ROPSTEN"|
 *  "NOKU_CHAIN"|"NOKU_CHAIN2"|
 *  "NOKU_CHAINTEST"|"NOKU_CHAIN2TEST"|
 *  "WAVES"|"WAVESTEST"|
 *  "ZEC"|"ZECTEST"|
 *  "XMR"
 * )} AvailableChains
 */
