import {blockies} from 'eth4you';

// Unsupported blockie background images
import BCHBlockie from '../../assets/images/staticBlockie/BCH.svg';
import BTCBlockie from '../../assets/images/staticBlockie/BTC.svg';
import BTCTESTBlockie from '../../assets/images/staticBlockie/BTCTEST.svg';
import DASHBlockie from '../../assets/images/staticBlockie/DASH.svg';
import DASHTESTBlockie from '../../assets/images/staticBlockie/DASHTEST.svg';
import LTCBlockie from '../../assets/images/staticBlockie/LTC.svg';
import LTCTESTBlockie from '../../assets/images/staticBlockie/LTCTEST.svg';
import ZECBlockie from '../../assets/images/staticBlockie/ZEC.svg';
import ZECTESTBlockie from '../../assets/images/staticBlockie/ZECTEST.svg';

const emptyAvatar = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAQAAAC0NkA6AAAALUlEQVR42u3NMQEAAAgDoK2/oc3g4QcFaCbvKpFIJBKJRCKRSCQSiUQikUhuFvwRG4t8xwv8AAAAAElFTkSuQmCC';

/**
 *
 * @param {string} address
 * @returns
 */
export const createBlockie = (address) => blockies.create({
  seed: address,
  size: 8,
  scale: 5
}).toDataURL();

/**
 *
 * @param {AvailableChains} chain
 * @param {string} address
 * @return {string}
 */
export const getBlockie = (chain, address) => {
  if (!chain || !address) {
    return emptyAvatar;
  }

  switch (chain) {
    // ETH-LIKE AND WAVES CHAINS
    case 'ETH':
    case 'ROPSTEN':
    case 'NOKU_CHAIN':
    case 'NOKU_CHAIN2':
    case 'NOKU_CHAINTEST':
    case 'NOKU_CHAIN2TEST':
    case 'WAVES':
    case 'WAVESTEST':
    case 'BSC':
    case 'BSC_TEST':
      return createBlockie(address);
    case 'BCH':
    case 'BCHTEST':
      return BCHBlockie;
    case 'BTC':
      return BTCBlockie;
    case 'BTCTEST':
      return BTCTESTBlockie;
    case 'DASH':
      return DASHBlockie;
    case 'DASHTEST':
      return DASHTESTBlockie;
    case 'LTC':
      return LTCBlockie;
    case 'LTCTEST':
      return LTCTESTBlockie;
    case 'ZEC':
      return ZECBlockie;
    case 'ZECTEST':
      return ZECTESTBlockie;
    default:
      break;
  }
};
