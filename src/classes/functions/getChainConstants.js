import {
  BCHChainInfo,
  BCHTestChainInfo,
  BTCChainInfo,
  BTCTestChainInfo,
  DashChainInfo,
  DashTestChainInfo,
  ETHChainInfo,
  LTCChainInfo,
  LTCTestChainInfo,
  NokuChain2ChainInfo,
  NokuChain2TestChainInfo,
  NokuChainChainInfo,
  NokuChainTestChainInfo,
  RopstenChainInfo,
  WavesChainInfo,
  WavesTestChainInfo,
  ZECChainInfo,
  ZECTestChainInfo,
  BscChainInfo,
  BscTestChainInfo
} from "./getChainInfo";

// import MetamaskLogo from "../../assets/images/vendors/metamask.svg";
// import LedgerLogo from "../../assets/images/vendors/ledger.svg";

// ETH LIKE
/**
 * @enum {string}
 */
export const UnlockMethods = {
  // Supported external signers
  Metamask: "metamask",
  Ledger: "ledger",
  // Other methods
  PrivateKey: "pk",
  MnemonicPhrase: "phrase",
  JsonFile: "json"
};

const ETHUnlockMethods = [
  {
    method: UnlockMethods.Metamask,
    image: "https://static.noku.io/assets/noku/providers/metamask.svg",
    isExternalProvider: true,
  },
  {
    method: UnlockMethods.Ledger,
    image: "https://static.noku.io/assets/noku/providers/ledger.svg",
    isExternalProvider: true,
  },
  {
    method: UnlockMethods.PrivateKey,
  },
  // {
  //   text: "Encrypted Private Key",
  //   method: "encryptedPk"
  // },
  {
    method: UnlockMethods.MnemonicPhrase,
  },
  {
    method: UnlockMethods.JsonFile,
  },
];

export const ETHConstants = {
  chainInfo: ETHChainInfo,
  externalProviderSupported: true,
  erc721TopTokens: {
    "0x06012c8cf97bead5deae237070f9587f8e7a266d": "CryptoKitties",
    "0x6ebeaf8e8e946f0716e6533a6f2cefc83f60e8ab": "Gods Unchained",
    "0xbd13e53255ef917da7557db1b7d2d5c38a2efe24": "DozerDoll",
    "0x8c9b261faef3b3c2e64ab5e58e04615f8c788099": "LucidSight-MLB-NFT",
    "0x1d963688fe2209a98db35c67a041524822cf04ff": "MARBLE-NFT",
    "0xdceaf1652a131f32a821468dc03a92df0edd86ea": "MyCryptoHeroes:Extension",
    "0x273f7f8e6489682df756151f5525576e322d51a3": "MyCryptoHeroes:Hero",
    "0x617913dd43dbdf4236b85ec7bdf9adfd7e35b340": "MyCryptoHeroes:Land",
    "0x67cbbb366a51fff9ad869d027e496ba49f5f6d55": "CryptoSpells",
    "0x2aea4add166ebf38b63d09a75de1a7b94aa24163": "KudosToken",
    "0x4f41d10f7e67fd16bde916b4a6dc3dd101c57394": "Flower",
    "0x960f401aed58668ef476ef02b2a2d43b83c261d8": "Dragonereum Dragon",
    "0x5caebd3b32e210e85ce3e9d51638b9c445481567": "WarRiders",
    "0x2f2d5aa0efdb9ca3c9bb789693d06bebea88792f": "BlockCities",
    "0xd9cf3f6ddca851887f3424a9630edfbf23d6da94": "NokuCryptoHeroes",
  },
  unlockMethods: [...ETHUnlockMethods],
  nokuTokenAddress: "0x1fc52f1abade452dd4674477d4711951700b3d27",
  eurnTokenAddress: "0x4e8d979271cc2739d2bbd13b291faa3eb6df1504",
  chfnTokenAddress: "0x9b54153d20041cb5d39d754c793330df8233558d",
  cryptoHeroesAddress: "0xd9cf3f6ddca851887f3424a9630edfbf23d6da94",
};

export const RopstenConstants = {
  chainInfo: RopstenChainInfo,
  externalProviderSupported: true,
  nokuTokenAddress: "0xefe639c2841ac016d21b63c07d00dc11ec9d3572",
  eurnTokenAddress: "0xa066e99d89beac271fcf5010758a8a654d9b1f67",
};

export const NokuChainConstants = {
  chainInfo: NokuChainChainInfo,
  externalProviderSupported: false,
  customChain: {
    name: "nokuchain",
  },
  unlockMethods: ETHUnlockMethods.filter(({ isExternalProvider }) => !isExternalProvider),
};

export const NokuChainTestConstants = {
  chainInfo: NokuChainTestChainInfo,
  externalProviderSupported: false,
  customChain: {
    name: "nokuchaintest",
  },
  unlockMethods: ETHUnlockMethods.filter(({ isExternalProvider }) => !isExternalProvider),
  // unlockMethods: ETHUnlockMethods.filter(({method}) => method !== UnlockMethods.Metamask && method !== UnlockMethods.Ledger)
};

export const BscConstants = {
  chainInfo: BscChainInfo,
  customChain: {
    name: "bsc-mainnet",
  },
  unlockMethods: ETHUnlockMethods.filter(({ method }) => method !== UnlockMethods.Metamask && method !== UnlockMethods.Ledger)
};

export const BscTestConstants = {
  chainInfo: BscTestChainInfo,
  customChain: {
    name: "bsc-testnet",
  },
};

export const NokuChain2Constants = {
  chainInfo: NokuChain2ChainInfo,
  externalProviderSupported: false,
  cryptoHeroesAddress: "0xfb574de3c071ac675a956a1d9d295aa2a04303ba",
  customChain: {
    name: "nokuchain2",
  },
  erc721TopTokens: {
    "0xfb574de3c071ac675a956a1d9d295aa2a04303ba": "NokuCryptoHeroes",
  },
  unlockMethods: ETHUnlockMethods.filter(({ isExternalProvider }) => !isExternalProvider),
  // unlockMethods: ETHUnlockMethods.filter(({method}) => method !== UnlockMethods.Metamask && method !== UnlockMethods.Ledger)
};

export const NokuChain2TestConstants = {
  chainInfo: NokuChain2TestChainInfo,
  externalProviderSupported: false,
  cryptoHeroesAddress: '0x9e82e35c5ff9ae8ed4b71d3573dc10cad5299dd9',
  customChain: {
    name: 'nokuchain2test'
  },
  unlockMethods: ETHUnlockMethods.filter(({ isExternalProvider }) => !isExternalProvider),
  // unlockMethods: ETHUnlockMethods.filter(({method}) => method !== UnlockMethods.Metamask && method !== UnlockMethods.Ledger)
};

// BTC LIKE

export const BTCConstants = {
  chainInfo: BTCChainInfo,
  unlockMethods: [
    {
      text: 'Private Key',
      method: 'pk'
    },
    {
      text: 'Mnemonic Phrase / Seed',
      method: 'seed'
    },
    {
      text: 'UTC / JSON File',
      method: 'json'
    }
  ],
  walletTypes: [
    { value: 'p2wpkh', label: 'p2wpkh', purpose: '84' }, // the first one, is the default one!
    { value: 'p2wpkh-p2sh', label: 'p2wpkh-p2sh', purpose: '49' },
    { value: 'p2pkh', label: 'p2pkh', purpose: '44' }
  ]
};

export const BTCTestConstants = {
  chainInfo: BTCTestChainInfo
};

export const BCHConstants = {
  chainInfo: BCHChainInfo,
  walletTypes: [
    // { value: "p2wpkh", label: "p2wpkh", purpose: "84" }, //the first one, is the default one!
    // { value: "p2wpkh-p2sh", label: "p2wpkh-p2sh", purpose: "49" },
    { value: 'p2pkh', label: 'p2pkh', purpose: '44' }
  ]
};

export const BCHTestConstants = {
  chainInfo: BCHTestChainInfo,
};

export const DashConstants = {
  chainInfo: DashChainInfo,
  walletTypes: [
    // { value: "p2wpkh", label: "p2wpkh", purpose: "84" }, //the first one, is the default one!
    // { value: "p2wpkh-p2sh", label: "p2wpkh-p2sh", purpose: "49" },
    { value: 'p2pkh', label: 'p2pkh', purpose: '44' }
  ]
};

export const DashTestConstants = {
  chainInfo: DashTestChainInfo
};

export const LTCConstants = {
  chainInfo: LTCChainInfo,
  walletTypes: [
    { value: 'p2wpkh', label: 'p2wpkh', purpose: '84' }, // the first one, is the default one!
    { value: 'p2wpkh-p2sh', label: 'p2wpkh-p2sh', purpose: '49' },
    { value: 'p2pkh', label: 'p2pkh', purpose: '44' }
  ]
};

export const LTCTestConstants = {
  chainInfo: LTCTestChainInfo,
  walletTypes: [
    { value: 'p2pkh', label: 'p2pkh', purpose: '44' }
  ]
};

export const ZECConstants = {
  chainInfo: ZECChainInfo,
  walletTypes: [
    // { value: "p2wpkh", label: "p2wpkh", purpose: "84" }, //the first one, is the default one!
    // { value: "p2wpkh-p2sh", label: "p2wpkh-p2sh", purpose: "49" },
    { value: 'p2pkh', label: 'p2pkh', purpose: '44' }
  ]
};

export const ZECTestConstants = {
  chainInfo: ZECTestChainInfo
};

// WAVES
export const WavesConstants = {
  chainInfo: WavesChainInfo,
  unlockMethods: [
    {
      text: 'Private Key',
      method: 'pk'
    }, {
      text: 'Mnemonic Phrase / Seed',
      method: 'waves_seed'
    },
    {
      text: 'UTC / JSON File',
      method: 'json'
    }
  ]
};

export const WavesTestConstants = {
  chainInfo: WavesTestChainInfo
};

/**
 * Gets the chain constants object without creating a chainer
 * @param {AvailableChains} chain Chain name
 */
export const getChainConstants = (chain = "") => {
  switch (chain?.toUpperCase?.()) {
    // ETH
    case "ETH":
      return ETHConstants;
    case "ROPSTEN":
      return RopstenConstants;
    case "NOKU_CHAIN":
      return NokuChainConstants;
    case "NOKU_CHAINTEST":
      return NokuChainTestConstants;
    case "NOKU_CHAIN2":
      return NokuChain2Constants;
    case "NOKU_CHAIN2TEST":
      return NokuChain2TestConstants;
    case "BSC":
      return BscConstants;
    case "BSC_TEST":
      return BscTestConstants;
    // BTC
    case "BTC":
      return BTCConstants;
    case "BTCTEST":
      return BTCTestConstants;
    case "BCH":
      return BCHConstants;
    case "BCHTEST":
      return BCHTestConstants;
    case "DASH":
      return DashConstants;
    case "DASHTEST":
      return DashTestConstants;
    case "LTC":
      return LTCConstants;
    case "LTCTEST":
      return LTCTestConstants;
    case "ZEC":
      return ZECConstants;
    case "ZECTEST":
      return ZECTestConstants;
    // Waves
    case "WAVES":
      return WavesConstants;
    case "WAVESTEST":
      return WavesTestConstants;
    default:
      break;
  }
};
