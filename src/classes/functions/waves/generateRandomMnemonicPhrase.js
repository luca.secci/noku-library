import waves4you from 'waves4you';

/**
 * @param {"mainnet"|"testnet"} network
 */
export const generateRandomMnemonicPhrase = async (network) => {
  const newWallet = waves4you.createNewWallet(network);
  return newWallet.phrase;
};
