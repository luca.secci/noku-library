import { ETHLikeChainInfoList } from "..";

/**
 * @param {number} chainId 
 */
export const getETHLikeChainInfoFromChainId = (chainId) => ETHLikeChainInfoList.find(({chainId: currentChainId}) => chainId === currentChainId);
