import eth4you from 'eth4you';

/**
 * Gets accounts from a mnemonic phrase
 * @param {{type: string, password: string}} options
 * @param {string} mnemonicPhrase A mnemonic phrase
 * @param {number} index
 * @param {string} chain
 */
export const getAccountsFromMnemonicPhrase = async ({ type, password }, mnemonicPhrase, index, chain) => {
  let typeValue;
  switch (type) {
    case 'jaxx':
    case 'metamask':
    case 'exodus':
    case 'trezor':
    case 'digitalBitbox':
      typeValue = '0';
      break;
    case 'ledger':
      typeValue = '1';
      break;
    case 'eidoo':
      typeValue = '2';
      break;
    default:
      typeValue = '0';
      break;
  }
  const mnes = await eth4you.getMnemonicAccounts2(mnemonicPhrase, password, index, 1, typeValue, chain);
  return mnes;
};
