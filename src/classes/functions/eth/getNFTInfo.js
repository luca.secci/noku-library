import axios from 'axios';

import { NFTCacheStorage } from '../../../utils/customStorage';

const createNFTInfoKey = (address, tokenId) => {
  return `NFTInfo-${address}-${tokenId}`;
};

/**
 * A token object
 * @typedef {{address: string, token_id?: string, tokenURI?: string, is_custom_token?: boolean}} NFTToken
 */

/**
 * @param {NFTToken} token The token of which we get the image
 * @param {{[key: string]: string}[]} erc721TopTokens A list of top tokens
 */
export const getNFTImage = async (token, erc721TopTokens = []) => {
  if (!token) {
    return null;
  }

  try {
    const { imageSrc } = await getNFTInfo(token, erc721TopTokens);
    return imageSrc;
  } catch (ex) {
    return null;
  }
};

/**
 * Gets normalised token info
 * @param {NFTToken} token The token
 * @param {{[key: string]: string}[]} erc721TopTokens A map of the top tokens
 */
export const getNFTName = async (token, erc721TopTokens = []) => {
  if (!token) {
    return null;
  }

  try {
    const { name } = await getNFTInfo(token, erc721TopTokens);
    return name;
  } catch (ex) {
    return null;
  }
};

/**
 * Gets normalised token info
 * @param {NFTToken} token The token
 * @param {{[key: string]: string}[]} erc721TopTokens A map of the top tokens
 */
export const getNFTDescription = async (token, erc721TopTokens = []) => {
  if (!token) {
    return null;
  }

  let result = null;

  const { address } = token;

  try {
    // let response = await this.getNFTInfo(token);
    switch (erc721TopTokens[address?.toLowerCase?.()]) {
      case 'Luxochain':
        result = (await axios.get(`https://luxo-api.noku.io/description?contract_addr=${token.address.toLowerCase()}`)).data;
        break;
      default:
        // result = response.image;
        break;
    }

    return result;
  } catch (ex) {
    return null;
  }
};

/**
 * Gets normalised token info
 * @param {NFTToken} token The token
 * @param {{[key: string]: string}[]} erc721TopTokens A map of the top tokens
 */
export const getNFTInfo = async (token = {}, erc721TopTokens = []) => {
  try {
    const result = {
      imageSrc: '',
      name: '',
      metadata: {},
      description: ''
    };

    const { address } = token;
    const id = token.tokenId ?? token.token_id ?? token.id;

    const nftInfoKey = createNFTInfoKey(address, id);
    
    // Fetch from local storage if available
    const cachedResult = await NFTCacheStorage.getItem(nftInfoKey);
    if (cachedResult) {
      return cachedResult;
    }

    switch (erc721TopTokens[address?.toLowerCase?.()]) {
      case 'CryptoKitties': {
        // Get info
        const url = `https://api.cryptokitties.co/kitties/${id}`;
        const { data } = await axios.get(url);
        // Assign info
        result.imageSrc = data.image_url;
        result.name = data.name;
        result.metadata = data;
        break;
      }
      case 'Luxochain': {
        // Get info
        const url = `https://luxo-api.noku.io/name?contract_addr=${address.toLowerCase()}`;
        const { data } = await axios.get(url);

        // Assign info
        result.name = data;
        result.imageSrc = `https://luxo-api.noku.io/image?contract_addr=${address.toLowerCase()}`;
        // Metadata is not handled
        break;
      }
      case 'Flower':
        result.imageSrc = `https://flowerpatch.app/render/flower-${id}.png`;
        break;
      case 'Dragonereum Dragon':
        result.imageSrc = `https://api.dragonereum.io/images/dragons/small/${id}.png`;
        break;
      case 'Gods Unchained':
      case 'DozerDoll':
      case 'MyCryptoHeroes:Extension':
      case 'MyCryptoHeroes:Hero':
      case 'MyCryptoHeroes:Land':
      case 'WarRiders':
      case 'BlockCities':
      case 'NokuCryptoHeroes': {
        // Get info
        const url = token.tokenURI;
        const { data } = await axios.get(url);

        // Assign info
        result.imageSrc = data.image;
        if (token.is_custom_token && token.tokenURI && token.tokenURI.indexOf('nftstorage.noku.io') > 0) {
          result.name = data.name;
          result.description = data.description;
        }
        result.metadata = data;
        break;
      }
      default: {
        if (token.is_custom_token && token.tokenURI && token.tokenURI.indexOf('nftstorage.noku.io') > 0) {
          // Get info
          const url = token.tokenURI;
          const { data } = await axios.get(url);

          // Assign info
          result.name = data.name;
          result.imageSrc = data.image;
          result.description = data.description;
          result.metadata = data;
        }
        break;
      }
    }

    return await NFTCacheStorage.setItem(nftInfoKey, result);
  } catch (error) {
    console.error(error);
    throw error;
  }
};
