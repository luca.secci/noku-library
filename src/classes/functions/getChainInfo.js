import ethereumLogo from '../../assets/images/chips/ethereum.png';
import bitcoinLogo from '../../assets/images/chips/bitcoin.png';
import wavesLogo from '../../assets/images/chips/waves.png';
import luxoLogo from '../../assets/images/chips/luxochain.svg';

import dashLogo from '../../assets/images/additionalCoins/dash.svg';
import ltcLogo from '../../assets/images/additionalCoins/litecoin.svg';
import zecLogo from '../../assets/images/additionalCoins/zcash.svg';
import xmrLogo from '../../assets/images/additionalCoins/monero.svg';

export const ETHChainInfo = {
  logo: ethereumLogo,
  hasFungibleTokens: true,
  hasNonFungibleTokens: true,
  hasGas: true, // transaction_fees = gas_limit * gas_price
  hasDataInTransaction: true,
  symbol: 'Ξ',
  ticker: 'ETH',
  decimals: 18,
  feeUnitLabel: 'GWEI',
  chainId: 1,
  isEthereumLike: true,
  createWithBlockies: true,
  checkENS: true,
  hasFees: true,
  chainName: "Ethereum",
  supportsEip1559: true,
  isTestnet: false,
};

export const RopstenChainInfo = {
  ...ETHChainInfo,
  symbol: 'ΞT',
  chainId: 3,
  chainName: "Ethereum Testnet",
  supportsEip1559: true,
  isTestnet: true,
};

/* 
Reference to add custom chain to any web3 provider:
interface AddEthereumChainParameter {
  chainId: string; // A 0x-prefixed hexadecimal string
  chainName: string;
  nativeCurrency: {
    name: string;
    symbol: string;
    decimals: number; // Metamask supports only 18
  };
  rpcUrls: string[];
  blockExplorerUrls?: string[];
  iconUrls?: string[];  // Metamask ignores this
}
*/

export const NokuChainChainInfo = {
  ...ETHChainInfo,
  logo: luxoLogo,
  symbol: 'LUXO',
  ticker: 'LUXO',
  chainId: 110,
  hasFees: false,
  checkENS: false,
  chainName: 'Luxochain',
  // AddEthereumChainParameter
  customChain: {
    chainId: `0x${(110).toString(16)}`,
    chainName: 'Luxochain',
    nativeCurrency: {
      name: "LUXO",
      symbol: "LUXO",
      decimals: 18,
    },
    rpcUrls: ["http://78.47.153.227:32000"],
  },
  supportsEip1559: false,
  isTestnet: false,
};

export const NokuChainTestChainInfo = {
  ...NokuChainChainInfo,
  chainId: 222,
  chainName: 'Luxochain Testnet',
  // AddEthereumChainParameter
  customChain: {
    chainId: `0x${(222).toString(16)}`,
    chainName: 'Luxochain Testnet',
    nativeCurrency: {
      name: "LUXO TEST",
      symbol: "LUXO",
      decimals: 18,
    },
    rpcUrls: ["http://78.47.153.227:22000"],
  },
  supportsEip1559: false,
  isTestnet: true,
};

export const BscChainInfo = {
  ...ETHChainInfo,
  logo: "https://static.noku.io/assets/noku/coins/bsc.png",
  symbol: 'BNB',
  ticker: 'BNB',
  chainId: 56,
  hasFees: true,
  checkENS: true,
  chainName: 'Binance Smart Chain',
  // AddEthereumChainParameter
  customChain: {
    chainId: `0x${(56).toString(16)}`,
    chainName: 'Binance Smart Chain',
    nativeCurrency: {
      name: "BNB",
      symbol: "BNB",
      decimals: 18,
    },
    rpcUrls: ["https://bsc-dataseed.binance.org/"],
  },
  supportsEip1559: true,
};

export const BscTestChainInfo = {
  ...BscChainInfo,
  chainId: 97,
  chainName: 'Binance Smart Chain Test',
  // AddEthereumChainParameter
  customChain: {
    chainId: `0x${(97).toString(16)}`,
    chainName: 'Binance Smart Chain Test',
    nativeCurrency: {
      name: "BNB TEST",
      symbol: "BNB",
      decimals: 18,
    },
    rpcUrls: ["https://data-seed-prebsc-1-s1.binance.org:8545/"],
  },
  supportsEip1559: true,
};

export const NokuChain2ChainInfo = {
  ...NokuChainChainInfo,
  logo: "https://static.noku.io/assets/noku/coins/noku.svg",
  symbol: 'NOKU',
  ticker: 'NOKU',
  chainId: 888,
  chainName: 'Nokuchain',
  // AddEthereumChainParameter
  customChain: {
    chainId: `0x${(888).toString(16)}`,
    chainName: 'Nokuchain',
    nativeCurrency: {
      name: "NOKU",
      symbol: "NOKU",
      decimals: 18,
    },
    rpcUrls: ["http://18.158.93.226:32000"],
  },
  supportsEip1559: false,
  isTestnet: false,
};

export const NokuChain2TestChainInfo = {
  ...NokuChain2ChainInfo,
  chainId: 333,
  chainName: 'Nokuchain Testnet',
  // AddEthereumChainParameter
  customChain: {
    chainId: `0x${(333).toString(16)}`,
    chainName: 'Nokuchain Testnet',
    nativeCurrency: {
      name: "NOKU TEST",
      symbol: "NOKU",
      decimals: 18,
    },
    rpcUrls: ["http://3.125.54.28:32000"],
  },
  supportsEip1559: false,
  isTestnet: true,
};

export const ETHLikeChainInfoList = [ETHChainInfo, RopstenChainInfo, NokuChainChainInfo, NokuChainTestChainInfo, NokuChain2ChainInfo, NokuChain2TestChainInfo];

export const BTCChainInfo = {
  logo: bitcoinLogo,
  decimals: 8,
  feeUnitLabel: 'SAT/B',
  symbol: '₿',
  ticker: 'BTC',
  isBitcoinLike: true,
  chainName: 'Bitcoin',
  hasFees: true,
  isTestnet: false,
};

export const BTCTestChainInfo = {
  ...BTCChainInfo,
  symbol: '₿T',
  ticker: 'BTCTEST',
  chainName: 'Bitcoin Testnet',
  isTestnet: true,
};

export const BCHChainInfo = {
  ...BTCChainInfo,
  symbol: '₿C',
  ticker: 'BCH',
  chainName: "Bitcoin Cash",
  isTestnet: false,
};

export const BCHTestChainInfo = {
  ...BCHChainInfo,
  symbol: '₿CT',
  ticker: 'BCHTEST',
  chainName: "Bitcoin Cash Testnet",
  isTestnet: true,
};

export const DashChainInfo = {
  ...BTCChainInfo,
  logo: dashLogo,
  symbol: 'Đ',
  ticker: 'DASH',
  chainName: "Dash",
  isTestnet: false,
};

export const DashTestChainInfo = {
  ...DashChainInfo,
  symbol: 'ĐT',
  ticker: 'DASHTEST',
  chainName: "Dash Testnet",
  isTestnet: true,
};

export const LTCChainInfo = {
  ...BTCChainInfo,
  logo: ltcLogo,
  ticker: 'LTC',
  symbol: 'Ł',
  chainName: "Litecoin",
  isTestnet: false,
};

export const LTCTestChainInfo = {
  ...LTCChainInfo,
  ticker: 'LTCTEST',
  symbol: 'ŁT',
  chainName: "Litecoin Testnet",
  isTestnet: true,
};

export const ZECChainInfo = {
  ...BTCChainInfo,
  logo: zecLogo,
  ticker: 'ZEC',
  symbol: 'Ƶ',
  chainName: "Zcash",
  isTestnet: false,
};

export const ZECTestChainInfo = {
  ...ZECChainInfo,
  ticker: 'ZECTEST',
  symbol: 'tƵ',
  chainName: "Zcash Testnet",
  isTestnet: true,
};

export const WavesChainInfo = {
  logo: wavesLogo,
  hasFungibleTokens: true,
  // Unsupported
  hasNonFungibleTokens: false,
  symbol: 'WAVES',
  ticker: 'WAVES',
  decimals: 8,
  hasFees: false,
  feeUnitLabel: 'WAVELET',
  createWithBlockies: true,
  isWavesLike: true,
  chainName: "Waves",
  isTestnet: false,
};

export const WavesTestChainInfo = {
  ...WavesChainInfo,
  symbol: 'WAVESTEST',
  ticker: 'WAVESTEST',
  chainName: "Waves Testnet",
  isTestnet: true,
};

export const XMRChainInfo = {
  logo: xmrLogo,
  decimals: 12,
  feeUnitLabel: 'XMR/B',
  symbol: 'XMR',
  isMoneroLike: true,
  chainName: "Monero",
  isTestnet: false,
};

/**
 * Gets the chainInfo object without creating a chainer
 * @param {AvailableChains} chain Chain name
 */
export const getChainInfo = (chain) => {
  switch (chain?.toUpperCase()) {
    case 'ETH':
      return ETHChainInfo;
    case 'ROPSTEN':
      return RopstenChainInfo;
    case 'NOKU_CHAIN':
      return NokuChainChainInfo;
    case 'NOKU_CHAINTEST':
      return NokuChainTestChainInfo;
    case 'NOKU_CHAIN2':
      return NokuChain2ChainInfo;
    case 'NOKU_CHAIN2TEST':
      return NokuChain2TestChainInfo;
    case 'BSC':
      return BscChainInfo;
    case 'BSC_TEST':
      return BscTestChainInfo;
    case 'BTC':
      return BTCChainInfo;
    case 'BTCTEST':
      return BTCTestChainInfo;
    case 'BCH':
      return BCHChainInfo;
    case 'BCHTEST':
      return BCHTestChainInfo;
    case 'DASH':
      return DashChainInfo;
    case 'DASHTEST':
      return DashTestChainInfo;
    case 'LTC':
      return LTCChainInfo;
    case 'LTCTEST':
      return LTCTestChainInfo;
    case 'ZEC':
      return ZECChainInfo;
    case 'ZECTEST':
      return ZECTestChainInfo;
    case 'WAVES':
      return WavesChainInfo;
    case 'WAVESTEST':
      return WavesTestChainInfo;
    case 'XMR':
      return XMRChainInfo;
    default:
      break;
  }
};
