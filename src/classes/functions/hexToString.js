/**
 * @param {*} hex Hex numbers to convert
 */
export const hexToString = (hex) => {
  let str = '';
  try {
    let len = hex.length;
    if (len & 1) {
      len--;
    }
    for (let i = 0; i < len; i += 2) {
      str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    }
  } catch (e) {
    return '';
  }
  return str;
};
