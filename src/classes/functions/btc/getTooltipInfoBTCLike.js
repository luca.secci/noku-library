import * as btc4you from 'btc4you';
import BigNumber from 'bignumber.js';

import { TooltipObject } from '../../TooltipObject/TooltipObject';

export const getTooltipInfoBTCLike = (multi, feeRate, chain, hdType, chainInfo) => {
  let tooltipObject = {};
  if (feeRate && feeRate > 0) {
    // Get average transaction size
    const transactionSize = btc4you.getAverageTransactionSize(chain, hdType);
    // Compute fee in BTC
    const baseBtcFee = new BigNumber(feeRate).times(transactionSize).div(new BigNumber(10).pow(chainInfo.decimals));
    // Get BTC -> Currency conversion rates
    // Make needed conversions
    const needMulti = [];
    for (const key in multi) {
      if (Object.hasOwnProperty.call(multi, key)) {
        const element = multi[key];
        if (key !== 'ts' && key !== 'delayed') { needMulti.push({ currency: key, value: baseBtcFee.times(element) }); }
      }
    }
    tooltipObject = new TooltipObject(transactionSize, [...needMulti]);
  }
  return tooltipObject;
};
