// Generic functions
import { getBlockie } from './getBlockie';
import { getChainInfo, ETHLikeChainInfoList } from './getChainInfo';
import { getThemeObject } from './getThemeObject';
import { hexToString } from './hexToString';
import { stringToHex } from './stringToHex';
import { getChainConstants, UnlockMethods } from "./getChainConstants";

// ETH only
import { generateRandomMnemonicPhrase } from './eth/generateRandomMnemonicPhrase';
import { getAccountsFromMnemonicPhrase } from './eth/getAccountsFromMnemonicPhrase';
import { getTooltipInfoETHLike } from './eth/getTooltipInfoETHLike';
import {
  getNFTInfo,
  getNFTName,
  getNFTDescription,
  getNFTImage
} from './eth/getNFTInfo';
import { getETHLikeChainInfoFromChainId } from "./eth/getETHLikeChainInfoFromChainId";

// BTC Only
import { getTooltipInfoBTCLike } from './btc/getTooltipInfoBTCLike';

export {
  // Generic
  getBlockie,
  getChainInfo,
  getChainConstants,
  getThemeObject,
  hexToString,
  stringToHex,
  UnlockMethods,
  // ETH
  generateRandomMnemonicPhrase,
  getAccountsFromMnemonicPhrase,
  getTooltipInfoETHLike,
  getNFTInfo,
  getNFTName,
  getNFTDescription,
  getNFTImage,
  ETHLikeChainInfoList,
  getETHLikeChainInfoFromChainId,
  // BTC
  getTooltipInfoBTCLike
};
