import { ThemeObject } from '../ThemeObject/ThemeObject';
import DASHSymbol from '../../assets/images/additionalCoins/dash.svg';
import LTCSymbol from '../../assets/images/additionalCoins/litecoin.svg';
import NokuChainSymbol from '../../assets/images/additionalCoins/nokuchain.svg';
import WAVESSymbol from '../../assets/images/additionalCoins/waves.svg';
import ZECSymbol from '../../assets/images/additionalCoins/zcash.svg';
import XMRSymbol from '../../assets/images/additionalCoins/monero.svg';
import LuxoChainSymbol from '../../assets/images/additionalCoins/luxochain.svg';

// BCH
const BCHThemeObject = new ThemeObject(true, ['fab', 'btc'], '45, 179, 0', '196, 242, 181', { transform: 'rotate(340deg)' }, 'Bitcoin Cash');
// BCH Test
const BCHTestThemeObject = new ThemeObject(true, ['fab', 'btc'], '65, 144, 212', '16, 61, 99', { transform: 'rotate(340deg)' }, 'Bitcoin Cash TESTNET'); // should be rotate 340deg
// BTC
const BTCThemeObject = new ThemeObject(true, ['fab', 'btc'], '255,158,0', '255,236,204', { transform: 'rotate(20deg)' }, 'Bitcoin'); // should be rotate 20deg
// BTC Test
const BTCTestThemeObject = new ThemeObject(true, ['fab', 'btc'], '148,0,255', '255,210,255', { transform: 'rotate(20deg)' }, 'Bitcoin TESTNET'); // should be rotate 20 deg
// DASH
const DASHThemeObject = new ThemeObject(false, DASHSymbol, '38, 115, 195', '212, 227, 243', { transform: 'rotate(0deg)' }, 'Dash');
// DASH Test
const DASHTestThemeObject = new ThemeObject(false, DASHSymbol, '235, 64, 52', '255, 215, 210', { transform: 'rotate(0deg)' }, 'Dash TESTNET');
// ETH
const ETHThemeObject = new ThemeObject(true, ['fab', 'ethereum'], '106,140,166', '232,237,241', { opacity: '0.8' }, 'Ethereum');
// Ropsten
const ROPSTENThemeObject = new ThemeObject(true, ['fab', 'ethereum'], '82, 79, 24', '245, 236, 71', { opacity: '0.8' }, 'ETH [Ropsten]');
// LTC
const LTCThemeObject = new ThemeObject(false, LTCSymbol, '135, 138, 139', '237, 238, 238', { transform: 'rotate(0deg)' }, 'Litecoin');
// LTC Test
const LTCTestThemeObject = new ThemeObject(false, LTCSymbol, '44, 108, 112', '154, 208, 222', { transform: 'rotate(0deg)' }, 'Litecoin TESTNET');
// NokuChain (LuxoChain)
const NokuChainThemeObject = new ThemeObject(false, LuxoChainSymbol, '68, 70, 237', '235, 235, 235', { opacity: '0.8' }, 'NOKU');
// NokuChain Test (LuxoChain Test)
const NokuChainTestThemeObject = new ThemeObject(false, LuxoChainSymbol, '68, 70, 237', '122, 246, 46', { opacity: '0.8' }, 'NOKU CHAIN TESTNET');
// NokuChain2
const NokuChain2ThemeObject = new ThemeObject(false, NokuChainSymbol, '68, 70, 237', '235, 235, 235', { opacity: '0.8' }, 'NOKU');
// NokuChain2 Test
const NokuChain2TestChainer = new ThemeObject(false, NokuChainSymbol, '68, 70, 237', '122, 246, 46', { opacity: '0.8' }, 'NOKU CHAIN2 TESTNET');
// Waves
const WavesThemeObject = new ThemeObject(false, WAVESSymbol, '0, 85, 255', '204, 221, 255', { transform: 'rotate(0deg)' }, 'Waves');
// Waves Test
const WavesTestThemeObject = new ThemeObject(false, WAVESSymbol, '83, 109, 52', '215, 203, 255', { transform: 'rotate(0deg)' }, 'Waves TESTNET');
// ZEC
const ZECThemeObject = new ThemeObject(false, ZECSymbol, '254, 189, 10', '255, 245, 219', { transform: 'rotate(0deg)' }, 'ZCash');
// Zec Test
const ZECTestThemeObject = new ThemeObject(false, ZECSymbol, '90, 47, 13', '180, 137, 107', { transform: 'rotate(0deg)' }, 'ZCash TESTNET');
// XMR
const XMRThemeObject = new ThemeObject(false, XMRSymbol, '255, 102, 0', '255, 225, 211', { transform: 'rotate(0deg)' }, 'Monero');

/**
 *
 * @param {AvailableChains} chain
 */
export const getThemeObject = (chain) => {
  switch (chain) {
    case 'BCH':
      return BCHThemeObject;
    case 'BCHTEST':
      return BCHTestThemeObject;
    case 'BTC':
      return BTCThemeObject;
    case 'BTCTEST':
      return BTCTestThemeObject;
    case 'DASH':
      return DASHThemeObject;
    case 'DASHTEST':
      return DASHTestThemeObject;
    case 'ETH':
      return ETHThemeObject;
    case 'ROPSTEN':
      return ROPSTENThemeObject;
    case 'NOKU_CHAIN':
      return NokuChainThemeObject;
    case 'NOKU_CHAIN2':
      return NokuChain2ThemeObject;
    case 'NOKU_CHAINTEST':
      return NokuChainTestThemeObject;
    case 'NOKU_CHAIN2TEST':
      return NokuChain2TestChainer;
    case 'WAVES':
      return WavesThemeObject;
    case 'WAVESTEST':
      return WavesTestThemeObject;
    case 'LTC':
      return LTCThemeObject;
    case 'LTCTEST':
      return LTCTestThemeObject;
    case 'ZEC':
      return ZECThemeObject;
    case 'ZECTEST':
      return ZECTestThemeObject;
    case 'XMR':
      return XMRThemeObject;
    default:
      break;
  }
};
