class ThemeObject {
  /**
    * Creates a Theme Object to use with the Chainer, this includes only the information to build it with informations about the icon and the color; this must not include any JSX
    * @param {boolean} isFontAwesome If the icon is available from FontAwesome
    * @param {string[]} iconSource The icon source, if isFontAwesome === true then iconSource must be an array like ["fab", "btc"], else it must be the src of an img
    * @param {string} primaryColor A string representation of the primary color of a given chain as rgb value, example: "100, 200, 0"
    * @param {string} clearPrimaryColor A string representation of the primary color of a given chain as rgb value, but clearer, example: "120, 220, 20"
    * @param {CSS} additionalStyle Any additional style that must be applied to the icon
    * @param {string} label This is the full name of the chain (used in printWallet)
    */
  constructor (isFontAwesome, iconSource, primaryColor, clearPrimaryColor, additionalStyle, label) {
    // Check for correctness of input, see jsdoc
    // if (isFontAwesome && !Array.isArray(iconSource)) {
    //   throw Error('Icon Source must be an array of string');
    // }
    // if (!isFontAwesome && !(iconSource instanceof String)) {
    //   throw Error('Icon Source must be a string');
    // }
    // Everything is ok
    this.isFontAwesome = isFontAwesome;
    this.iconSource = iconSource;
    this.primaryColor = primaryColor;
    this.clearPrimaryColor = clearPrimaryColor;
    this.additionalStyle = additionalStyle;
    this.label = label;
  }
}

export { ThemeObject };
