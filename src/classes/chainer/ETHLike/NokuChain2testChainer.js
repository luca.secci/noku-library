import { NokuChain2TestConstants } from '../../functions/getChainConstants';
import NokuChain2Chainer from './NokuChain2Chainer';

class NokuChain2testChainer extends NokuChain2Chainer {
  constructor (chain = 'NOKU_CHAIN2TEST', socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance = (b) => b, _initOptions) {
    super(chain, socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance, _initOptions);

    this._isTestnet = true;

    this.setChainInfo(NokuChain2TestConstants.chainInfo);

    this._customChain = NokuChain2TestConstants.customChain;

    this.cryptoHeroesAddress = NokuChain2TestConstants.cryptoHeroesAddress;
  }

  getExternalExplorerLink(hash) {
    return `https://testnet-explorer.noku.io/tx/${hash}/token-transfers`;
  }
}

export default NokuChain2testChainer;
