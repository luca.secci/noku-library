import BigNumber from 'bignumber.js';
import eth4you from 'eth4you';
import axios from 'axios';

import Chainer from '../Chainer';
import isEmptyObject from '../../../utils/isEmptyObject';
import { generateRandomMnemonicPhrase } from '../../functions/eth/generateRandomMnemonicPhrase';
import { getAccountsFromMnemonicPhrase } from '../../functions/eth/getAccountsFromMnemonicPhrase';
import { getTooltipInfoETHLike } from '../../functions/eth/getTooltipInfoETHLike';
import { getNFTDescription, getNFTImage, getNFTInfo, getNFTName } from '../../functions/eth/getNFTInfo';
import { ETHConstants } from '../../functions/getChainConstants';
import { SupportedProviders } from '../../../utils/web3/providers';
import { equalsInsensitive } from '../../../utils/equalsInsensitive';

/**
 * Method signatures
 * @typedef {(address: string, chain: string, pending?: boolean) => Promise<string|number>} getNonceFn
 * @typedef {(txSigned: string, data: any, chain: string) => Promise} sendRawTxFn
 * @typedef {({from: string, to: string, data: any, value: string, gasprice: string}, chain: string, axiosParams: any) => Promise<number>} estimateGasFn
 * @typedef {(chain: string, addressOrEns: string) => Promise<string|null>} getAddressByEnsFn
 */

class EthChainer extends Chainer {
  /**
   * An ETH chainer
   * @param {string} chain The chain
   * @param {string} socketEndpoint Web socket endpoint to listen to balance/transactions/etc.
   * @param {string} socketSwapEndpoint Web socket endpoint for swap
   * @param {getNonceFn} _getNonce
   * @param {sendRawTxFn} _sendRawTransaction ?
   * @param {estimateGasFn} _estimateGas A promise function that gives back the gas limit of a transaction
   * @param {getAddressByEnsFn} _getAddressByENS A promise function that gives back the address by ENS or null if wrong
   * @param {} _getBalance A promise function that gives back the wallet's balance
   * @param {} _getTransaction A promise function that gives back the transaction
   * @param {} _sendRawTransactionWithParams A promise function that sends a transaction with some params, used by swap (is it generalizable?)
   */
  constructor(chain = 'ETH', socketEndpoint = '', socketSwapEndpoint = '', _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance = (b) => b, _initOptions = { fetchLuxo: false }) {
    super(chain, socketEndpoint, socketSwapEndpoint, _parseBalance);

    this._unlockMethods = ETHConstants.unlockMethods;

    this.setChainInfo(ETHConstants.chainInfo);

    // Cached addresses by ENS
    this.addressesByEns = new Map();

    // THESE ARE TEMPORARY LUXOCHAIN API & IMPLEMENTATIONS. TO BE RE-IMPLEMENTED!!!!!!
    if (_initOptions.fetchLuxo) {
      this.TMP_LUXO_getContracts();
    }

    // ERC721 top tokens
    this.erc721TopTokens = ETHConstants.erc721TopTokens;

    // Token addresses
    this.nokuTokenAddress = ETHConstants.nokuTokenAddress;
    this.eurnTokenAddress = ETHConstants.eurnTokenAddress;
    this.chfnTokenAddress = ETHConstants.chfnTokenAddress;
    this.cryptoHeroesAddress = ETHConstants.cryptoHeroesAddress;

    // Custom methods
    this._getNonce = _getNonce;
    this._sendRawTransaction = _sendRawTransaction;
    this._estimateGas = _estimateGas;
    this._getAddressByENS = _getAddressByENS;
    this._getBalance = _getBalance;
    this._getTransaction = _getTransaction;
    this._sendRawTransactionWithParams = _sendRawTransactionWithParams;
    this._parseBalance = _parseBalance;

    // Supported Web3 External providers (Metamask, Ledger, Trezor, etc.)
    this.supportedExternalProviders = [...SupportedProviders];


  }

  // THESE ARE TEMPORARY LUXOCHAIN API & IMPLEMENTATIONS. TO BE RE-IMPLEMENTED!!!!!!
  async TMP_LUXO_getContracts() {
    try {
      let list;
      const listFromStorage = sessionStorage.getItem('luxochaintokens');
      if (listFromStorage) {
        list = JSON.parse(listFromStorage);
      } else {
        const { data } = await axios.get('https://luxo-api.noku.io/luxoaddresslist');
        list = data?.list ?? [];
        if (!sessionStorage.getItem('luxochaintokens')) {
          sessionStorage.setItem('luxochaintokens', JSON.stringify(list));
        }
      }
      for (let i = 0; i < list.length; i++) {
        this.erc721TopTokens[list[i]] = 'Luxochain';
      }
    } catch (ex) { }
  }

  /**
   * @param {string} ens An ENS (ex. mywallet.eth)
   * @return {Promise<string>}
   */
  async getAddressByENS(ens) {
    // Check in cache
    if (this.addressesByEns.has(ens)) {
      return this.addressesByEns.get(ens);
    }
    // Get the address and save it in cache
    const address = await this._getAddressByENS(this.chain, ens);
    if (address) {
      this.addressesByEns.set(ens, address);
    }
    return address;
  }

  /**
   * Gets a valid address
   * @param {string} addressOrENS
   */
  async resolveAddressOrENS(addressOrENS) {
    const chainInfo = this.getChainInfo();
    let address = '';

    const { default: isEthereumAddress } = await import("validator/es/lib/isEthereumAddress");
    if (isEthereumAddress(addressOrENS)) {
      address = addressOrENS;
    } else if (chainInfo.checkENS && this._getAddressByENS) {
      try {
        const addressByEns = await this.getAddressByENS(addressOrENS);
        if (addressByEns) {
          address = addressByEns;
        }
      } catch (ex) { }
    }

    return address?.toLowerCase();
  }

  /**
   * Instantiate a chainer via session storage
   * @param {string} addressOrENS Address or ENS
   */
  async loadFromStore(addressOrENS) {
    const address = await this.resolveAddressOrENS(addressOrENS);

    // Call load from store if defined
    // await super.loadFromStore?.(address);

    // Set address and private key
    this.address = address;

    const privateKey = sessionStorage.getItem('private-key-' + address);
    if (privateKey) {
      this.setPrivateKey(privateKey);
      return privateKey;
    }
    return false;
  }

  /**
   * Validate an ethereum address, true if it is valid
   * @param {string} addressOrENS An address or ENS
   */
  async validateAddress(addressOrENS) {
    const chainInfo = this.getChainInfo();
    const { default: isEthereumAddress } = await import("validator/es/lib/isEthereumAddress");
    if (isEthereumAddress(addressOrENS)) {
      return true;
    } else if (chainInfo.checkENS) {
      try {
        const addressByEns = await this.getAddressByENS(addressOrENS);
        if (addressByEns) {
          return true;
        }
      } catch (ex) { }
    }

    return false;
  }

  /**
   * @param {string} privateKey
   */
  async setPrivateKey(privateKey, savePrivateKey) {
    // Derive address
    const { toChecksumAddress, addressFromPrivate } = await import("eth4you");
    const address = toChecksumAddress(addressFromPrivate(privateKey))?.toLowerCase();
    if (this.address && !equalsInsensitive(this.address, address)) {
      throw new Error("Wrong private key");
    }
    this.address = address;
    // Save private key
    await super.setPrivateKey(privateKey, savePrivateKey);
  }

  getAddress() {
    if (this.address) {
      return this.address;
    } else if (this.getPrivateKey()) {
      const address = eth4you.toChecksumAddress(eth4you.addressFromPrivate(this.getPrivateKey()))?.toLowerCase();
      this.address = address;
      return address;
    } else {
      return '';
    }
  }

  /**
   * Expose _getNonce
   * @param  {...any} params Any additional params
   */
  async getNonce(...params) {
    const chain = this.getChain();
    const address = this.getAddress();
    return await this._getNonce(address, chain, ...params);
  }

  /**
   * @typedef {{
   *  address: string,
   *  erc20: boolean,
   *  erc721: boolean,
   *  decimals: number,
   *  token_id?: string,
   *  useErc721safeTransferFromData?: boolean
   *  useErc721safeTransferFrom?: boolean
   * }} Token
   */

  /**
  * Generate a transaction
  * @param {string} _fromAddress
  * @param {string} _toAddress
  * @param {Token} currency if defined the token to transfer
  * @param {string} _value transaction value "1.123",
  * @param {string} data data field in transaction
  * @param {number|string} gasPrice gasPrice OR priority fee in gwei
  * @param {boolean} isFullBalanceTransaction if should send all transfer
  * @param {{ balance: string, [key: string]: any }} options
  * @param {import('axios').AxiosRequestConfig} axiosParams
  * @param {*} _gasLimit
  * @return {Promise<{
  *   fee: any,
  *   totalFeeTx: string,
  *   rawTx: {
  *     chainId: number,
  *     [key: string]: any,
  *   },
  *   message?: string,
  * }>}
  */
  async generateTransaction(_fromAddress, _toAddress, currency, _value, data, gasPrice, isFullBalanceTransaction, options = {}, axiosParams = {}, _gasLimit, baseFeeGwei = 0, maxFeeGwei = 0) {
    const defaultResponse = { fee: undefined, rawTx: undefined, totalFeeTx: undefined, message: '' };
    try {
      const isTokenTransfer = !!(currency || {}).address;
      const chainInfo = this.getChainInfo();
      const { toUnit, toWei, generateEthTransaction, generateValue } = await import("eth4you");

      const baseFee = baseFeeGwei ? toUnit(baseFeeGwei, "gwei", "wei") : 0;
      const maxFee = maxFeeGwei ? toUnit(maxFeeGwei, "gwei", "wei") : 0;

      // Validate addresses
      const fromAddress = await this.resolveAddressOrENS(_fromAddress);
      if (!fromAddress) {
        throw new Error("Invalid from address");
      }

      const toAddress = await this.resolveAddressOrENS(_toAddress);
      if (!toAddress) {
        throw new Error("Invalid to address");
      }

      let value = _value;
      let estimateGasParams;
      let rawTransaction = {};
      const gasPriceWei = toWei(gasPrice, 'gwei');
      const gasPriceHex = gasPrice !== undefined ? '0x' + new BigNumber(gasPriceWei).toString(16) : null; // the eth "estimate_gas" requires an integer value

      if (isTokenTransfer) { // is a token transfer
        value = currency.erc721 ? '0x0' : value;
        rawTransaction = generateEthTransaction(fromAddress, toAddress, value, currency, gasPriceHex, 1, data, chainInfo.chainId, undefined, undefined, undefined, baseFee, maxFee);

        estimateGasParams = !isEmptyObject(rawTransaction) ? {
          from: fromAddress,
          to: currency.address,
          data: rawTransaction.data,
          value: '0x0',
          gasprice: gasPrice, //estimate needs GWEI!
          baseFee,
          maxPriorityFeePerGas: gasPriceWei, //in WEI!
          maxFeePerGas: rawTransaction.maxFeePerGas,
        } : null;
      } else {
        rawTransaction = generateEthTransaction(fromAddress, toAddress, value, undefined, gasPriceHex, 1, data, chainInfo.chainId, undefined, undefined, undefined, baseFee, maxFee);

        estimateGasParams = !isEmptyObject(rawTransaction) ? {
          from: fromAddress,
          to: toAddress,
          data: rawTransaction.data,
          value: generateValue(value),
          sel: 'ETH',
          gasPrice: gasPrice, //estimate needs GWEI!
          baseFee,
          maxPriorityFeePerGas: gasPriceWei, //in WEI!
          maxFeePerGas: rawTransaction.maxFeePerGas,
        } : null;
      }

      let BN_totalFeeEth;

      if (estimateGasParams) {
        let gasLimit = _gasLimit;
        if (!gasLimit) {
          gasLimit = await this._estimateGas({ ...estimateGasParams }, this.getChain(), axiosParams);
        }
        gasLimit = new BigNumber(gasLimit);

        if (!chainInfo.hasFees) {
          gasPrice = '0x0';
          rawTransaction.gasPrice = '0x0';
          rawTransaction.customChain = { ...this._customChain };
        }

        rawTransaction.gasLimit = gasLimit;
        rawTransaction.normalValue = _value; // ETH value

        // maxFeePerGas is base fee + priority fee
        // maxFeePerGas is in WEI, make it in GWEI
        if (chainInfo.hasFees) {
          const BN_maxFeePerGas = new BigNumber(rawTransaction.maxFeePerGas ?? 0);
          const maxFeePerGas = toUnit(BN_maxFeePerGas.toString(), "wei", "gwei");
          // gasLimit is in GWEI
          const BN_gasLimit = new BigNumber(rawTransaction.gasLimit);
          const totalFeeGWEI = new BigNumber(BN_gasLimit.times(maxFeePerGas));
          const totalFeeEth = toUnit(totalFeeGWEI, "gwei", "eth");
          BN_totalFeeEth = new BigNumber(totalFeeEth);
        } else {
          BN_totalFeeEth = new BigNumber(0);
        }

        if (!isTokenTransfer) {
          if (!isFullBalanceTransaction) {
            if ((new BigNumber(value)).plus(BN_totalFeeEth).isGreaterThan(options.balance || 0)) {
              throw new Error('Insufficient balance');
            }
          } else { // is full balance
            // const feeBN = (new BigNumber(gasPrice)).times(new BigNumber(rawTransaction.gasLimit));
            _value = (new BigNumber(_value)).minus(BN_totalFeeEth).toString(10);
            const newEstimateFee = await this.generateTransaction(fromAddress, toAddress, currency, _value, data, gasPrice, false, options, axiosParams = {}, _gasLimit, baseFee, maxFee);
            if ((new BigNumber(_value)).isLessThan(0)) {
              throw new Error('Insufficient balance');
            }
            return {
              ...newEstimateFee,
              newValue: _value
            };
          }
        }
        // maxFeeWei ? `0x${new BigNumber(maxFeeWei).toString(16)}` : `0x${new BigNumber(baseFeeWei).plus(gasPriceDec).toString(16)}`;


        const totalFeeTx = (BN_totalFeeEth).toString(10);
        return { fee: gasLimit, rawTx: rawTransaction, totalFeeTx };
      } else {
        throw new Error('Ops something went wrong during transaction building');
      }
    } catch (e) {
      return {
        ...defaultResponse,
        message: e
      };
    }
  }

  /*
  input {
    rawTx: object {chainId: number,  }
  }

  return {
  }
  */
  async sendRawTransaction(rawTx, data) {
    try {
      const nonce = rawTx.nonce ?? await this._getNonce(this.getAddress(), this.getChain());
      if (nonce === undefined) {
        throw new Error('empty nonce');
      }
      rawTx.nonce = nonce;

      const { signEthTransaction } = await import("eth4you");
      const txSigned = signEthTransaction(rawTx, this.getPrivateKey(), rawTx.chainId || this.getChainInfo().chainId);

      const result = await this._sendRawTransaction(txSigned, data, this.getChain());
      return result;
    } catch (e) {
      throw new Error(e.message || e);
    }
  }

  /*
  input {
    rawTx: object {chainId: number,  }
  }

  return {
  }
  */
  async sendSwapTransaction(rawTx, params) {
    try {
      const fromAddress = this.getAddress();
      const toAddress = params?.toAddress ?? this.getAddress();

      return await this.sendRawTransactionWithParams(rawTx, { ...params, fromAddress, toAddress });
    } catch (e) {
      throw new Error(e.message || e);
    }
  }

  async sendRawTransactionWithParams(rawTx, params) {
    try {
      const nonce = rawTx.nonce ?? await this._getNonce(this.getAddress(), this.getChain());
      if (nonce === undefined) {
        throw new Error('empty nonce');
      }
      rawTx.nonce = nonce;
      const type = params?.type;
      const swapSessionId = params?.swapSessionId;

      const { signEthTransaction } = await import("eth4you");
      const txSigned = signEthTransaction(rawTx, this.getPrivateKey(), rawTx.chainId || this.getChainInfo().chainId);

      const response = await this._sendRawTransactionWithParams(txSigned, params, this.getChain(), { type, swapSessionId });
      return response;
    } catch (e) {
      throw new Error(e.message || e);
    }
  }

  signEthTransaction(rawTx) {
    const txSigned = eth4you.signEthTransaction(rawTx, this.getPrivateKey(), rawTx?.chainId || this.getChainInfo().chainId);
    return txSigned;
  }

  /* METHODS */
  async getBalance(...additionalArguments) {
    const balance = await this._getBalance(this.getChain(), this.getAddress(true), ...additionalArguments);
    this.setBalance(balance);
    return this._parseBalance(balance);
  }

  async getTransaction(hash, shouldCheckPending) {
    if (!hash) {
      throw new Error('missing parameter hash');
    }
    try {
      const chain = this.getChain();
      return await this._getTransaction(chain, hash, shouldCheckPending);
    } catch (error) {
      return null;
    }
  }

  getExternalExplorerLink(transactionHash) {
    return `https://etherscan.io/tx/${transactionHash}`;
  }

  /* WEBSOCKET METHODS */

  setVariablesSubscription(type, result) {
    switch (type) {
      case 'gasPriceConfiguration':
        this.predictGasStationTime = result.predictGasStationTime;
        this.gasPriceList = result.gasPriceList;
        this.baseFee = result.baseFee;
        break;
      case 'balance': {
        const balanceResponse = result;
        this.setBalance(balanceResponse);
        result = this._parseBalance(balanceResponse);
        break;
      }
      case 'transactions':
        break;
      default:
        break;
    }
    return result;
  }

  /* JSON METHODS */
  async unlockJsonWallet(json, password) {
    try {
      const { fromFile_remastered } = await import("eth4you");
      const content = fromFile_remastered(json, password);
      const privateKey = content.key;
      await this.setPrivateKey(privateKey);
      if (json.name) {
        this.name = json.name;
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  // additionalInfo è per usi futuri
  generateJson(password, _additionalInfo = {}) {
    const privateKey = this.getPrivateKey();
    if (!privateKey) {
      throw new Error('no private key');
    }
    const string = eth4you.toV3(privateKey, password, this.getName(), this.getAddress(), this.getChain(), undefined, this.getMnemonicPhrase());
    return JSON.parse(string);
  }

  async lock() {
    const address = this.getAddress();
    super.lock(address);
    return true;
  }

  /* MNEMONIC PHRASE METHODS */
  async generateRandomMnemonicPhrase() {
    return await generateRandomMnemonicPhrase();
  }

  async getAccountFromMnemonicPhrase(mnemonicPhrase = '', index = 0, options = {}, savePrivateKey) {
    const mnes = await getAccountsFromMnemonicPhrase(options, mnemonicPhrase, index, this.chain);
    const account = mnes[0];
    await this.setPrivateKey(account.privateKey, savePrivateKey);
    this.setMnemonicPhrase(mnemonicPhrase, savePrivateKey);
    return this;
  }

  /* GAS RELATED METHODS */
  _getPredictGasStationTime(gasPrice) {
    if (!this.predictGasStationTime) {
      return null;
    }
    const time = this.predictGasStationTime[gasPrice];
    if (time) {
      return time;
    }
    return null;
  }

  getTooltipInfo(multi, gasPrice, gasLimit) {
    const estimatedMinutes = this._getPredictGasStationTime(gasPrice);
    return getTooltipInfoETHLike(multi, gasPrice, gasLimit, estimatedMinutes);
  }

  /* NFT METHODS */
  async getNFTImage(token) {
    try {
      return await getNFTImage(token, this.erc721TopTokens);
    } catch (error) {
      return '';
    }
  }

  async getNFTName(token) {
    try {
      return await getNFTName(token, this.erc721TopTokens);
    } catch (error) {
      return '';
    }
  }

  async getNFTDescription(token) {
    try {
      return await getNFTDescription(token, this.erc721TopTokens);
    } catch (error) {
      return '';
    }
  }

  async getNFTInfo(token) {
    try {
      return await getNFTInfo(token, this.erc721TopTokens);
    } catch (error) {
      return {};
    }
  }
}

export default EthChainer;
