import BscChainer from './BscChainer';
import { BscTestConstants } from '../../functions/getChainConstants';

class BscTestChainer extends BscChainer {
  constructor(chain = 'BSC_TEST', socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance = (b) => b, _initOptions) {
    super(chain, socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance, _initOptions);

    this._isTestnet = true;

    this._customChain = BscTestConstants.customChain;

    this.setChainInfo(BscTestConstants.chainInfo);
  }

  getExternalExplorerLink(hash) {
    return `https://testnet.bscscan.com/tx/${hash}`;
  }
}

export default BscTestChainer;
