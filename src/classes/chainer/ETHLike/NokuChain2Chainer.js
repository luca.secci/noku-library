import { NokuChain2Constants } from '../../functions/getChainConstants';
import NokuChainChainer from './NokuChainChainer';

class NokuChain2Chainer extends NokuChainChainer {
  constructor (chain = 'NOKU_CHAIN2', socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance = (b) => b, _initOptions) {
    super(chain, socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance, _initOptions);

    this.setChainInfo(NokuChain2Constants.chainInfo);

    this._customChain = NokuChain2Constants.customChain;

    this.erc721TopTokens = NokuChain2Constants.erc721TopTokens;

    this.cryptoHeroesAddress = NokuChain2Constants.cryptoHeroesAddress;
  }

  getExternalExplorerLink(hash) {
    return `https://explorer.noku.io/tx/${hash}/token-transfers`;
  }
}

export default NokuChain2Chainer;
