import EthChainer from './EthChainer';
import { RopstenConstants } from '../../functions/getChainConstants';

class RopstenChainer extends EthChainer {
  constructor (chain = 'ROPSTEN', socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance = (b) => b, _initOptions) {
    super(chain, socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance, _initOptions);

    this._isTestnet = true;

    this.nokuTokenAddress = RopstenConstants.nokuTokenAddress;
    this.eurnTokenAddress = RopstenConstants.eurnTokenAddress;

    this.setChainInfo(RopstenConstants.chainInfo);
  }

  getExternalExplorerLink (hash) {
    return `https://ropsten.etherscan.io/tx/${hash}`;
  }
}

export default RopstenChainer;
