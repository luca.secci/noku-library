import Chainer from './Chainer';

// ETH LIKE
import ETHChainer from './ETHLike/EthChainer';
import RopstenChainer from './ETHLike/RopstenChainer';
import NokuChainChainer from './ETHLike/NokuChainChainer';
import NokuChainTestChainer from './ETHLike/NokuChaintestChainer';
import NokuChain2Chainer from './ETHLike/NokuChain2Chainer';
import NokuChain2TestChainer from './ETHLike/NokuChain2testChainer';

// BTC LIKE
import BTCChainer from './BTCLike/BtcChainer';
import BTCTestChainer from './BTCLike/BtctestChainer';
import BCHChainer from './BTCLike/BCHChainer';
import BCHTestChainer from './BTCLike/BCHTestChainer';
import DashChainer from './BTCLike/DashChainer';
import DashTestChainer from './BTCLike/DashtestChainer';
import LTCChainer from './BTCLike/LTCChainer';
import LTCTestChainer from './BTCLike/LTCTestChainer';
import ZECChainer from './BTCLike/ZECChainer';
import ZECTestChainer from './BTCLike/ZECTestChainer';

// WAVES
import WavesChainer from './WAVES/WavesChainer';
import WavesTestChainer from './WAVES/WavesTestChainer';

// XMR
import XMRChainer from './XMR/XmrChainer';

export {
  Chainer,
  // ETH LIKE
  ETHChainer,
  RopstenChainer,
  NokuChain2Chainer,
  NokuChain2TestChainer,
  NokuChainChainer,
  NokuChainTestChainer,
  // BTC LIKE
  BTCChainer,
  BTCTestChainer,
  BCHChainer,
  BCHTestChainer,
  DashChainer,
  DashTestChainer,
  LTCChainer,
  LTCTestChainer,
  ZECChainer,
  ZECTestChainer,
  // WAVES
  WavesChainer,
  WavesTestChainer,
  // XMR
  XMRChainer
};
