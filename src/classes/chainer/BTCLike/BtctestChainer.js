import BtcChainer from './BtcChainer';
import { BTCTestConstants } from '../../functions/getChainConstants';

class BTCTestChainer extends BtcChainer {
  constructor (chain = 'BTCTEST', socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance) {
    super(chain, socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance);

    this._isTestnet = true;

    this.setChainInfo(BTCTestConstants.chainInfo);
  }
}

export default BTCTestChainer;
