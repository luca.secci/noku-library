import { BCHTestConstants } from '../../functions/getChainConstants';
import BCHChainer from './BCHChainer';

class BCHTestChainer extends BCHChainer {
  constructor (chain = 'BCHTEST', socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance) {
    super(chain, socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance);

    this._isTestnet = true;

    this.setChainInfo(BCHTestConstants.chainInfo);
  }

  getExternalExplorerLink (transactionId) {
    return `https://www.blockchain.com/bch-testnet/tx/${transactionId}`;
  }
}

export default BCHTestChainer;
