import LTCChainer from './LTCChainer';
import { LTCTestConstants } from '../../functions/getChainConstants';

class LTCTestChainer extends LTCChainer {
  constructor (chain = 'LTCTEST', socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance) {
    super(chain, socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance);

    this._isTestnet = true;
    this.setChainInfo(LTCTestConstants.chainInfo);

    this.setWalletTypes(LTCTestConstants.walletTypes);
    super.initWalletTypes();
  }
}

export default LTCTestChainer;
