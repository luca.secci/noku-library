import BTCChainer from './BtcChainer';
import { DashConstants } from '../../functions/getChainConstants';

class DashChainer extends BTCChainer {
  constructor (chain = 'DASH', socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance) {
    super(chain, socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance);

    this.setChainInfo(DashConstants.chainInfo);

    this.setWalletTypes(DashConstants.walletTypes);
    super.initWalletTypes();
  }
}

export default DashChainer;
