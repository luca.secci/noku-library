import WavesChainer from './WavesChainer';
import { WavesTestConstants } from '../../functions/getChainConstants';

class WavesTestChainer extends WavesChainer {
  constructor (chain = 'WAVESTEST', socketEndpoint, socketSwapEndpoint, _getBalance, _estimateFees, _sendRawTransaction, _sendRawTransactionWithParams, _getAddressByAlias, _getTransaction) {
    super(chain, socketEndpoint, socketSwapEndpoint, _getBalance, _estimateFees, _sendRawTransaction, _sendRawTransactionWithParams, _getAddressByAlias, _getTransaction);
    this._isTestnet = true;

    this.setChainInfo(WavesTestConstants.chainInfo);
  }

  getExternalExplorerLink (txId) {
    return `https://wavesexplorer.com/testnet/tx/${txId}`;
  }
}

export default WavesTestChainer;
