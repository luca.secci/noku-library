import React from 'react';
import { useState } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { action } from '@storybook/addon-actions';

import Button from '../Input/Button/Button';

import DrawerNavigator from "../Layout/DrawerNavigator/DrawerNavigator";

const drawerNavigatorStory = {
  title: "Drawer Navigator",
  component: DrawerNavigator,
};

const appMenuItems = [
  {
    destination: "/app/somewhere",
    title: "Somewhere",
    linkType: "internal",
    onItemClick: action("click")
  }, {
    destination: "/app/somewhere-else",
    title: "Somewhere Else",
    linkType: "internal",
    onItemClick: action("click")
  }, {
    destination: "/here",
    title: "Here",
    linkType: "internal",
    onItemClick: action("click")
  }
];

const menuItems = [
  {
    destination: '/',
    title: 'Home',
    linkType: 'internal',
    onItemClick: action("click")
  },
  {
    destination: '/test',
    title: 'Something',
    linkType: 'internal',
    onItemClick: action("click")
  },
  {
    destination: 'https://google.com',
    title: 'Google',
    linkType: 'external',
    onItemClick: action("click")
  }
];


export const DrawerNavigatorDemo = () => {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);

  return (
    <Router>
      <Button onClick={() => setIsDrawerOpen(prev => !prev)}>
        Open/Close
      </Button>
      <DrawerNavigator
        accountId="60a65e98e38ef4513a7362fe"
        isOpen={isDrawerOpen}
        onDrawerClose={() => setIsDrawerOpen(false)}
        //accountAvatar="https://picsum.photos/200"
        accountFullName="Ponzio Pelato"
        accountEmail="ponzio.pelato@spqr.ro"
        appMenuItems={appMenuItems}
        otherMenuItems={menuItems}
      />
    </Router>
  );
};

export default drawerNavigatorStory;
