import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import Footer from '../Layout/Footer/Footer';
import { ReactComponent as Logo } from "../assets/images/logo/logo_colored.svg";
import { ReactComponent as NewMediumIcon } from "../assets/images/icons/medium.svg";

const FooterStory = {
  title: "Footer"
};

const socialLinks = [
  { key: "twitter", link: "https://google.com", Icon: FontAwesomeIcon, IconProps: { icon: ["fab", "twitter"] }},
  { key: "facebook", link: "https://google.com", Icon: FontAwesomeIcon, IconProps: { icon: ["fab", "facebook-f"] }},
  { key: "telegram", link: "https://google.com", Icon: FontAwesomeIcon, IconProps: { icon: ["fab", "telegram"] }},
  { key: "medium", link: "https://google.com", Icon: NewMediumIcon, IconProps: {}},
];

export const FooterComponent = () => {
  return (
    <div>
      <p>Lorem ipsum</p>
      <Footer Logo={Logo} socialLinks={socialLinks} />
    </div>
  );
};

export default FooterStory;
