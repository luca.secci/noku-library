import { addDays, addHours, endOfDay, parseISO, startOfDay, subDays, subHours, subYears } from "date-fns";
import React, { useState } from "react";
import DatePicker from "../Input/DatePicker/DatePicker";

export default {
  title: "DatePicker",
};

export const SingleDatePicker = () => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  return (
    <DatePicker
      changed={(_, value) => setSelectedDate(value)}
      stateKey="selectedDate"
      selected={selectedDate}
      isInline={true}
      startDate={selectedDate}
      selectsStart={true}
      selectsEnd={false}
    />
  );
};

export const DateRange = () => {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  return (
    <div>
      <DatePicker
        changed={(_, value) => setStartDate(value)}
        selected={startDate}
        isInline={true}
        startDate={startDate}
        selectsStart={true}
        selectsEnd={false}
      />
      <DatePicker
        changed={(_, value) => setEndDate(value)}
        selected={endDate}
        isInline={true}
        startDate={endDate}
        selectsStart={false}
        selectsEnd={true}
        minDate={startDate}
      />
    </div>
  );
};

const MINIMUM_SELECTABLE_DATE = parseISO("2021-08-22Z0");
const DEFAULT_HOUR_RANGE = 24;

export const DateTime = () => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  return (
    <DatePicker
      changed={(_, value) => setSelectedDate(value)}
      stateKey="selectedDate"
      selected={selectedDate}
      isInline={true}
      startDate={selectedDate}
      selectsStart={true}
      selectsEnd={false}
      showTimeSelect={true}
      minDate={MINIMUM_SELECTABLE_DATE}
      maxDate={subHours(new Date(), DEFAULT_HOUR_RANGE)}
      minTime={MINIMUM_SELECTABLE_DATE}
      maxTime={subHours(new Date(), DEFAULT_HOUR_RANGE)}
      timeIntervals={60}
      locale={"en"}
    />
  );
};
