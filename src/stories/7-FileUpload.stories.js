import React, { useState } from 'react';
import FileUpload from '../Input/FileUpload/FileUpload';

export default {
  title: 'FileUpload'
};

const LimitWidth = (props) => <div className="max-width-20rem"> {props.children} </div>;

export const FileUploadDemo = () => {
  const [file, setFile] = useState({});
  const [error, setError] = useState(false);
  return (
    <LimitWidth>
      <FileUpload
        dropped={setFile}
        dropRejected={setError}
        showPreview={true}
        title="Drop your image here"
        subtitle="we accept: JPG. PNG, GIF"
        hasImagePreview={false}
      />
    </LimitWidth>
  );
};
