import React from 'react';
import Button from '../Input/Button/Button';

import PopupAlert from '../Output/PopupAlert/PopupAlert';

export default {
  title: 'Popup Alert'
};

export const SuccessPopup = () => {
  const actions = <>
    <Button color="confirm" uppercase> Ok </Button>
  </>;

  return (
    <PopupAlert actions={actions} isOpen={true} title="Success!" description="Some action has been completed successfully" />
  );
};

export const WarningPopup = () => {
  const actions = <>
    <Button color="warning" uppercase> Ok </Button>
  </>;

  return (
    <PopupAlert actions={actions} level="warning" isOpen={true} title="Warning!" description="Some action requires your attention" />
  );
};

export const ErrorPopup = () => {
  const actions = <>
    <Button color="error" uppercase> Ok </Button>
  </>;

  return (
    <PopupAlert actions={actions} level="error" isOpen={true} title="Error!" description="Some action has failed horribly! Save yourself!" />
  );
};
