import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { action } from '@storybook/addon-actions';

import { ReactComponent as Logo } from "../assets/images/logo/logo_colored.svg";

import NokuHeader from "../Layout/NokuHeader/NokuHeader";
import User from '../Output/User/User';

const NokuHeaderStory = {
  title: "Noku Header"
};

const menuItems = [
  {
    destination: '/',
    title: 'Home',
    linkType: 'internal'
  },
  {
    destination: '/test',
    title: 'Something',
    linkType: 'internal'
  },
  {
    destination: '/test',
    title: 'Force Active',
    linkType: 'internal',
    forceActive: true
  },
  {
    destination: 'https://google.com',
    title: 'Google',
    linkType: 'external'
  }
];

export default NokuHeaderStory;

export const NokuHeaderDemo = () => {

  return (
    <Router>
      <NokuHeader
        logoSrc="https://via.placeholder.com/70x25/0000FF"
        reverseLogoSrc="https://via.placeholder.com/70x25"
        items={menuItems}
        onHamburgerClick={action('hamburger click')}
        rightSlot={(
          <User
            nickname="Ponzio Pelato"
            email="ponzio.pelato@spqr.ro"
            // avatar={avatar}
            avatar="https://picsum.photos/200"
            kycImage="https://via.placeholder.com/50x67"
            kycLevel={3}
            handleLogout={(e) => { console.log(e) }}
          />
        )}
      />
    </Router>
  );
};

export const NokuHeaderWithLogoComponent = () => {
  return (
    <Router>
      <NokuHeader
        // logoSrc="https://via.placeholder.com/70x25/0000FF"
        // reverseLogoSrc="https://via.placeholder.com/70x25"
        logoComponent={Logo}
        items={menuItems}
        onHamburgerClick={action('hamburger click')}
        rightSlot={(
          <User
            nickname="Ponzio Pelato"
            email="ponzio.pelato@spqr.ro"
            // avatar={avatar}
            avatar="https://picsum.photos/200"
            kycImage="https://via.placeholder.com/50x67"
            kycLevel={3}
            showLanguage={true}
          />
        )}
      />
    </Router>
  );
};

export const NokuHeaderWithGeneratedUserAvatar = () => {
  return (
    <Router>
      <NokuHeader
        // logoSrc="https://via.placeholder.com/70x25/0000FF"
        // reverseLogoSrc="https://via.placeholder.com/70x25"
        logoComponent={Logo}
        items={menuItems}
        onHamburgerClick={action('hamburger click')}
        rightSlot={(
          <User
            userId="5fc0b55bd177790eb830b9c5"
            nickname="Ponzio Pelato"
            email="ponzio.pelato@spqr.ro"
            // avatar={avatar}
            // avatar="https://picsum.photos/200"
            kycImage="https://via.placeholder.com/50x67"
            kycLevel={3}
            showLanguage={true}
            handleLogout={(e) => { console.log(e) }}
          />
        )}
      />
    </Router>
  );
};
