import React from 'react';
import CryptoHeroesModal from '../Output/CryptoHeroesModal/CryptoHeroesModal';
import CurrentTokenStatus from '../utils/cryptoHeroes/CurrentTokenStatus';

const CryptoHeroesModalStory = {
  title: "Crypto Heroes Modal"
};

export const CryptoHeroesModalComponent = () => {

  const metadata = {
    "name": "COVID",
    "description": "COVID-19",
    "image": "https://static.noku.io/nftstorage/images/0xfb574de3c071ac675a956a1d9d295aa2a04303ba/14UW83BeDeQf6CNkCsqCb7tWt4LaY.png",
    "external_uri": "",
    "attributes": {
      "T": "A",
      "W": "19",
      "Q": "1",
      "R": "B"
    }
  };

  const tokenId = "8000052000000284@0xfb574de3c071ac675a956a1d9d295aa2a04303ba";

  return <CryptoHeroesModal metadata={metadata} id={tokenId} isOpen={true} />;
};


export const CryptoHeroesModalComponentWithMarketplace = () => {

  const metadata = {
    "name": "COVID",
    "description": "COVID-19",
    "image": "https://static.noku.io/nftstorage/images/0xfb574de3c071ac675a956a1d9d295aa2a04303ba/14UW83BeDeQf6CNkCsqCb7tWt4LaY.png",
    "external_uri": "",
    "attributes": {
      "T": "A",
      "W": "19",
      "Q": "1",
      "R": "B"
    }
  };

  const tokenId = "8000052000000284@0xfb574de3c071ac675a956a1d9d295aa2a04303ba";

  return <CryptoHeroesModal metadata={metadata} id={tokenId} isOpen={true} withMarketplace={true} tokenStatus={CurrentTokenStatus.CryptoHeroes} openMarketPlaceModal={() => console.log("hi there")} />;
};


export default CryptoHeroesModalStory;
