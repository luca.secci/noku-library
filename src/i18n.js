import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import detector from "i18next-browser-languagedetector";
import { registerLocale } from "react-datepicker";
import { enGB as en, it, ko } from "date-fns/locale";

import translationEN from "./assets/locales/en/translation.json";
import translationIT from "./assets/locales/it/translation.json";
import translationKO from "./assets/locales/ko/translation.json";

export const i18nInit = (appNameSpace = "default", appTranslationEN = {}, appTranslationIT = {}, appTranslationKO = {}) => {
  const resources = {
    en: {
      "noku-library": translationEN,
      [appNameSpace]: appTranslationEN,
    },
    it: {
      "noku-library": translationIT,
      [appNameSpace]: appTranslationIT,
    },
    ko: {
      "noku-library": translationKO,
      [appNameSpace]: appTranslationKO,
    },
  };

  registerLocale("en", en);
  registerLocale("it", it);
  registerLocale("ko", ko);

  return i18n
    .use(detector)
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
      resources,
      fallbackLng: ["en", "it", "ko"],
      // lng: 'it',
      keySeparator: false, // we do not use keys in form messages.welcome
      interpolation: {
        escapeValue: false, // react already safes from xss
      },
    });
};

export default i18nInit;
