import React from 'react';

/**
 * Card
 * @prop {Function} onClick on card click callback
 * @prop {boolean} clickable=false if true will apply additional style to the card
 * @prop {boolean} shadow=true if true will apply a box-shadow
 * @prop {string} bodyClassName="" additional card classes
 * @prop {Object} bodyStyle={} additional card style
 */
const Card = React.forwardRef(({ onClick = () => null, clickable = false, shadow = true, bodyClassName = '', bodyStyle = {}, children, ...props }, ref) => (
  <div
    className={`noku-library-card-body ${clickable ? 'clickable' : ''} ${shadow ? 'shadow' : ''} ${bodyClassName}`}
    onClick={onClick}
    ref={ref}
    style={bodyStyle}
    {...props}
  >
    {children}
  </div>
));

Card.displayName = "Card";
export default Card;
