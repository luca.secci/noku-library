import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Badge from '@material-ui/core/Badge';

import classes from './GenericAvatar.module.scss';

const withBadge = (Avatar, badgeContent, badgeClassName) => (
  <Badge
    overlap="circle"
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right'
    }}
    badgeContent={badgeContent}
    className={badgeClassName}
  >
    {Avatar}
  </Badge>
);

const GenericAvatar = ({ avatarClassName = '', avatarStyle = {}, size = 'medium', image, hasBadge = false, badgeContent, badgeClassName, children, imageSize = '', ...props }) => {
  const InnerAvatar = (
    <Avatar className={`${classes.muiCustom} ${classes.genericAvatar} ${size === 'medium' ? classes.medium : classes.small} ${imageSize === 'cover' ? classes.cover : ''} ${avatarClassName}`} style={avatarStyle} src={image}>
      {children}
    </Avatar>
  );

  return hasBadge ? withBadge(InnerAvatar, badgeContent, badgeClassName) : InnerAvatar;
};

export default GenericAvatar;
