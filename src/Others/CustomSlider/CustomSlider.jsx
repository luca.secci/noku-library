import React from 'react';
import ViewSlider from 'react-view-slider';

import classes from './CustomSlider.module.scss';

const baseSettings = {
  spacing: 1.1,
  keepViewsMounted: true, // avoid some height problems
  animateHeight: false,
  fillParent: false, // avoid true otherwise it will fill all the space
  transitionDuration: 500 // default speed
};

export const CustomSlider = props => {
  const renderView = ({ index, active, transitionState }) =>
    props.steps ? props.steps[index] : <> </>;

  return (
    <>
      {props.beforeSlider}
      <ViewSlider
        {...baseSettings}
        {...props.settings}
        activeView={props.activeStep}
        renderView={renderView}
        numViews={props.steps.length}
        viewportClassName={!props.isModal ? classes.centerViewport : ''}
        innerViewWrapperStyle={!props.isModal && props.slideWidth ? { minWidth: props.slideWidth } : {}}
      />
      {props.afterSlider}
    </>
  );
};

export default CustomSlider;
