import React from 'react';

/**
 * CroppedBordersCard
 * @prop {Function} onClick on card click callback
 * @prop {string} bodyClassName="" additional card classes
 * @prop {Object} bodyStyle={} additional card style
 */
const CroppedBordersCard = React.forwardRef(({ onClick = () => null, clickable = false, shadow = true, bodyClassName = '', bodyStyle = {}, ...props }, ref) => (
  <div className={`noku-library-cropped-borders-card-body ${bodyClassName}`} onClick={onClick} ref={ref} style={bodyStyle}> {props.children} </div>
));

export default CroppedBordersCard;
