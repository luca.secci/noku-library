import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import { makeStyles } from '@material-ui/core/styles';
import Button from '../../Input/Button/Button';

import classes from './BottomSheetDialog.module.scss';

const BottomSheetDialog = ({ isVisible, title, onBackdropClick, showCancel = true, cancelText = 'Cancel', zIndex = 2, children }) => {
  const useStyles = makeStyles(() => ({
    backdrop: {
      zIndex
    }
  }));
  const backdropClasses = useStyles();

  return (
    <div className={`${classes.dialogWrapper} ${isVisible ? classes.visible : ''}`} style={{ zIndex }}>
      <Backdrop className={backdropClasses.backdrop} open={isVisible} onClick={onBackdropClick} />
      <div className={`${classes.bottomSheetDialogWrapper} ${isVisible ? classes.visible : ''}`} style={{ zIndex: zIndex + 1 }}>
        <div className={classes.header}>
          <div className={classes.draggableZone}></div>
          <div className={classes.title}> <span>{title}</span>{showCancel ? <Button style="flat" onClick={onBackdropClick}> {cancelText} </Button> : <> </>}</div>
        </div>
        <div className={classes.body}>
          {children}
        </div>
      </div>
    </div>
  );
};

export default BottomSheetDialog;
