import { useCallback, useEffect, useRef } from 'react';
import TagManager from 'react-gtm-module';

const updateTagManager = (user) => {
  TagManager.dataLayer({
    dataLayer: {
      event: "pageView",
      userId: user?.id
    }
  });
};

const useGoogleTagManager = (history, user, tagManagerCode) => {
  const initialized = useRef(false);
  const unregister = useRef();

  const registerListener = useCallback(() => {
    const unregisterCall = history.listen(() => {
      updateTagManager(user);
    });

    unregister.current = unregisterCall;
  }, [history, user]);

  useEffect(() => {
    if(!initialized.current) {
      return false;
    }
    if(unregister?.current) {
      unregister.current();
      unregister.current = null;
    }
    registerListener();
    
  }, [user, unregister, registerListener]);


  useEffect(() => {
    if (!tagManagerCode || initialized.current) {
      return false;
    }
    TagManager.initialize({
      gtmId: tagManagerCode
    });
    initialized.current = true;

    updateTagManager(user);
    registerListener();

  }, [tagManagerCode, history, user, registerListener]);
};

export default useGoogleTagManager;