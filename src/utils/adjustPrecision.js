import BigNumber from "bignumber.js";

function adjustPrecision(num, maxDecimals = 0, params) {
    if(typeof num === 'string'){
        num = num.replace(/,/g, '');
    }

    let numBN = new BigNumber(num);
    let adjustedBN = numBN.dp(maxDecimals, BigNumber.ROUND_FLOOR);
    let adjusted = parseFloat(adjustedBN.toString());

    const _params = {...params,  maximumFractionDigits: 18 };

    return adjusted.toLocaleString('en-EN', _params).replace(/,/g, "'");
}

export default adjustPrecision;