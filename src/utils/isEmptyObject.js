/**
 * Checks if something is undefined/null or has no properties
 * @param {Object} obj Any JS object
 * @return {boolean} True if object is undefined or no properties
 */
export const isEmptyObject = (obj) => {
  return !obj || (Object.entries(obj).length === 0 && obj.constructor === Object);
};

export default isEmptyObject;
