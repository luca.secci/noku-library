import { createInstance } from "localforage";

const defaultOptions = {
  name: "noku-library",
  storeName: "storage",
  description: "Storage for noku applications",
};

/**
 * Creates a custom storage with some predefined options.
 * @param {LocalForageOptions} options 
 * @returns {LocalForage}
 */
export const makeStorage = (options) => createInstance({...defaultOptions, ...options});

export const CustomStorage = createInstance({...defaultOptions});

export const NFTCacheStorage = createInstance({
  ...defaultOptions,
  storeName: "nft-cache-storage",
  description: "Storage for NFT metadata",
});

export const ExternalWalletStorage = createInstance({
  ...defaultOptions,
  storeName: "external-wallet-storage",
  description: "Storage for external wallets (web3 provider like Metamask/Ledger/Trezor/etc.)",
});

export const DerivedAccountListStorage = createInstance({
  ...defaultOptions,
  storeName: "derived-account-list-storage",
  description: "Storage for derived accounts",
});

