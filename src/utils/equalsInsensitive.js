/**
 * @param {string} a 
 * @param {string} b 
 * @returns true if both strings are equal (case insensitive)
 */
export const equalsInsensitive = (a = "", b = "") => a.toLowerCase() === b.toLowerCase();
