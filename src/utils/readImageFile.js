export const readImageFile = (file) => new Promise((resolve, reject) => {
  const reader = new FileReader();

  reader.onloadend = (e) => resolve(e.target.result);

  if (file) {
    reader.readAsDataURL(file);
  } else {
    reject(new Error('Missing file'));
  }
});