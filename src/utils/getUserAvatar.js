import eth4you from "eth4you";
import { outer, total, base } from "gridy-avatars";

export const getUserAvatar = (avatar, userId) => {
  if (avatar || !userId) {
    return avatar;
  }
  const hashedId = eth4you.sha256(userId).toString(eth4you.CryptoJS.enc.Hex);
  const trunkedHashId = hashedId.slice(0, 4);
  const seed = parseInt(trunkedHashId, 16).toString();
  const avatarId = Math.floor(seed / Math.pow(16, 4) * total + total).toString(base).substring(1);
  const generatedAvatar = outer(avatarId);
  return `data:image/svg+xml;base64, ${window.btoa(generatedAvatar)}`;
};

export default getUserAvatar;
