import pTimeout from 'p-timeout';
import { readImageFile } from '../readImageFile';

/**
 * Reads a blob and returns a base64 encoded string, rejects if file could not be encoded or read.
 * @param {Blob} file
 * @param {Number} timeout
 * @returns {Promise<string>}
 */
export const readFileToBase64 = (file, timeout = 500) => {
  // const fileReader = new FileReader();
  // fileReader.readAsDataURL(file);
  // const loadPromise = new Promise((resolve, reject) => {
  //   fileReader.addEventListener("load", () => {
  //     const result = fileReader.result;
  //     if (result) {
  //       return resolve(result);
  //     }
  //     return reject("Invalid file!");
  //   });
  // });

  return pTimeout(readImageFile(file), timeout);
};
