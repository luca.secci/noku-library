import { addMinutes, format } from "date-fns";
import { enGB as en, it, ko } from "date-fns/locale";

const locales = new Map(
  Object.entries({
    en,
    it,
    ko,
  })
);

export const getDateFnsLocaleFromKey = (searchKey = "en") => locales.get(searchKey);

export const formatWithLocale = (date, formatString, searchKey = "en", additionalOptions = {}) => {
  if (!formatString || !date) {
    throw new Error("Missing date or formatString");
  }
  const locale = getDateFnsLocaleFromKey(searchKey);
  return format(date, formatString, { locale, ...additionalOptions });
};

export const formatUTCWithLocale = (date, formatString, searchKey = "en", additionalOptions = {}) => {
  return formatWithLocale(addMinutes(date, date.getTimezoneOffset()), formatString, searchKey, additionalOptions);
};