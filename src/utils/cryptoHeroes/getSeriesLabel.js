/**
 * Gets the crypto heroes series label
 * @param {"G"|"P"|"H"|string} series The card series from the token attributes
 * @return {"Gold"|"Platinum"|"Historic"|undefined} The label, "Unknown" if it is not recognised
 */
export const getCryptoHeroesSeriesLabel = (series) => {
  let result;
  switch (series) {
    case "G":
      result = "Gold";
      break;
    case "P":
      result = "Platinum";
      break;
    case "H":
      result = "Historic";
      break;
    default:
      // result = "Unknown";
      break;
  }
  return result;
}

/**
 * Gets the crypto heroes series label from the metadata
 * @param {Object} metadata The token metadata
 */
export const getCryptoHeroesSeriesLabelFromMetadata = (metadata = {}) => getCryptoHeroesSeriesLabel(metadata?.attributes?.S);
