export const CurrentTokenStatus = {
  Balance: "InBalance",
  CryptoHeroes: "CryptoHeroes",
  Marketplace: "Marketplace"
};

export default CurrentTokenStatus;