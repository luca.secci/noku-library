/**
 * Gets the crypto heroes rarity label
 * @param {"B"|"R"|"UR"|"UQ"|string} rarity The card rarity from the token attributes
 * @return {"Basic"|"Rare"|"Ultra-Rare"|"Unique"|undefined} The label, "Unknown" if it is not recognised
 */
export const getCryptoHeroesRarityLabel = (rarity) => {
  let result;
  switch (rarity) {
    case "B":
      result = "Basic";
      break;
    case "R":
      result = "Rare";
      break;
    case "UR":
      result = "Ultra-Rare";
      break;
    case "UQ": 
      result = "Unique";
      break;
    default:
      // result = "Unknown";
      break;
  }
  return result;
}

/**
 * Gets the crypto heroes rarity label from the metadata
 * @param {Object} metadata The token metadata
 */
export const getCryptoHeroesRarityLabelFromMetadata = (metadata = {}) => getCryptoHeroesRarityLabel(metadata?.attributes?.R);
