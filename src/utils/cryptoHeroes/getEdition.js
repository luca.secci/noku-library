/**
 * Get the year from the edition
 * @param {number} edition The edition number
 */
export const getCryptoHeroesEditionYear = (edition = 0) => edition + 2020;

/**
 * Gets the edition year from the token metadata
 * @param {Object} metadata The token metadata
 */
export const getCryptoHeroesEditionYearFromMetadata = (metadata = {}) => {
  let edition = metadata?.attributes?.Q ?? 0;
  if (typeof edition === "string") {
    edition = parseInt(edition);
  }
  return getCryptoHeroesEditionYear(edition);
};
