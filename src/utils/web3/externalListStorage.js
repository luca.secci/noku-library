import { ExternalWalletStorage } from "../customStorage";
import { getProviderByType, validateProvider } from "./providers";

/*
const exampleExternalWallet = {
  provider: "metamask" || "ledger",
  address: "0x...",
};
*/

const baseKey = "ExternalWallet";

/**
 * @typedef {Object} ExternalWallet
 * @property {string} address - The wallet address
 * @property {number} accountIndex - The account index, if the provider supports multiple addresses
 * @property {ExternalProvider} provider - The wallet provider
 */

const getStorageKey = (address = "") => `${baseKey}-${address.toLocaleLowerCase()}`;

/**
 * Use this function to safely create a new ExternalWallet instance
 * @param {string} providerType The type of the provider
 * @param {string} address The wallet address
 * @param {number} accountIndex The account index
 * @return {ExternalWallet}
 */
export const createExternalWallet = (providerType, address, accountIndex = 0) => {
  if (!providerType) {
    throw new Error('Missing external wallet provider');
  }
  if (!address) {
    throw new Error('Missing external wallet address');
  }
  if (accountIndex === null || accountIndex === undefined) {
    throw new Error(`Missing external wallet account index`);
  }
  const provider = getProviderByType(providerType);
  if (!provider) {
    throw new Error(`Unsupported provider: ${providerType}`);
  }
  return { provider, address, accountIndex };
};

/**
 * If the same wallet is already in the list, it will be overriden
 * @param {ExternalWallet} externalWallet 
 */
export const addWalletToExternalList = async (externalWallet) => {
  try {
    if (!externalWallet) {
      throw new Error('No external wallet');
    }
    if (!externalWallet.provider) {
      throw new Error('Missing external wallet provider');
    }
    if (!externalWallet.address) {
      throw new Error('Missing external wallet address');
    }
    if (externalWallet.accountIndex === null || externalWallet.accountIndex === undefined) {
      throw new Error(`Missing external wallet account index`);
    }
    const provider = validateProvider(externalWallet.provider);
    const { accountIndex } = externalWallet;
    let { address } = externalWallet;
    address = address.toLocaleLowerCase();
    return await ExternalWalletStorage.setItem(getStorageKey(address), { accountIndex, provider, address });
  } catch (error) {
    console.error(error);
  }
};

/**
 * @param {string} address 
 * @returns {Promise<ExternalWallet>}
 */
export const getWalletFromExternalList = async (address) => {
  try {
    if (!address) {
      throw new Error("Missing address");
    }
    return await ExternalWalletStorage.getItem(getStorageKey(address));
  } catch (error) {
    console.error(error);
    return null;
  }
};

/**
 * @param {string} address 
 * @returns {Promise<boolean>}
 */
 export const isWalletInExternalList = async (address) => {
  try {
    return !!(await getWalletFromExternalList(address));
  } catch (error) {
    console.error(error);
    return false;
  }
};

/**
 * @param {string} address 
 */
export const removeWalletFromExternalList = async (address) => {
  try {
    if (!address) {
      throw new Error("Missing address");
    }
    return await ExternalWalletStorage.removeItem(getStorageKey(address));
  } catch (error) {
    console.error(error);
  }
};
