import { equalsInsensitive } from "../equalsInsensitive";

/**
 * @enum {string}
 */
export const ProviderType = {
  Metamask: "Metamask",
  Ledger: "Ledger",
};

/**
 * @typedef {Object} ExternalProvider
 * @property {ProviderType} type - The type of the external provider.
 * @property {boolean} supportMultipleAddresses - Wether this provider supports HD accounts or derived accounts.
 * @property {boolean} isHardwareWallet - Wether this provider is physical.
 */

/**
 * @type {ExternalProvider[]}
 */
export const SupportedProviders = [
  {
    type: ProviderType.Metamask,
    supportMultipleAddresses: false,
    isHardwareWallet: false,
  },
  {
    type: ProviderType.Ledger,
    supportMultipleAddresses: true,
    isHardwareWallet: true,
  },
];

/**
 * @param {string} type 
 */
export const getProviderByType = (type) => SupportedProviders.find((provider) => equalsInsensitive(provider.type, type));

/**
 * 
 * @param {any} provider An unknown provider object.
 * @returns {ExternalProvider}
 */
export const validateProvider = (provider) => {
  if (!(Object.values(ProviderType).includes(provider.type))) {
    throw new Error('Provider type is invalid');
  }
  if (typeof provider.supportMultipleAddresses !== "boolean") {
    throw new Error('Provider support multiple addresses must be a boolean');
  }
  if (typeof provider.isHardwareWallet !== "boolean") {
    throw new Error('Provider is hardware wallet must be a boolean');
  }
  return provider;
};
