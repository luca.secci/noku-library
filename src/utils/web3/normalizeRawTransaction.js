/*
Reference:
https://docs.ethers.io/v5/api/providers/types/#providers-TransactionRequest
*/

/**
 * 
 * @param {*} rawTransaction 
 * @param {number} chainId the chain id
 * @param {import("./providers").ExternalProvider} provider
 * @returns Ethers compatible UnsignedTransaction
 */
export const normalizeRawTransaction = (rawTransaction, chainId, provider) => {
  const { normalValue, customChain, gasPrice, data: oldData, gasLimit: oldGasLimit, ...rawTx } = rawTransaction;

  const data = oldData || "0x";
  const gasLimit = "0x" + oldGasLimit.toString(16);

  return {
    ...rawTx,
    data,
    gasLimit,
  };
};
