import isAscii from 'validator/es/lib/isAscii';

export const isSolidityCompliant = (text) => {
  return (isAscii(text) && (text.length < 256));
};

export default isSolidityCompliant;
