import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Select from '../Select/Select';
import Checkbox from '../Checkbox/Checkbox';

export const BaseInput = React.forwardRef(({
  elementConfig = {}, 
  changed = () => null, 
  disabled = false, 
  defaultValue, 
  stateKey, 
  value, 
  type, 
  placeholder, 
  label, 
  labelClassName,
  beforeIcon, 
  beforeIconClassName, 
  afterIcon, 
  afterIconClassName, 
  error, 
  errorMessage = '', 
  wrapperClassName, 
  inputClassName, 
  blurred = () => null, 
  inputContainerClassName,
  rounded = false, 
  fluid, 
  required, 
  showRequiredAsterisk = true,
  showPasswordSwitch = false,
  ...props
}, ref) => {
  let InputElement = <> </>;

  const [showPassword, setShowPassword] = useState(false);

  const generalConfig = {
    ...elementConfig,
    value,
    onChange: (e) => changed(stateKey, e?.target?.value ?? e, e),
    onBlur: (e) => blurred(stateKey, e?.target?.value ?? e, e),
    className: `input ${inputClassName}`,
    disabled,
    defaultValue,
    placeholder,
    required
  };

  switch (type) {
    case 'textarea':
      InputElement = <textarea ref={ref} {...generalConfig} />;
      break;
    case 'select':
      InputElement = <Select ref={ref} {...generalConfig} />;
      break;
    // MAYBEDO
    case 'checkbox':
      InputElement = <Checkbox {...generalConfig} />;
      break;
    case 'password':
      InputElement = <input ref={ref} type={showPassword ? 'text' : 'password'} {...generalConfig} />;
      break;
    default:
      InputElement = <input ref={ref} type={type} {...generalConfig} />;
      break;
  }

  const Label = label ? (
    <label className={`label ${labelClassName}`}>
      {label}{showRequiredAsterisk && required ? '*' : '' }
    </label>
  ) : <> </>;

  const BeforeIcon = beforeIcon ? (
    <span className={`icon before-icon ${beforeIconClassName}`}>
      {beforeIcon}
    </span>
  ) : <> </>;

  let AfterIcon = <> </>;
  if (afterIcon) {
    AfterIcon = (
      <span className={`icon after-icon ${afterIconClassName}`}>
        {afterIcon}
      </span>
    );
  } else if (showPasswordSwitch) {
    AfterIcon = (
      <span className={`icon after-icon ${afterIconClassName}`} onClick={() => setShowPassword(!showPassword)}>
        <FontAwesomeIcon icon={['fas', showPassword ? 'eye-slash' : 'eye']} />
      </span>
    );
  }

  const ErrorMessage = (
    <div className={'error-message'}>{errorMessage}</div>
  );

  return (
    <div className={`noku-library-base-input-wrapper ${error ? 'error' : ''} ${fluid ? 'fluid' : ''} ${rounded ? 'rounded' : ''} ${disabled ? 'disabled' : ''} ${type} ${wrapperClassName}`}>
      {Label}
      <div className={`input-container ${type} ${inputContainerClassName}`}>
        {BeforeIcon}
        {InputElement}
        {AfterIcon}
      </div>
      {ErrorMessage}
    </div>
  );
});

export default BaseInput;
