import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const LargeButton = ({ onClick, isActive, href, icon, text }) => (
  <button onClick={onClick} className={`noku-library-large-button large-button ${isActive ? 'active' : ''}`}>
    {href ? <a href={href} className='link'>&nbsp;</a> : null}
    <div className='text'>
      {icon ? <div className={'icon'}><FontAwesomeIcon icon={icon} /></div> : null}
      <span> {text} </span>
    </div>
    <span className='chevron'>
      <FontAwesomeIcon icon={'angle-right'} />
    </span>
  </button>
);

export default LargeButton;
