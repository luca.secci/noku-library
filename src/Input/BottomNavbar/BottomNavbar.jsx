import React from 'react';

const FloatingActionButton = ({ content, onClick }) => (
  <>
    <div className="noku-library-floating-action-button" onClick={onClick}>
      {content}
    </div>
    {/* Fix spacing between menu items */}
    <div className="nav-item nav-item-casper" />
  </>
);

const BottomNavbar = ({ menuItems = [], bottomNavbarClassName, itemClicked = () => null, autoWidth = false }) => {
  const hasFab = menuItems.find((item) => item.isFab);
  let basicStyle = {};
  if(autoWidth) {
    basicStyle = { width: `${100 / menuItems.length}%` };
  }

  return (
    <div className={`noku-library-bottom-navbar ${hasFab ? 'fab-cutout' : ''} ${bottomNavbarClassName}`}>
      {
        menuItems.map(({ content, isActive, isFab, style }, index) => isFab ? <FloatingActionButton key={`bottom_navbar_item_${index}`} content={content} onClick={(_) => itemClicked(index)} /> : (
          <div key={`bottom_navbar_item_${index}`} className={`nav-item${basicStyle.width ? '' : ' no-auto-width'}`} onClick={(_) => itemClicked(index)} style={basicStyle}>
            <div className={`nav-item-content ${isActive ? 'active' : ''}`} style={style ?? {}}>
              {content}
            </div>
          </div>
        ))
      }
    </div>
  );
};

export default BottomNavbar;
