import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Avatar } from "@material-ui/core";
import React, { useCallback, useMemo, useState } from "react";
import { useDropzone } from "react-dropzone";
import { useTranslation } from "react-i18next";

import { readImageFile } from "../../utils/readImageFile";
import Button from "../Button/Button";

const FileUpload = ({
  multiple = false,
  acceptedFiles = "image/jpg, image/jpeg, image/png",
  maxSize = 5 * 1024 * 1024,
  dropped = () => { },
  dropRejected = () => { },
  showPreview = false,
  title = "",
  subtitle = "",
  className = "",
  hasImagePreview = true
}) => {
  const { t } = useTranslation("noku-library");
  const [uploadSrc, setUploadSrc] = useState();
  const [fileName, setFileName] = useState();
  const getImageSrc = useCallback(async (file) => {
    try {
      const imgSrc = await readImageFile(file);
      setUploadSrc(imgSrc);
      setFileName(file?.name);
    } catch (error) {
      console.error(error);
    }
  }, []);

  const onDrop = useCallback(
    (acceptedFiles) => {
      getImageSrc(acceptedFiles[0]);
      dropped(multiple ? acceptedFiles : acceptedFiles[0]);
    },
    [dropped, getImageSrc, multiple]
  );

  const onDropRejected = useCallback(
    (rejectedFiles) => {
      dropRejected(rejectedFiles);
      setUploadSrc();
    },
    [dropRejected]
  );

  const { getRootProps, getInputProps, isDragActive, isDragReject } =
    useDropzone({
      multiple,
      acceptedFiles,
      maxSize,
      onDrop,
      onDropRejected,
    });

  const UploaderBody = useMemo(() => {
    let Preview = <></>;
    if (hasImagePreview) {
      Preview = <Avatar classes={{ root: "avatar" }}><img src={uploadSrc} alt="preview" /></Avatar>;
    } else {
      Preview = <div className={"file-name"}>{fileName}</div>;
    }

    if (showPreview && uploadSrc) {
      return (
        <>
          {Preview}
          <Button
            buttonClassName={"button"}
            startIcon={<FontAwesomeIcon icon={"folder"} />}
            startIconClassName={"icon"}
            color="gradient"
          >
            {t("changeFile")}
          </Button>
        </>
      );
    } else {
      return (
        <>
          <div className={"title"}>{title}</div>
          <div className={"subtitle"}>{subtitle}</div>
          <Button
            buttonClassName={"button"}
            startIcon={<FontAwesomeIcon icon={"folder"} />}
            startIconClassName={"icon"}
            color="gradient"
          >
            {t("chooseFile")}
          </Button>
        </>
      );
    }
  }, [fileName, hasImagePreview, showPreview, subtitle, title, uploadSrc]);

  return (
    <div
      {...getRootProps()}
      className={`noku-library-file-upload file-upload ${className} ${isDragActive ? "dropping" : ""
        } ${isDragReject ? "rejecting" : ""}`}
    >
      {UploaderBody}
      <input {...getInputProps()} />
    </div>
  );
};

export default FileUpload;
