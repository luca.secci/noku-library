import React from "react";
import { FormControl, InputLabel, MenuItem, Select as MuiSelect } from "@material-ui/core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SelectIcon = ({ className = "" }) => (
  <FontAwesomeIcon
    className={`${className} select-icon`}
    icon={"chevron-down"}
  />
);

const Select = ({
  onChange = () => {},
  value = "",
  options = [],
  label = "",
  variant = "outlined",
  size = "medium",
  color = "primary",
  className = "",
  labelClassName = "",
  selectClassName = "",
  menuItemClassName = "",
}) => {
  return (
    <FormControl classes={{ root: `noku-library-select ${size} ${variant} ${color} ${className}` }} variant={variant} size={size}>
      <InputLabel classes={{ root: `label ${labelClassName}` }}>{label}</InputLabel>
      <MuiSelect
        value={value}
        onChange={(e) => onChange(e.target.value)}
        label={label}
        IconComponent={SelectIcon}
        classes={{ select: `select ${selectClassName}` }}
      >
        {
          options.map(({key, label, value}) => (
            <MenuItem
              key={`select-option-${key}`}
              classes={{ root: `noku-library-select-menu-item ${menuItemClassName}` }}
              value={value}
            >
              {label}
            </MenuItem>
          ))
        }
      </MuiSelect>
    </FormControl>
  );
};

export default Select;
