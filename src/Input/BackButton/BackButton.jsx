import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * @prop {boolean} hideBack=false if true will hide the button
 * @prop {Function} goBack callback called when button is clicked
 * @prop {string} goBackTitle button text
 * @prop {JSX} icon override arrow-left icon with something else
 * @prop {string} wrapperClassName additional wrapper classes
 * @prop {string} buttonClassName additional button classes
 * @prop {string} titleClassName additional title classes
 * @prop {JSX} children
 */
const BackButton = ({ hideBack = false, goBack, goBackTitle = 'Back', icon, wrapperClassName = '', buttonClassName = '', titleClassName = '', children }) => (
  <>
    {hideBack ? (
      <> </>
    ) : (
      <div className={`noku-library-back-button ${wrapperClassName}`}>
        <div
          className={`back ${buttonClassName}`}
          onClick={goBack}
        >
          {icon ?? (
            <FontAwesomeIcon
              className={'arrow-icon'}
              icon='arrow-left'
            />
          )}
          <span className={`title ${titleClassName}`}>{goBackTitle}</span>
        </div>
        {children}
      </div>
    )}
  </>
);

export default BackButton;
