import React from 'react';
import QrReader from 'react-qr-reader';

const QrReaderComponent = ({ onScan = () => {} }) => {
  return (
    <QrReader
      delay={500}
      onError={(err) => console.log(err)}
      onScan={onScan}
      style={{ width: '100%' }}
    />
  );
};

export default QrReaderComponent;
