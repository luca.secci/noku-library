import React from "react";
import DatePicker from "react-datepicker";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { differenceInCalendarDays, isWithinInterval, startOfDay, endOfDay } from "date-fns";
import { useTranslation } from "react-i18next";
import { formatWithLocale } from "../../utils/datetime/locales";

const DatePickerHeader = ({ date, decreaseMonth, increaseMonth, minDate, locale }) => (
  <div className={"custom-header"}>
    <span className={`prev-button ${differenceInCalendarDays(date, minDate) < 0 ? "disable-button" : ""}`} onClick={decreaseMonth}>
      <FontAwesomeIcon icon={["fas", "chevron-left"]} />
    </span>
    <span className={"current-month"}>{formatWithLocale(date, "MMMM yyyy", locale)}</span>
    <span className={"next-button"} onClick={increaseMonth}>
      <FontAwesomeIcon icon={["fas", "chevron-right"]} />
    </span>
  </div>
);

const dayBeforeMin = (date, minDate, maxDate) => {
  if (minDate && maxDate) {
    return !isWithinInterval(date, { start: startOfDay(minDate), end: endOfDay(maxDate) }) ? "day-excluded" : "";
  }
  return "";
};

const timeInRange = (time, minTime, maxTime) => {
  if (minTime && maxTime) {
    return !isWithinInterval(time, { start: minTime, end: maxTime }) ? "time-excluded" : "";
  }
  return "";
};

/**
 * Custom DateTimePicker
 * @prop {Function} changed On date change callback function, parameters are: stateKey (the prop) - SelectedDate (JS Native Date)
 * @prop {string} stateKey Identify the date change
 * @prop {Date} selected Currently selected date
 * @prop {boolean} isInline If true displays the datepicker inline
 * @prop {Date} startDate When the Datepicker is used to select a range of dates this is the starting date
 * @prop {Date} endDate When the Datepicker is used to select a range of dates this is the end date
 * @prop {boolean} selectsStart When the Datepicker is used to select a range, this identifies if it selects the Start, defaults to true
 * @prop {boolean} selectsEnd When the Datepicker is used to select a range, this identifies if it selects the End Date, defaults to false
 * @prop {Date} minDate If set it is the minimal selected date
 * @prop {Function} excludedDay If set must return a class string, it gives back the day as a Date, by default uses minDate
 * @prop {boolean} showTimeSelect = false If true shows time select
 * @prop {Date} minTime If set it is the minimum time selectable
 * @prop {Date} maxTime If set it is the maximum time selectable
 * @prop {Function} timeClassName If set must return a class string, it gives back the time as a Date, by default uses minTime and maxTime and considers TimezoneOffset so time will always be in UTC by default
 * @prop {number} timeIntervals Time intervals in minutes
 */
export const DateTimePicker = React.forwardRef(
  (
    {
      locale = "en",
      changed,
      stateKey,
      selected,
      isInline = true,
      startDate,
      endDate,
      selectsStart = true,
      selectsEnd = false,
      minDate,
      maxDate,
      excludedDay = (date) => dayBeforeMin(date, minDate, maxDate),
      showTimeSelect = false,
      minTime,
      maxTime,
      timeClassName = (time) => timeInRange(time, minTime, maxTime),
      timeIntervals = 15,
    },
    ref
  ) => {
    const { t } = useTranslation("noku-library");
    return (
      <DatePicker
        locale={locale}
        selected={selected}
        onChange={(date) => changed(stateKey, date)}
        dayClassName={excludedDay}
        inline={isInline}
        startDate={startDate}
        endDate={endDate}
        renderCustomHeader={(params) => DatePickerHeader({ ...params, minDate, locale })}
        selectsStart={selectsStart}
        selectsEnd={selectsEnd}
        showTimeSelect={showTimeSelect}
        timeFormat="HH:mm"
        timeIntervals={timeIntervals}
        minDate={startOfDay(minDate)}
        maxDate={endOfDay(maxDate)}
        timeClassName={timeClassName}
        ref={ref}
        timeCaption={t("time")}
      />
    );
  }
);

DateTimePicker.displayName = 'DateTimePicker';

export default DateTimePicker;
