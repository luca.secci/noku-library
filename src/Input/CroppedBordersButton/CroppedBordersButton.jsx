import React from 'react';

import croppedGradientBorders from "./croppedBorders/button-cropped-border-gradient.svg";
import Button from '../Button/Button';

export const CroppedBordersButton = ({
  croppedBordersClassName = "",
  croppedBordersVariant = "",
  children,
  buttonClassName = "",
  ...props
}) => {

  return (
    <Button
      buttonClassName={`noku-library-cropped-borders-button ${buttonClassName}`}
      color="light-primary"
      {...props}
    >
      <img src={croppedGradientBorders} className={`cropped-borders ${croppedBordersClassName}`} alt="" />
      <span className={"text"}>
        {children}
      </span>
    </Button>
  );
};

export default CroppedBordersButton;
