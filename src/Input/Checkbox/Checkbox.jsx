import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * @prop {CSS} className CSS class
 * @prop {boolean} value = true if checkbox is checked
 * @prop {function} onChange callback called when checkbox change value
 * @prop {boolean} disabled = true if checkbox is disabled
 * @prop {boolean} readOnly = true if checkbox is read only
 * @prop {boolean} some if true checkbox shows minus icon when checked
 */
const Checkbox = ({ className, value, onChange = () => null, disabled, readOnly, some = false }) => (
  <span className={`${className} noku-library-checkbox`}>
    <input
      type="checkbox"
      checked={value}
      onChange={(e) => onChange(e.target.checked)}
      disabled={disabled}
      readOnly={readOnly}
    />
    <span>
      <FontAwesomeIcon className='icon' icon={['fas', some ? 'minus' : 'check']} />
    </span>
  </span>
);
export default Checkbox;
