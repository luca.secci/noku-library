import React, { useState, useEffect } from 'react';

import asyncSleep from "../../utils/asyncSleep";
import Button from "../../Input/Button/Button";
import Loader from "../../Output/Loader/Loader";
import Modal from "../../Output/Modal/Modal";

const AskContainer = ({ askContainerAction, quantity, tokenName, addressTo, feeText, ...props }) => {
  const [visible, setVisible] = useState(false);
  const [containerClasses, setContainerClasses] = useState('');

  const handleContainerClasses = async (action) => {
    console.log('handleContainerClasses');
    console.log(action);

    let _classes;

    switch (action) {
      case 'ask':
        _classes = 'ask';
        break;
      case 'load':
        _classes = 'ask load';
        break;
      case 'finish':
        _classes = 'ask load finish';
        break;
      default:
        break;
    }

    if (_classes) {
      setVisible(true);
      setContainerClasses(_classes);
    } else {
      const time = props.isMobile ? 150 : 650;
      setContainerClasses(_classes);
      await asyncSleep(time);

      setVisible(false);
      await asyncSleep(300);

      props.askClosed && props.askClosed();
    }
  };

  useEffect(() => {
    handleContainerClasses(askContainerAction);
  }, [askContainerAction, handleContainerClasses]);

  const content = visible ? (
    <div className={`noku-library-ask-container ${containerClasses} ${props.askContainerClassName}`}>
      <div className="finish-step">
        <div className="ask-layer">
          <div className="message">
            <div>
              {
                props.children ??
                <span>
                  Are you sure to send <span className="quantity"> {quantity} {tokenName} </span>
                  to <span className="address"> {addressTo} </span> ?
                </span>
              }
            </div>
            <div className="fee">{feeText}</div>
          </div>

          <div className={`buttons ${props.isMobile ? 'column-reverse' : ''}`}>
            <Button
              onClick={props.execute}
              color={props.isMobile ? 'secondary' : 'confirm'}
            >
              Confirm
            </Button>
            <Button
              onClick={props.discard}
              style={props.isMobile ? 'flat' : ''}
            >
              Discard
            </Button>
          </div>

        </div>
        <div className="loader-layer">
          <Loader />
          {/* MAYBEDO: Componentize this animated checkmark (used in ReloadToCheckmark too) */}
          <svg
            className={'checkmark finish-load'}
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 52 52"
          >
            <circle
              className={'checkmark__circle'}
              cx="26"
              cy="26"
              r="25"
              fill="none"
            />
            <path
              className={'checkmark__check'}
              fill="none"
              d="M14.1 27.2l7.1 7.2 16.7-16.8"
            />
          </svg>
          <div className={'progress'}>{props.progressText ?? 'Sending'} in progress...</div>
          <div className={'finish'}>{props.finishText ?? 'Transaction sent'}</div>
        </div>
      </div>
    </div>
  ) : <> </>;

  return props.isMobile ? (
    <Modal
      open={visible}
      onCloseModal={props.discardExecute}
      isMobile={true}
    >
      {content}
    </Modal>
  ) : content;
};

/**
 * TODO: Update Docs
 * @prop {boolean} isMobile true if devices is mobile
class AskContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      containerClasses: null,
      visible: false
    }
  }

  discardExecute = (e) => {
    e && e.preventDefault && e.preventDefault();
    this.props.discardExecute && this.props.discardExecute();
  }

  execute = (e) => {
    e && e.preventDefault && e.preventDefault();
    this.props.execute && this.props.execute();
  }

  getContainerClasses() {
    return this.state.containerClasses;
  }

  setContainerClasses = async (action) => {

    let _classes = null;

    switch (action) {
      case 'ask':
        _classes = classes.ask;
        break;
      case 'load':
        _classes = `${classes.ask} ${classes.load}`;
        break;
      case 'finish':
        _classes = `${classes.ask} ${classes.load} ${classes.finish}`;
        break;
      default:
        break;
    }

    if (_classes) {
      this._isMounted && this.setState({ visible: true }, () => {
        this.setState({ containerClasses: _classes });
      });
    } else {
      let timeout = this.props.isMobile ? 150 : 650;
      this._isMounted && this.setState({ containerClasses: _classes }, async () => {
        await asyncSleep(timeout);

        this._isMounted && this.setState({ visible: false }, async () => {
          await asyncSleep(300);
          this.props.askClosed && this.props.askClosed();
        });
      });
    }
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.askContainerAction !== this.props.askContainerAction) {
      this.setContainerClasses(this.props.askContainerAction);
    }
  }

  render() {
    // let containerCss = this.getContainerClasses(this.props.askContainerAction);
    let containerCss = this.getContainerClasses();
    let quantity = this.props.amountToSend;
    let addressTo = this.props.addressTo;
    let tokenName = this.props.tokenName ? this.props.tokenName : ((this.props.token || {}.token) || {}).name;
    let feeText = `Fee ${this.props.totalFeeTx || 0} ${this.props.chainName}`;

    let content = <div className={`flexContainer spaceBetween ${classes.jsActionContainer} ${classes.maxContainer} ${containerCss}`}>
      <div className={`${classes.finishStep} ${this.state.visible ? `${classes.visible}` : ''}`}>
        <div className={classes.askLayer}>
          <div className={classes.message}>
            <div>
              {
                this.props.children ??
                <>
                  Are you sure to send <span className={classes.jsQuantity}>{quantity} {tokenName} </span>
                  to <span className={classes.jsAddress}> {addressTo} </span> ?
                </>
              }
            </div>
            <div className={classes.jsFee}>{feeText}</div>
          </div>
          <div className={`${classes.buttonsContainer} flexContainer ${this.props.isMobile ? 'columnReverse' : ''}`}>
            {this.props.isMobile ?
              <>
                <Button
                  onClick={this.execute}
                  className={`largeButtonMobile`}
                  type={"secondary"}
                >
                  Confirm
                </Button>
                <Button
                  onClick={this.discardExecute}
                  className={`largeButtonMobile`}
                >
                  Discard
                </Button>
              </>
              :
              <>
                <Button
                  onClick={this.execute}
                  className={`largeButtonMobile`}
                  type={"confirm"}
                >
                  Confirm
                </Button>
                <Button
                  onClick={this.discardExecute}
                  className={`largeButtonMobile`}
                  type={"discard"}
                >
                  Discard
                </Button>
              </>
            }

          </div>
        </div>
        <div className={classes.loaderLayer}>
          <Loader className={classes.loaderAction} />
          <svg
            className={`${classes.checkmark} ${classes.finishload}`}
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 52 52"
          >
            <circle
              className={classes.checkmark__circle}
              cx="26"
              cy="26"
              r="25"
              fill="none"
            />
            <path
              className={classes.checkmark__check}
              fill="none"
              d="M14.1 27.2l7.1 7.2 16.7-16.8"
            />
          </svg>
          <div className={classes.progress}>{this.props.progressText ?? "Sending"} in progress...</div>
          <div className={classes.finish}>{this.props.finishText ?? "Transaction sent"}</div>
        </div>
      </div>
    </div>

    return (this.props.isMobile ? <Modal
      open={this.state.visible}
      onCloseModal={this.discardExecute}
      actions={false}
    >
      {content}
    </Modal> : content
    );
  }
}
*/

export default AskContainer;
