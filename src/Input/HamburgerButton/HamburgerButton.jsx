import React from 'react';

const HamburgerButton = ({ isOpen = false, onClick = () => { } }) => (
  <div className={`noku-library-hamburger ${isOpen ? "open" : ""}`} onClick={onClick}>
    <div />
    <div />
    <div />
  </div>
);

export default HamburgerButton;
