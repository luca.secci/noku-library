import React, { forwardRef, useMemo } from 'react';
import ButtonBase from '@material-ui/core/ButtonBase';

import Loader from '../../Output/Loader/Loader';
import { Link } from 'react-router-dom';

/**
 * @prop {string} style="filled" one of filled, outlined or flat, deprecated for variant
 * @prop {string} color="primary" one of primary, light-primary, secondary, confirm, warning, error, discard, reverse-primary or gradient
 * @prop {boolean} disabled=false true if button must be disabled
 * @prop {Function} onClick button click callback
 * @prop {string} buttonClassName additional button classes
 * @prop {boolean} disableRipple=false true if ripple must be disabled
 * @prop {JSX} startIcon start (left) icon or prepend component
 * @prop {string} startIconClassName startIcon additional class
 * @prop {JSX} endIcon end (right) icon or append component
 * @prop {string} endIconClassName endIcon additional class
 * @prop {boolean} loading=false if the button should show a spinner
 * @prop {string} loadingText="Loading" Customize loading text
 * @prop {boolean} shadow=false if the button should have a box-shadow
 * @prop {string} size="medium" one of small, medium, large (this takes full width)
 * @prop {boolean} fluid=false if true will take all available space
 * @prop {boolean} uppercase=false If true will apply text-transform: uppercase to the content
 * @prop {boolean} round=false If true will have a rounded style
 * @prop {string} type="button" HTML DOM button type
 * @prop {string} link If included a destination link
 * @prop {string} linkType (internal or external)
 * @prop {string} linkTarget for the external type, optionally ad a a:href target (_blank for a new tab)
 * @prop {string} variant The button variant, one of *filled* (default), outlined or flat
 * @prop {boolean} noWrap The button won't wrap text
 * @prop {string} loaderClassName loader additional class
 */
export const Button = forwardRef(({
  style,
  color = 'primary',
  disabled = false,
  onClick,
  buttonClassName,
  disableRipple = false,
  startIcon,
  startIconClassName,
  endIcon,
  endIconClassName,
  loading = false,
  loadingText = "Loading",
  shadow = false,
  size = 'medium',
  fluid = false,
  uppercase = false,
  round = false,
  type = 'button',
  link,
  linkType = "internal",
  linkTarget,
  variant = 'filled',
  noWrap = true,
  children,
  loaderClassName
}, ref) => {
  const StartIcon = useMemo(() => startIcon ? (
    <span className={`icon start-icon ${startIconClassName}`}>
      {startIcon}
    </span>
  ) : <></>, [startIcon, startIconClassName]);

  const EndIcon = useMemo(() => endIcon ? (
    <span className={`icon end-icon ${endIconClassName}`}>
      {endIcon}
    </span>
  ) : <> </>, [endIcon, endIconClassName]);

  const Spinner = useMemo(() => loading ? (
    <>
      <span> {loadingText} </span>
      <Loader svgClassName={`loader ${loaderClassName}`} />
    </>
  ) : <> </>, [loaderClassName, loading, loadingText]);

  const Content = useMemo(() => !loading ? (
    <>
      {StartIcon}
      {children}
      {EndIcon}
    </>
  ) : Spinner, [EndIcon, Spinner, StartIcon, children, loading]);

  const LinkComponent = useMemo(() => {
    if (!link) {
      return <></>;
    }
    if (linkType === "internal") {
      return <Link className="link" to={link} />;
    }
    if (linkType === "external") {
      // eslint-disable-next-line jsx-a11y/anchor-has-content
      return <a className="link" href={link} target={linkTarget} />;
    }
  }, [link, linkTarget, linkType]);

  variant = style ?? variant;

  const buttonClasses = useMemo(() => {
    const classes = ["noku-library-button", "basic", variant, color, size];
    
    if (shadow) {
      classes.push("shadow");
    }
    if (disabled) {
      classes.push("disabled");
    }
    if (loading) {
      classes.push("loading");
    }
    if (fluid) {
      classes.push("fluid");
    }
    if (uppercase) {
      classes.push("uppercase");
    }
    if (round) {
      classes.push("round");
    }
    if (noWrap) {
      classes.push("no-wrap");
    }
    if (buttonClassName) {
      classes.push(buttonClassName);
    }

    return classes.join(" ");
  }, [buttonClassName, color, disabled, fluid, loading, noWrap, round, shadow, size, uppercase, variant]);

  return (
    <ButtonBase
      onClick={onClick}
      disabled={disabled || loading}
      className={buttonClasses}
      disableRipple={disableRipple}
      type={type}
      ref={ref}
    >
      {Content}
      {LinkComponent}
    </ButtonBase>
  );
});

Button.displayName = "Button";

export default Button;
