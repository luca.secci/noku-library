# Buttons guide

SASS senpai decided to categorize buttons in the following categories.

First we differenciate style and color types.

Style means how the button is viewed (ex. filled or outlined).

Color means how do i color the button (ex. primary or secondary).

Style categories:

* [x] Filled (Default)
* [x] Outlined
* [x] Flat

Color categories:

* [x] Primary (Default)
* [ ] Secondary
* [ ] Confirm
* [ ] Discard
