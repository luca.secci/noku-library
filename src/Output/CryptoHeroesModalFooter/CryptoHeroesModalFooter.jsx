import React from 'react';

import { getCryptoHeroesCardNumber } from '../../utils/cryptoHeroes/getCardNumber';
import { getCryptoHeroesEditionYearFromMetadata } from '../../utils/cryptoHeroes/getEdition';
import { getCryptoHeroesRarityLabelFromMetadata } from '../../utils/cryptoHeroes/getRarityLabel';
import { getCryptoHeroesSeriesLabelFromMetadata } from '../../utils/cryptoHeroes/getSeriesLabel';

import classes from './CryptoHeroesModalFooter.module.scss';

export const CryptoHeroesModalFooter = ({ id, metadata, footerClassName }) => {

  return (
    <div className={`${classes.footer} ${footerClassName}`}>
      <div className={`${classes.row}`}>
        <div className={classes.column}>
          <div className={classes.firstRow}>{getCryptoHeroesRarityLabelFromMetadata(metadata) ?? " "}</div>
          <div className={classes.secondRow}>#{getCryptoHeroesCardNumber(id)}</div>
        </div>
        <div className={classes.column}>
          <div className={classes.firstRow}>{getCryptoHeroesEditionYearFromMetadata(metadata) ?? " "}</div>
          <div className={classes.secondRow}>
            {getCryptoHeroesSeriesLabelFromMetadata(metadata) ?? " "}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CryptoHeroesModalFooter;
