import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Avatar } from "@material-ui/core";
import React, { useMemo } from "react";
import { useTranslation } from "react-i18next";

// import defaultAvatar from "../../assets/images/avatar.svg";
import getUserAvatar from "../../utils/getUserAvatar";

// Avatar, name, email kyclevel, kycimage, baseAccountsUrl

export const User = ({
  userId = "",
  nickname,
  email,
  kycLevel = 1,
  avatar,
  accountsUrl = "",
  logoutUrl = "",
  accountDetailsUrl = "",
  accountLanguageUrl = "",
  upgradeKYCLevelUrl = "",
  fontAwesomeFamily = "fas",
  showKYC = true,
  showAccountDetails = true,
  showLogout = true,
  showLanguage = false,
  handleLogout = null
}) => {
  const { t } = useTranslation('noku-library');

  if (accountsUrl) {
    if (!accountDetailsUrl) {
      accountDetailsUrl = `${accountsUrl}/app/home/account_detail`;
    }

    if (!upgradeKYCLevelUrl) {
      upgradeKYCLevelUrl = `${accountsUrl}/app/home/kyc`;
    }

    if (!logoutUrl) {
      logoutUrl = `${accountsUrl}/logout`;
    }

    if (!accountLanguageUrl) {
      accountLanguageUrl = `${accountsUrl}/app/home/settings/language`;
    }
  }

  const renderLogout = useMemo(() => {
    if (handleLogout) {
      return <>
        <span className="menu-item logout" onClick={(e) => handleLogout(e)}>
          <FontAwesomeIcon
            className="icon"
            icon={[fontAwesomeFamily, "sign-out-alt"]}
          />
          <span className="text">{t("logout")}</span>
        </span>
      </>
    } else if (showLogout) {
      return <>
        <a className="menu-item logout" href={logoutUrl}>
          <FontAwesomeIcon
            className="icon"
            icon={[fontAwesomeFamily, "sign-out-alt"]}
          />
          <span className="text">{t("logout")}</span>
        </a>
      </>
    } else {
      return <></>
    }
  }, [handleLogout, logoutUrl, fontAwesomeFamily, showLogout])

  const avatarSrc = useMemo(() => getUserAvatar(avatar, userId), [avatar, userId]);

  return (
    <div className="noku-library-user">
      <Avatar src={avatarSrc} className="avatar" />
      <div className="arrow">
        <FontAwesomeIcon icon={"sort-down"} />
      </div>
      <div className="dropdown">
        <div className="user-info">
          <Avatar src={avatarSrc} className="avatar" />
          <div className="nickname-and-email">
            <div className="nickname">{nickname}</div>
            <div className="email">{email}</div>
          </div>
          {showKYC ? (
            <img className="kyc-image" src={`https://static.noku.io/assets/noku/kyc-levels/${kycLevel}.svg`} alt="Kyc level" />
          ) : (
            <></>
          )}
        </div>
        {showKYC ? (
          <a className="kyc-block" href={upgradeKYCLevelUrl}>
            <div className="text">{t("upgradeKYCLevel")}</div>
            <div className="chip">{t("level")} {kycLevel}</div>
          </a>
        ) : (
          <></>
        )}
        {showAccountDetails ? (
          <a className="menu-item account-details" href={accountDetailsUrl}>
            <FontAwesomeIcon
              className="icon"
              icon={[fontAwesomeFamily, "user-circle"]}
            />
            <span className="text">{t("accountDetails")}</span>
          </a>
        ) : (
          <></>
        )}
        {showLanguage ? (
          <>
            <div className="divider" />
            <a className="menu-item account-details" href={accountLanguageUrl}>
              <FontAwesomeIcon
                className="icon"
                icon={[fontAwesomeFamily, "globe"]}
              />
              <span className="text">{t("language")}</span>
            </a>
          </>
        ) : (
          <></>
        )}
        <div className="divider" />
        { renderLogout }
      </div>
    </div>
  );
};

export default User;
