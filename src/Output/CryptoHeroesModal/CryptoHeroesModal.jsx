import React from 'react';
import Modal from '../Modal/Modal';

import { CryptoHeroesModalBody } from '../CryptoHeroesModalBody/CryptoHeroesModalBody';
import { CryptoHeroesModalFooter } from '../CryptoHeroesModalFooter/CryptoHeroesModalFooter';

import classes from './CryptoHeroesModal.module.scss';
import CurrentTokenStatus from '../../utils/cryptoHeroes/CurrentTokenStatus';

export const CryptoHeroesModal = ({ id, metadata = {}, isOpen = false, onClose = () => { }, openMarketPlaceModal = () => { }, withMarketplace = false, tokenStatus = CurrentTokenStatus.Balance }) => {
  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      // actions={Footer}
      bodyClassName={classes.body}
      showCancel={false}
      modalClassName={classes.modal}
      actionClassName={classes.actions}
      showCloseIcon={true}
      externalCloseIconButton={true}
      // closeIconButtonClassName={classes.closeIcon}
      headerClassName={classes.header}
      titleClassName={classes.title}
      paperClassName={classes.paper}
      dividerClassName={classes.divider}
      scroll="body"
    >
      <CryptoHeroesModalBody
        id={id}
        openMarketPlaceModal={openMarketPlaceModal}
        withMarketplace={withMarketplace}
        tokenStatus={tokenStatus}
        {...metadata}
      />
      <CryptoHeroesModalFooter id={id} metadata={metadata} />
    </Modal>
  );
};

export default CryptoHeroesModal;
