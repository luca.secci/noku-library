import React from 'react';

/**
 * @prop {number} progressPercent this is the percent (so from 0 to 100)
 * @prop {string} backgroundClassName additional background class
 * @prop {string} foregroundClassName additional foreground (this is the colored bar) class
 */
const ProgressBar = ({ progressPercent, backgroundClassName, foregroundClassName }) => (
  <div className={`noku-library-progress-bar background ${backgroundClassName}`}>
    <div style={{ width: `${progressPercent}%` }} className={`foreground ${foregroundClassName}`} />
  </div>
);

export default ProgressBar;
