import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconButton, Tooltip } from '@material-ui/core';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import Button from "../../Input/Button/Button";
import { CurrentTokenStatus } from '../../utils/cryptoHeroes/CurrentTokenStatus';

import classes from './CryptoHeroesModalBody.module.scss';

export const Asset = ({ asset = "", multiplier = "" }) => asset && multiplier ? <div className={classes.asset}> {asset} x {multiplier} </div> : <></>;

export const CryptoHeroesModalBody = ({ image = "", attributes = {}, id = "", openMarketPlaceModal = () => { }, withMarketplace = false, tokenStatus = CurrentTokenStatus.Balance }) => {
  const { t } = useTranslation("noku-library");

  const MarketplaceButton = useMemo(() => {
    if (withMarketplace) {
      switch (tokenStatus) {
        case CurrentTokenStatus.Balance:
          return (
            <>
              <div className={classes.title}>{t("chBuyModalTitle")}</div>
              <div className={classes.description}>{t("chBuyModalDescription")}</div>
              <Button variant="outlined" classes={{ root: classes.cart, disabled: classes.disabled }} size="small" onClick={openMarketPlaceModal}>
                {t("sellNow")}
              </Button>
            </>
          );
        case CurrentTokenStatus.CryptoHeroes:
          return (
            <>
              <div className={classes.title}>{t("chBuyModalTitle")}</div>
              <div className={classes.description}>{t("chBuyModalDescription")}</div>
              <Tooltip arrow placement="bottom" title={t("cardDisabled")}>
                <div>
                  <Button variant="outlined" classes={{ root: classes.cart, disabled: classes.disabled }} disabled={true} size="small">
                    {t("sellNow")}
                  </Button>
                </div>
              </Tooltip>
            </>
          );
        case CurrentTokenStatus.Marketplace:
          return (
            <>
              <div className={classes.title}>{t("chUnlockModalTitle")}</div>
              <div className={classes.description}>{t("chUnlkockModalDescription")}</div>
              <Button variant="outlined" size="small" onClick={openMarketPlaceModal}>
                {t("unlockNow")}
              </Button>
            </>
          );

        default:
          break;
      }
    }
    return <></>;

  }, [openMarketPlaceModal, t, tokenStatus, withMarketplace]);

  return (
    <div className={classes.contentWrapper}>
      <div className={classes.firstHalf} />
      <div className={classes.content}>
        <div className={`${classes.row} ${classes.id}`}>
          ID CODE: {id.split('@')[0]}
        </div>
        <img className={classes.tokenImage} src={image} alt="" loading="lazy" />
        <div className={classes.bodyInfo}>
          <div className={`${classes.row}`}>
            <div className={`${classes.label} ${classes.assetLabel}`}>
              {(attributes?.A1 || attributes?.A2 || attributes?.A3) && attributes?.M ? "Asset" : " "}
            </div>
            <div className={classes.label}>
              {attributes?.W ? `Weight ${attributes?.W}` : " "}
            </div>
          </div>
          <div className={`${classes.row} ${classes.assetRow}`}>
            {(attributes?.A1 || attributes?.A2 || attributes?.A3) && attributes?.M ? (
              <>
                <Asset asset={attributes?.A1} multiplier={attributes?.M} />
                <Asset asset={attributes?.A2} multiplier={attributes?.M} />
                <Asset asset={attributes?.A3} multiplier={attributes?.M} />
              </>
            ) : <></>}
          </div>
          {MarketplaceButton}
        </div>
      </div>
    </div>
  )
};

export default CryptoHeroesModalBody;
