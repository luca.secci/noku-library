import React, { useCallback } from "react";
import { IconButton, Dialog, DialogActions, DialogContent, DialogTitle } from "@material-ui/core";

import Button from "../../Input/Button/Button";
import ProgressBar from "../../Output/ProgressBar/ProgressBar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

/**
 * @prop {boolean} isOpen If modal is open then it's true
 * @prop {boolean} isLoading If modal is loading some data then it's true
 * @prop {callback} onClose Called when modal is closed
 * @prop {string} title Custom title string
 * @prop {CSS} titleClassName The title additional classes, this is inside header
 * @prop {string} titleColor The title color
 * @prop {CSS} headerClassName The header additional classes
 * @prop {CSS} dividerClassName The divider additional classes
 * @prop {boolean} showActions Force actions rendering
 * @prop {JSX} actions Custom actions buttons or any valid JSX
 * @prop {CSS} actionClassName The footer additional classes
 * @prop {boolean} showCancel default = true Shows a "Cancel" button
 * @prop {boolean} disableCancel Should the cancel button be disabled
 * @prop {string} cancelText default = "Cancel" Custom optional text for cancel button
 * @prop {string} cancelButtonColor Custom color for the cancel button
 * @prop {boolean} showCloseIcon = false, if true shows a close icon
 * @prop {boolean} hasProgressBar If true shows a linear progress indicator, must specify progress porp
 * @prop {number} progressPercent Current percentage progress (int)
 * @prop {boolean} isFullScreen default = false If true the dialog will be full-screen
 * @prop {CSS} bodyClassName The body additional classes
 * @prop {CSS} actionWrapperClassName The actions wrapper classes
 * @prop {CSS} paperClassName The paper classes
 * @prop {CSS} containerClassName The container classes
 * @prop {boolean} removeNavigatorHeight If the modal should remove the appbar/navigator height
 * @prop {Function} onBackdropClick Called when user click on the backdrop
 * @prop {string} scroll Reference for scroll, can be "paper" or "body"
 * @prop {JSX} children Modal Body JSX
 */
export const Modal = ({
  modalClassName = "",
  isOpen = false,
  isLoading = false,
  onClose = () => { },
  title,
  titleColor = "",
  titleClassName = "",
  headerClassName = "",
  dividerClassName = "",
  showActions = true,
  actions = <></>,
  actionClassName = "",
  actionsBackgroundColor = "",
  showCancel = true,
  disableCancel = false,
  cancelText = "Cancel",
  cancelButtonColor,
  showCloseIcon = false,
  disableCloseIcon = false,
  closeIconButtonClassName = "",
  closeIconClassName = "",
  externalCloseIconButton = false,
  hasProgressBar,
  progressPercent,
  isFullScreen = false,
  bodyClassName = "",
  actionWrapperClassName = "",
  paperClassName = "",
  removeNavigatorHeight = false,
  onBackdropClick,
  scroll = "paper",
  containerClassName = "",
  actionsSpacing = "flex-end",
  children,
}) => {
  const handleClose = useCallback((event, reason) => {
    if (reason === "backdropClick" && onBackdropClick) {
      return onBackdropClick(event);
    }
    onClose(event);
  }, [onBackdropClick, onClose]);

  const header = title || showCloseIcon ? (
    <DialogTitle className={`header ${headerClassName}`}>
      <div className={`title ${titleColor} ${titleClassName}`}>{title}</div>
      {
        showCloseIcon ? (
          <IconButton disabled={disableCloseIcon} classes={{
            root: `close-icon-button ${externalCloseIconButton ? "external" : ""} ${closeIconButtonClassName}`,
            disabled: `disabled`
          }} onClick={handleClose}>
            <FontAwesomeIcon className={`close-icon ${closeIconClassName}`} icon="times" />
          </IconButton>
        ) : <></>
      }
    </DialogTitle>
  ) : (
    <></>
  );

  const divider = hasProgressBar ? (
    <ProgressBar progressPercent={progressPercent} />
  ) : (
    <div className={`divider ${dividerClassName}`} />
  );

  const body = (
    <DialogContent className={`body ${bodyClassName}`}>
      <div className={`loader ${isLoading ? "active" : ""}`}>
        <div className="message">Loading...</div>
      </div>
      {children}
    </DialogContent>
  );

  const modalActions = showActions ? (
    <DialogActions className={`actions ${actionsBackgroundColor} ${actionsSpacing} ${actionClassName}`}>
      {
        showCancel ? (
          <Button
            className="cancel-button"
            variant="flat"
            color={cancelButtonColor ?? actionsBackgroundColor === 'primary' ? 'reverse-primary' : undefined}
            onClick={handleClose}
            disabled={disableCancel}
          >
            {cancelText}
          </Button>
        ) : (
          <></>
        )
      }
      <div className={actionWrapperClassName}>{actions}</div>
    </DialogActions>
  ) : (
    <></>
  );

  return (
    <Dialog
      // onBackdropClick={onBackdropClick ?? onClose}
      open={isOpen}
      onClose={handleClose}
      fullScreen={isFullScreen}
      // className={`noku-library-modal ${removeNavigatorHeight ? "boxed" : ""} ${modalClassName}`}
      maxWidth="xl"
      scroll={scroll}
      // PaperProps={{className: `${scroll === "body" ? "overflow-unset" : ""} ${paperClassName}`}}
      classes={{
        root: `noku-library-modal ${removeNavigatorHeight ? "boxed" : ""} ${modalClassName}`,
        paper: `paper ${paperClassName}`,
        container: containerClassName,
        paperScrollBody: "overflow-unset",
      }}
      BackdropProps={{ className: "backdrop" }}
    >
      {header}
      {divider}
      {body}
      {modalActions}
    </Dialog>
  );
};

export default Modal;
