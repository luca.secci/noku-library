import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Modal from '../../Output/Modal/Modal';

/**
 * @prop {boolean} isOpen If alert is open then it's true
 * @prop {'success'|'warning'|'errror'} level The popup alert level
 * @prop {JSX} actions Custom actions buttons or any valid JSX
 * @prop {string} title The popup title
 * @prop {string} titleClassName The popup additional classes
 * @prop {string} description The popup description
 * @prop {string} descriptionClassName The popup additional classes
 * @prop {JSX} headerIcon overrides the header icon (default: success - check-circle, warning - exclamation-circle, error - exclamation-triangle)
 * @prop {string} headerClassName Modal header additional classes
 * @prop {string} modalTitleClassName Modal title additional classes
 * @prop {JSX} children Modal Body JSX
 */
const PopupAlert = ({ isOpen = false, level = 'success', actions, actionsClassName = "", actionsWrapperClassName = "", title = '', titleClassName = '', description, descriptionClassName = '', headerIcon, headerClassName = '', modalTitleClassName = '', children }) => {
  // Set header icon if not specified
  if (!headerIcon) {
    switch (level) {
      case 'success':
        headerIcon = <FontAwesomeIcon icon="check-circle" />;
        break;
      case 'warning':
        headerIcon = <FontAwesomeIcon icon="exclamation-circle" />;
        break;
      case 'error':
        headerIcon = <FontAwesomeIcon icon="exclamation-triangle" />;
        break;
      default:
        break;
    }
  }

  // Create the popup body
  const PopupBody = (
    <>
      <div className={`title ${titleClassName}`}>{title}</div>
      <div className={`description ${descriptionClassName}`}>{description}</div>
    </>
  );

  return (
    <Modal
      actions={actions}
      isOpen={isOpen}
      title={headerIcon}
      headerClassName={`popup-alert-header ${headerClassName} ${level}`}
      bodyClassName="popup-alert-body"
      actionClassName={`popup-alert-actions ${actionsClassName}`}
      actionWrapperClassName={`popup-alert-actions-wrapper ${actionsWrapperClassName}`}
      modalClassName="popup-alert"
      showCancel={false}
      titleClassName={`popup-alert-title ${modalTitleClassName}`}
    >
      {PopupBody}
    </Modal>
  );
};

export default PopupAlert;
