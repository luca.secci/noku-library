import React, { useCallback, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';

/**
 * @typedef {Object} MenuItems
 * @property {string} destination The link destination
 * @property {string} title What to show
 * @property {"internal"|"external"} linkType The type of the link, external or internal, when internal the link is rendered with <NavLink>
 * @property {(e: Event) => {}} onItemClick a callback to execute when this item has been clicked
 */
const MenuItem = ({ destination, title, linkType, onItemClick, exact = true }) => {
  if (linkType === 'internal') {
    return (
      <NavLink className="link" activeClassName="active" to={destination} exact={exact} onClick={onItemClick}>
        {title}
      </NavLink>
    );
  } else {
    return (
      <a className="link" href={destination} onClick={onItemClick}> {title} </a>
    );
  }
};

/**
 * @param {{ items: MenuItems[], logoSrc: string, rightSlot: JSX.Element, linksClassName: string, withMobileMenu: boolean, onHamburgerClick: Function }} props
 */
const MainNavigator = ({ items = [], logoSrc, logoComponent, rightSlot, showHamburger, linksClassName = '', withMobileMenu = true, onHamburgerClick = () => {}, hideLogo = false, ...props }) => {
  let PrimaryLogo = () => <img className="logo-image primary" src={logoSrc} alt="logo" />;
  // let SecondaryLogo = <img className="logo-image secondary" src={reverseLogoSrc} alt="logo" />;

  if (logoComponent) {
    PrimaryLogo = logoComponent;
  }

  // Mobile navigator logic
  const [isOpen, setIsOpen] = useState(false);

  // Override hamburger click
  const handleHamburgerClick = useCallback(
    () => {
      const newValue = !isOpen;
      setIsOpen(newValue);
      onHamburgerClick(newValue);
    },
    [isOpen, onHamburgerClick]
  );

  return (
    <div className={`noku-library-navigator ${isOpen ? 'open' : ''} ${withMobileMenu ? 'with-mobile-menu' : ''}`}>
      <div className={`hamburger ${showHamburger === true ? 'show' : ''} ${showHamburger === false ? 'hide' : ''}`} onClick={handleHamburgerClick}>
        <div />
        <div />
        <div />
      </div>
      {
        !hideLogo ? (
          <div className="logo">
          <Link className="logo-link" to="/">
            <PrimaryLogo />
          </Link>
        </div>
          ) : <></>
      }
      <div className={`links ${linksClassName}`}>
        {items.map((item = {}, i) => <MenuItem key={`navigator-menu-item-${i}`} {...item} />)}
      </div>
      <div className="right-slot">
        {rightSlot}
      </div>
    </div>
  );
};

export default MainNavigator;
