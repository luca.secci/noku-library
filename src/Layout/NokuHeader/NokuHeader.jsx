import React from "react";
import { Link, NavLink } from "react-router-dom";

const MenuItem = ({ destination, title, linkType, onItemClick, exact = true, forceActive = false, highlight = false }) => {
  if (linkType === 'internal') {
    return (
      <NavLink className={`link ${forceActive ? 'active' : ''} ${highlight ? 'link-highlight' : ''}`} activeClassName="active" to={destination} exact={exact} onClick={onItemClick}>
        {title}
      </NavLink>
    );
  } else {
    return (
      <a className={`link ${forceActive ? 'active' : ''} ${highlight ? 'link-highlight' : ''}`} href={destination} onClick={onItemClick}> {title} </a>
    );
  }
};

export const NokuHeader = ({ onHamburgerClick = () => {}, logoSrc, reverseLogoSrc, items = [], logoComponent, rightSlot = <></>, logoLink = "/", ...props }) => {
  let PrimaryLogo = () => <img className="logo-image primary" src={logoSrc} alt="logo" />;
  // let SecondaryLogo = <img className="logo-image secondary" src={reverseLogoSrc} alt="logo" />;

  if (logoComponent) {
    PrimaryLogo = logoComponent;
  }

  return (
    <div className="noku-library-header">
      <div className="hamburger" onClick={onHamburgerClick}>
        <div></div>
        <div></div>
        <div></div>
      </div>
      <div className="logo">
        <Link className="logo-link" to={logoLink}>
          <PrimaryLogo />
        </Link>
      </div>
      <div className={`links`}>
        {items.map((item = {}, i) => <MenuItem key={`navigator-menu-item-${i}`} {...item} />)}
      </div>
      <div className="right-slot">
        {rightSlot}
      </div>
    </div>
  );
};

export default NokuHeader;
