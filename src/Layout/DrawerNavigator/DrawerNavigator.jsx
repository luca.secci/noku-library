import React, { forwardRef, useMemo } from 'react';
import { NavLink } from 'react-router-dom';
import { Avatar, Divider, Drawer, List, ListItem } from '@material-ui/core';
import getUserAvatar from '../../utils/getUserAvatar';

/*
Example menu item: {
  destination: "/home",
  title: "Homepage",
  linkType: "internal",
  onItemClick: () => console.log("hello"),
  exactMatch: false
}
*/

const MenuItem = ({ destination, title, linkType, onItemClick, exactMatch = true }) => {
  const LinkComponent = useMemo(
    () => {
      if (linkType?.toLowerCase?.() === 'internal') {
        return (
          forwardRef((linkProps, ref) => (
            <NavLink ref={ref} activeClassName="active" to={destination} exact={exactMatch} {...linkProps} />
          ))
        );
      } else {
        return (
          forwardRef((linkProps, ref) => (
            // Disabled because we pass children as props
            // eslint-disable-next-line jsx-a11y/anchor-has-content
            <a ref={ref} href={destination} {...linkProps} />
          ))
        );
      }
    }, [destination, exactMatch, linkType]);

  return (
    <ListItem classes={{ root: `menu-item link` }} button onClick={onItemClick} component={LinkComponent}>
      {title}
    </ListItem>
  );
};

export const DrawerNavigator = ({ 
  accountId,
  isOpen = false, 
  onDrawerClose, 
  accountAvatar = "", 
  accountFullName = "", 
  accountEmail = "", 
  drawerRootClassName = "",
  drawerContentClassName = "", 
  appMenuItems = [], 
  otherMenuItems = [], 
  onAccountInfoClick = () => {},
  accountsUrl = "#",
  footer = <></>, 
  variant = "temporary" 
}) => (
  <Drawer
    className={`noku-library-drawer ${drawerRootClassName}`}
    open={isOpen}
    onClose={onDrawerClose}
    transitionDuration={250}
    PaperProps={{ className: `content ${drawerContentClassName}` }}
    variant={variant}
  >
    <div className="upper-content">
      <a className="account-avatar-wrapper" href={accountsUrl} onClick={onAccountInfoClick}>
        <Avatar src={getUserAvatar(accountAvatar, accountId)} className="avatar" />
      </a>
      {/*
      <div className="account-avatar-wrapper" onClick={onAccountInfoClick}>
        <Avatar src={accountAvatar} className="avatar" />
      </div>
      */}
      <a className="user-info" href={accountsUrl} onClick={onAccountInfoClick}>
        {
          accountFullName ? (
            <div className="full-name">{accountFullName}</div>
          ) : <></>
        }
        {
          accountEmail ? (
            <div className="email">{accountEmail}</div>
          ) : <></>
        }
      </a>
      {
        appMenuItems?.length > 0 ? (
          <List className="app-items">
            {appMenuItems.map((item) => <MenuItem key={item.title} {...item} />)}
          </List>
        ) : <></>
      }
      {
        appMenuItems?.length > 0 && otherMenuItems?.length > 0 ? (
          <div className="divider">
            <Divider />
          </div>
        ) : <></>
      }
      {
        otherMenuItems?.length > 0 ? (
          <List className="other-menu-items">
            {otherMenuItems.map((item) => <MenuItem key={item.title} {...item} />)}
          </List>
        ) : <></>
      }
    </div>
    <div className="footer">
      {footer}
    </div>
  </Drawer>
);

export default DrawerNavigator;
