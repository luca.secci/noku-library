import React from 'react';
import { format } from 'date-fns';
import { IconButton } from '@material-ui/core';

export const Footer = ({ className = "", innerClassName = "", Logo = (props) => <></>, socialLinks, ...props }) => {
  return (
    <div className={`noku-library-footer ${className}`}>
      <div className={`inner ${innerClassName}`}>
        <div className={`copyright-and-logo`}>
          <Logo className={`logo`} />
          <div className={`copyright`}>
            Copyright {format(new Date(), "yyyy")} Noku
          </div>
        </div>
        <div className={`social-links`}>
          {socialLinks.map(({ key, link = "#", Icon, IconProps: { className: iconClassName = "", ...IconProps } }) => (
            <IconButton href={link} target="_blank" referrerPolicy="no-referrer" key={key} classes={{ root: "icon-button" }}> 
              <Icon className={`icon ${iconClassName}`} {...IconProps} /> 
            </IconButton>
            )
          )}
        </div>
      </div>
    </div>
  );
};

export default Footer;
