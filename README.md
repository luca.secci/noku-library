# Chainer differences from Wallet

1. The `getThemeObject` gives back a source svg to use inside an img or an object therefore losing almost all customization capabilities

2. You MUST provide:

  * `socketEndpoint` a socket.io 3.0 compatible backend that SHOULD listen to:

    * `refreshAll` -> MUST refresh all info (balance, transactions and fee configurations)
    
    * `getTransaction` -> MUST emit the wallet transactions (`transaction`)

    * `getBalance` -> MUST emit the wallet balance (`balance`)

  * `socketSwapEndpoint` a socket.io 3.0 compatible backend that SHOULD listen to:

    * ``

  * `getNonce` A promise-like function that takes "address" and "chain" and gives back a wallet's nonce

  * `sendRawTransaction`

3. ...

# Discussion about customisation

## Solutions

### CSS-IN-JS

This solution has been widely adopted by the React community, but there are some drawbacks in writing CSS-IN-JS, for example if you have a SCSS palette you cannot use it in JS.

We have axioma, eidoo and noku palette already defined in custom_variables, so if we adopt this solution we need to find a way to pass these variables into the components.

For example we could put them in the store:

```jsx
// Button.jsx

// Suppose we have props.primaryColor, props.primaryHover, how can we style the button?

export default function Button(props) {
  const style = {
      backgroundColor: props.primaryColor,
      "&:hover" :{
        backgroundColor: props.primaryHover,
      },
      ...props.style,
  };

return ( 
    <button onClick={props.onClick} disabled={props.disabled} className={`${typeClass} ${classes.basic} ${props.className}`} style={style}>
      {props.children}
    </button>
  );
}
```

This solution has a problem: what should we do if we need to overwrite for any reason the style? Style has the maximum priority in HTML/CSS so if we will work in CSS we will need to use `!important` to overwrite it!

Also how should we handle multiple button types?

```jsx
// Button.jsx

// Suppose we have props.primaryColor, props.primaryHover, props.secondaryColor, props.secondaryHover and props.type

export default function Button(props) {
  let style;
  
  switch(props.type) {
    case "primary":
      style = {
        backgroundColor: props.primaryColor,
        "&:hover" :{
          backgroundColor: props.primaryHover,
        },
        ...props.style,
      };
      break;
    case "secondary":
      style = {
        backgroundColor: props.secondaryColor,
        "&:hover" :{
          backgroundColor: props.secondaryHover,
        },
        ...props.style,
      };
      break;
    // etc. for each type!
  }

  return ( 
    <button onClick={props.onClick} disabled={props.disabled} className={`${typeClass} ${classes.basic} ${props.className}`} style={style}>
      {props.children}
    </button>
  );
}
```

### SASS library

So SASS senpai help us by providing a SASS only solution by taking inspiration from Bootstrap and Foundation.

We already have the palettes defined, for example:

```scss
// custom_variables.scss (inside your app)
// Noku palette
$color-palette: (
  primary-color: #4548ed;
  dark-primary-color: #2225c3;
  super-dark-primary-color: #1c1c9c;

  secondary-color: $primary-color;
  dark-secondary-color: $dark-primary-color;
  super-dark-secondary-color: $super-dark-primary-color;

  button-primary-color: #ffffff;
  button-primary-bg-color: $dark-primary-color;
  button-primary-hover-bg-color: $super-dark-primary-color;

  button-secondary-color: $primary-color;
  button-secondary-bg-color: #ffffff;
  button-secondary-hover-bg-color: #ff0000; //unused if white

  // etc.
)
```

```scss
// mixin.css (inside noku-library)
@mixin button-styles($color-palette) {
  .noku-library-button {
    // Add some basic style here
    ...
    // Customise each type
    &.primary {
      background-color: map-get($map: $color-palette, $key: $button-primary-bg-color);
      color: map-get($color-palette, $button-primary-color);
      &:hover {
        background-color: map-get($map: $color-palette, $key: $button-primary-hover-bg-color);
      }
    }
    // Same goes for every style
    &.secondary {

    }
  }
}
```

```scss
// index.scss (inside your app)
@import "./.../custom_variables.scss";
@import "~noku-library/buttons.scss";

@include button-styles($color-palette);
```

With this pure SCSS solution we can use the already defined color-palette, plus we will avoid duplication of colors in the code (both JS and CSS files), we could also use this library with any framework, the React components will be more a opt-in, an option, than a obbligated choice.

Also we will be able to almost completely keep `Button.module.scss` as it is, avoiding any possibile future issue since it is "battle tested".

## Components

### Input
* [x] Button
* [x] Action (deprecated by button style flat)
* [x] AskContainer
* [x] Back Button
* [x] Base Input
* [x] !Checkbox -
* [ ] CopyBox (used only in a modal)
* [x] DropDown
* [x] !EidooDatePicker (should become DatePicker) -
* [ ] ?ExpandableSearchBar (too specialised?)
* [x] !File Upload -
* [x] LargeButton
* [ ] MultipleOptions (With Single Option) -
* [ ] !Paginator
* [ ] QrReader
* [ ] Radio - (To Review)
* [ ] RadioGroup - (To Review)
* [ ] SearchBar
* [x] Select
* [ ] ?TextEditor - (Master)
* [ ] !Toggle
* [ ] ?SendTo
* [ ] ?AmountToSend (?)
  
### Output

* [ ] !Accordion
* [ ] ?BaseStep
* [ ] !Blockie
* [ ] !Blockie Container (merge with Blockie ?)
* [ ] Card -
* [ ] ?CircularProgress - (should be revised)
* [ ] ?CreateWalletCard
* [ ] ?CurrenciesTooltip
* [ ] ?CurrencyChip
* [x] !CustomSlider -
* [ ] !Loader (should be renamed spinner?)
* [ ] !Notification
  * [ ] !Notification Theme
* [ ] !Reload To Checkmark
  * [ ] DrawCheckmark (?)
* [x] Modals (Now is Modal)
* [x] Status Bar (Now is progress bar)
* [ ] ?!Transaction Accordion (https://www.youtube.com/watch?v=iB-TsyYqRTk)

### Pages

* [ ] Page Not Found
* [ ] Error Boundary
  
### Layout

* [ ] !Navigator (Must be refactored) -
  * [ ] !User (Navigator dependency) -
  
### Others

* [ ] GAListener
* [x] Chainers

Chainers -> Unlock Wallet (TODO) -> Send TX (TODO)

# How to use the library (NPM)

1. Inside your application run `npm i git+https://git.noku.io/noku-tech/noku-library.git#${CURRENT_VERSION}` with the desired version in `${CURRENT_VERSION}`

2. a) If you're using npm >= 7, check that the following peer dependencies are installed:

    * i18next

    * i18next-browser-languagedetector

    * react-i18next

    * gridy-avatars


2. b) If you're using npm < 7, install the peer dependencies: `npm i --save i18next i18next-browser-languagedetector gridy-avatars react-i18next`


# How to use the library (LOCAL DEV)

**my-app for noku-app is ../noku-app/src/client/node_modules/react**!

1. Inside `noku-library`: `npm link ../my-app/node_modules/react`

2. Inside `noku-library`: `npm link`

3. Inside `my-app`: `npm link noku-library`

4. Inside `my-app`: `import { Whathever } from 'noku-library/dist';`

Shortcut for unix based and `noku-app` (when inside `noku-library`): 

* `npm link ../noku-app/src/client/node_modules/react && npm link && cd ../noku-app/src/client && npm link noku-library`

Shortcut for unix based and `noku-app` (when inside `noku-app/src/client`):

* `cd ../../../noku-library && npm link ../noku-app/src/client/node_modules/react && npm link && cd ../noku-app/src/client && npm link noku-library`

# Import default styles/customization

The starting point is `noku-library/dist/all.scss`, if you don't need to customize the colours then just import this and you're good to go, import that scss file somewhere.

Otherwise if you need to customize the palette start from a scss/sass file:

```scss
// Import default style
@import "~noku-library/dist/all.scss";

// Override some colors, if the variables have correct naming then the master-palette will do the rest
// for example: $primary-SHADE, where SHADE is a number included in [100, 200, 300, 400, 500, 600, 700, 800, 900]
@import "./variables.scss";

// Create Palettes
@import "~noku-library/dist/scss/master-palette.scss";

// If you want further customization you can override the single palettes
// Override something of the main-navigator-palette
$override-main-navigator-palette: (
  link-underline-color: $secondary-color,
  logo-width: $logo-width,
);

// Merge the default palette with the override
$main-navigator-palette: map-merge(
  $map1: $main-navigator-palette,
  $map2: $override-main-navigator-palette, 
);

// Create new Styles, This will apply your new palette
@import "~noku-library/dist/include-mixins.scss";
```

# Release

`npm version patch -m "Release %s"` 
