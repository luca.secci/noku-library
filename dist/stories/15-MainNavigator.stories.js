import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import MainNavigator from '../Layout/MainNavigator/MainNavigator';

export default {
  title: 'Main Navigator'
};

const menuItems = [
  {
    destination: '/',
    title: 'Home',
    linkType: 'internal'
  },
  {
    destination: '/test',
    title: 'Something',
    linkType: 'internal'
  },
  {
    destination: 'https://google.com',
    title: 'Google',
    linkType: 'external'
  }
];

export const MainNavigatorDemo = () => {
  const RightSlot = (
    <div>
      Hello, right slot here
    </div>
  );

  return (
    <Router>
      <MainNavigator items={menuItems} logoSrc="https://via.placeholder.com/70x25/0000FF" reverseLogoSrc="https://via.placeholder.com/70x25" rightSlot={RightSlot} />
    </Router>
  );
};
