import React from 'react';
import BottomNavbar from '../Input/BottomNavbar/BottomNavbar';

export default {
  title: 'Bottom Navbar'
};

const menuItems = [
  {
    content: <span>asd</span>,
    isActive: true,
    isFab: false,
    link: "/asd",
  },
  {
    content: <span>asd2</span>,
    isActive: true,
    isFab: false,
    link: "/asd2",
  },
  {
    content: <span>asd3</span>,
    isActive: true,
    isFab: false,
    link: "/asd3",
  },
  {
    content: <span>asd4</span>,
    isActive: true,
    isFab: false,
    link: "/asd4",
  },
  {
    content: <span>asd4</span>,
    isActive: true,
    isFab: false,
    link: "/asd4",
  },
  {
    content: <span>asd4</span>,
    isActive: true,
    isFab: false,
    link: "/asd4",
  },

]

export const UnlockWalletDemo = () => <BottomNavbar menuItems={menuItems} autoWidth={true} />;
