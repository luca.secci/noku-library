import React from 'react';
import Checkbox from '../Input/Checkbox/Checkbox';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Checkbox'
};

export const CheckboxDemo = () => {
  return (
    <Checkbox changed={action('changed')} />
  );
};
