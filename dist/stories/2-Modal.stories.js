import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';
import ModalComponent from "../Output/Modal/Modal";
import Button from "../Input/Button/Button";

const modalStory = {
  title: 'Modal',
  component: ModalComponent
};

export default modalStory;

const withClick = (callback = () => null) => () => {
  action('clicked');
  callback();
};

export const Modal = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <Button onClick={withClick(() => setIsOpen(!isOpen))}>
        Open Modal
      </Button>
      <ModalComponent
        isOpen={isOpen}
        onClose={withClick(() => setIsOpen(!isOpen))}
        actions={<Button onClick={withClick(() => setIsOpen(!isOpen))}> Confirm </Button>}
        isLoading={false}
        title={'Modal Title'}
        hasProgressBar={true}
        progressPercent={50}
      >
        <span>Modal content</span>
      </ModalComponent>
    </>
  );
};

export const ModalPrimaryActions = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <Button onClick={withClick(() => setIsOpen(!isOpen))}>
        Open Modal
      </Button>
      <ModalComponent
        isOpen={isOpen}
        onClose={withClick(() => setIsOpen(!isOpen))}
        actions={<Button onClick={withClick(() => setIsOpen(!isOpen))}> Confirm </Button>}
        actionsBackgroundColor="primary"
        title={'Modal Title'}
        titleColor={'primary'}
        isLoading={false}
        hasProgressBar={true}
        progressPercent={50}
      >
        <span>Modal content</span>
      </ModalComponent>
    </>
  );
};

export const ModalClose = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <Button onClick={withClick(() => setIsOpen(!isOpen))}>
        Open Modal
      </Button>
      <ModalComponent
        isOpen={isOpen}
        onClose={withClick(() => setIsOpen(!isOpen))}
        actions={<Button onClick={withClick(() => setIsOpen(!isOpen))}> Confirm </Button>}
        isLoading={false}
        title={'Modal Title'}
        hasProgressBar={true}
        progressPercent={50}
        showCloseIcon={true}
      >
        <span>Modal content</span>
      </ModalComponent>
    </>
  );
};

export const ModalDisableClose = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <Button onClick={withClick(() => setIsOpen(!isOpen))}>
        Open Modal
      </Button>
      <ModalComponent
        isOpen={isOpen}
        onClose={withClick(() => setIsOpen(!isOpen))}
        actions={<Button onClick={withClick(() => setIsOpen(!isOpen))}> Confirm </Button>}
        isLoading={false}
        title={'Modal Title'}
        hasProgressBar={true}
        progressPercent={50}
        showCloseIcon={true}
        disableCloseIcon={true}
        disableCancel={true}
      >
        <span>Modal content</span>
      </ModalComponent>
    </>
  );
};

export const ModalNoCancel = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <Button onClick={withClick(() => setIsOpen(!isOpen))}>
        Open Modal
      </Button>
      <ModalComponent
        isOpen={isOpen}
        onClose={withClick(() => setIsOpen(!isOpen))}
        actions={<Button onClick={withClick(() => setIsOpen(!isOpen))}> Confirm </Button>}
        isLoading={false}
        title={'Modal Title'}
        hasProgressBar={true}
        progressPercent={50}
        hideCancel={true}
      >
        <span>Modal content</span>
      </ModalComponent>
    </>
  );
};

export const ModalHiddenCancel = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <Button onClick={withClick(() => setIsOpen(!isOpen))}>
        Open Modal
      </Button>
      <ModalComponent
        isOpen={isOpen}
        onClose={withClick(() => setIsOpen(!isOpen))}
        actions={<Button onClick={withClick(() => setIsOpen(!isOpen))}> Confirm </Button>}
        isLoading={false}
        title={'Modal Title'}
        hasProgressBar={true}
        progressPercent={50}
        disableCancel={true}
      >
        <span>Modal content</span>
      </ModalComponent>
    </>
  );
};

export const FramedModal = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <Button onClick={withClick(() => setIsOpen(!isOpen))}>
        Open Modal
      </Button>
      <ModalComponent
        isOpen={isOpen}
        onClose={withClick(() => setIsOpen(!isOpen))}
        actions={<Button onClick={withClick(() => setIsOpen(!isOpen))}> Confirm </Button>}
        isLoading={false}
        title={'Modal Title'}
        hasProgressBar={true}
        progressPercent={50}
        disableCancel={true}
        removeNavigatorHeight={true}
      >
        <span>Modal content</span>
      </ModalComponent>
    </>
  );
};

const LoremIpsum = `
Stwike him, Centuwion! Stwike him vewy wuffly! And Dinsdale says 'I hear you've been a naughty boy, Clement', and he splits me nostrils open, saws me leg off and pulls me liver out. And I tell him 'My name's not Clement', and then he loses his temper and nails my head to the floor. Nobody expects the Spanish Inquisition! Well, I think I should point out first, Brian, in all fairness, we are not, in fact, the rescue committee. However, I have been asked to read the following prepare statement on behalf of the movement. "We the People's Front of Judea, brackets, officials, end brackets, do hereby convey our sincere fraternal and sisterly greetings to you, Brian, on this, the occasion of your martyrdom."

I'm not a roman mum, I'm a kike, a yid, a heebie, a hook-nose, I'm kosher mum, I'm a Red Sea pedestrian, and proud of it! Oh, what wouldn't I give to be spat at in the face? I sometimes hang awake at night, dreaming of being spat at in the face I'm afraid I have no choice but to sell you all for scientific experiments.

If we took the bones out, it wouldn't be crunchy, would it? Listen, strange women lyin' in ponds distributin' swords is no basis for a system of government. Supreme executive power derives from a mandate from the masses, not from some farcical aquatic ceremony. Get on with it! Why is it the world never remembered the name of Johann Gambolputty de von Ausfern-schplenden-schlitter-crasscrenbon-fried-digger-dangle-dongle-dungle-burstein-Von-knacker-thrasher-apple-banger-horowitz-ticolensic-grander-knotty-spelltinkle-grandlich-grumblemeyer-spelterwasser-kurstlich-himbleeisen-bahnwagen-gutenabend-bitte-ein-nürnburger-bratwurstle-gerspurten-mit-zwei-macheluber-hundsfut-gumberaber-shoenendanker-kalbsfleisch-mittler-aucher von Hautkopft of Ulm? Well, had I got as far as the penis entering the vagina? Oh and Jenkins, apparently your mother died this morning. You don't frighten us, English pig dogs. Go and boil your bottoms, you sons of a silly person. I blow my nose at you, so-called "Arthur King," you and all your silly English K-nig-hts.

This morning, shortly after 11:00, comedy struck this little house on Dibley Road. Sudden, violent comedy. What is the capital of Assyria? Shut your festering gob, you tit! Your type really makes me puke you vacuous, toffy-nosed, malodorous pervert! This is boring. Let's go watch a stoning. I told you to lay off the beans, you whore!

If a sperm is wasted God gets quite irate. We use only the finest baby frogs, dew picked and flown from Iraq, cleansed in finest quality spring water, lightly killed, and then sealed in a succulent Swiss quintuple smooth treble cream milk chocolate envelope and lovingly frosted with glucose. All right, but apart from the sanitation, medicine, education, wine, public order, irrigation, roads, the fresh water system and public health, what have the Romans ever done for us?
`;

export const ModalWithExternalCloseButton = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <Button onClick={withClick(() => setIsOpen(!isOpen))}>
        Open Modal
      </Button>
      <ModalComponent
        isOpen={isOpen}
        onClose={withClick(() => setIsOpen(!isOpen))}
        actions={<Button onClick={withClick(() => setIsOpen(!isOpen))}> Confirm </Button>}
        isLoading={false}
        title={'Modal Title'}
        hasProgressBar={true}
        progressPercent={50}
        disableCancel={true}
        externalCloseIconButton={true}
        showCloseIcon={true}
        scroll="body"
      >
        <p> {LoremIpsum} </p>
        <p> {LoremIpsum} </p>
        <p> {LoremIpsum} </p>
        <p> {LoremIpsum} </p>
        <p> {LoremIpsum} </p>
      </ModalComponent>
    </>
  );
};
