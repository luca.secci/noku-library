import React from 'react';
import CroppedBordersButton from '../Input/CroppedBordersButton/CroppedBordersButton';

const CroppedBordersButtonStory = {
  title: "Cropped Borders Button"
};

export const CroppedBordersButtonComponent = () => {
  return (
    <div style={{ padding: "20px" }}>
      <CroppedBordersButton>Request Access</CroppedBordersButton>
    </div>
  );
};


export default CroppedBordersButtonStory;
