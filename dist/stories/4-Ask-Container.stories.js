import React from 'react';
import AskContainer from '../Input/AskContainer/AskContainer';

export default {
  title: 'Ask Container'
};

export const AskContainerDemo = () => {
  return (
    <div style={{ position: 'relative', width: '75%', height: '20em' }}>
      <AskContainer askContainerAction="ask" quantity={5} tokenName="bigMoney" addressTo="Destination Address"></AskContainer>
      <p> Long text... </p>
    </div>
  );
};
