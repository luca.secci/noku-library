import React from 'react';
import LargeButton from '../Input/LargeButton/LargeButton';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Large Button'
};

export const LargeButtonDemo = () => {
  return (
    <LargeButton onClick={action('clicked')} text="lorem ipsum dolor sit amet" />
  );
};
