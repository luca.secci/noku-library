import React, { useState } from 'react';
import Select from '../Input/Select/Select';

const SelectStory = {
  title: "Select"
};

const options = [{key: "opt1", value: "opt1", label: "Option 1"}, {key: "opt2", value: "opt2", label: "Option 2"}];

export const SelectComponent = () => {
  const [selectedOption, setSelectedOption] = useState("opt1");

  const onChange = (option) => {
    console.log(option);
    setSelectedOption(option);
  };

  return <Select label="Select demo" options={options} onChange={onChange} value={selectedOption} />;
};

export const SmallSelectComponent = () => {
  const [selectedOption, setSelectedOption] = useState("opt1");
  const onChange = (option) => {
    console.log(option);
    setSelectedOption(option);
  };

  return <Select label="Select demo" options={options} onChange={onChange} value={selectedOption} size="small" />;
};

export default SelectStory;
