import React from 'react';
import { action } from '@storybook/addon-actions';

import CroppedBordersCard from '../Others/CroppedBordersCard/CroppedBordersCard';

export default {
  title: 'Cropped borders card',
  component: CroppedBordersCard
};

export const Card = () => (
  <div style={{ maxWidth: '10em', padding: '1em' }}>
    <CroppedBordersCard onClick={action('clicked')}>
      <div> test123 </div>
      <div> test123 </div>
    </CroppedBordersCard>
  </div>
);
