import React, { useState } from 'react';
import BaseInput from '../Input/BaseInput/BaseInput';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default {
  title: 'Base Input'
};

const LimitWidth = (props) => <div className="max-width-10rem"> {props.children} </div>;

export const BaseInputText = () => <LimitWidth> <BaseInput /> </LimitWidth>;

export const BaseInputSelect = () => {
  const options = [
    {
      label: 'Option 1',
      value: 'opt1'
    },
    {
      label: 'Option 2',
      value: 'opt2'
    }
  ];

  return <LimitWidth> <BaseInput type="select" elementConfig={{ options }} placeholder="test" stateKey="select" changed={(key, value) => console.log(key, value)} /> </LimitWidth>;
};

export const TextInput = () => {
  const [value, setValue] = useState('');

  return (
    <LimitWidth>
      <BaseInput
        stateKey="demo"
        value={value}
        changed={(key, value) => setValue(value)}
        label="Text BaseInput"
        placeholder="Placeholder"
        type="text"
      />
    </LimitWidth>
  );
};

export const NumberInput = () => {
  const [value, setValue] = useState('');

  return (
    <LimitWidth>
      <BaseInput
        stateKey="demo"
        value={value}
        changed={(key, value) => setValue(value)}
        label="Number BaseInput"
        placeholder="Placeholder"
        type="number"
      />
    </LimitWidth>
  );
};

export const TextareaInput = () => {
  const [value, setValue] = useState('');

  return (
    <LimitWidth>
      <BaseInput
        stateKey="demo"
        value={value}
        changed={(key, value) => setValue(value)}
        label="TextArea"
        placeholder="Long text here"
        type="textarea"
      />
    </LimitWidth>
  );
};

export const AfterIconInput = () => {
  const [value, setValue] = useState('');
  const afterIcon = <FontAwesomeIcon icon={['fas', 'user']} />;

  return (
    <LimitWidth>
      <BaseInput
        stateKey="demo"
        value={value}
        changed={(key, value) => setValue(value)}
        label="Hello there"
        placeholder="General Kenobi"
        type="text"
        afterIcon={afterIcon}
      />
    </LimitWidth>
  );
};
