import React, { useState } from 'react';
// import { action } from '@storybook/addon-actions';
import CustomSlider from "../Others/CustomSlider/CustomSlider";
import Button from "../Input/Button/Button";
import BaseStep from "../Others/BaseStep/BaseStep";

export default {
  title: 'Custom Slider'
};

export const CustomSliderDemo = () => {
  const [activeStep, setActiveStep] = useState(0);

  const steps = [
    <div key="step_0">
      <Button onClick={() => setActiveStep(1)}> Next </Button> <br />
      Hello There
    </div>,
    <div key="step_1">
      <Button onClick={() => setActiveStep(0)}> Go Back </Button>
      General Kenobi
    </div>
  ];

  return (
    <CustomSlider
      steps={steps}
      activeStep={activeStep}
      isModal={true}
      slideWidth="480px"
    />
  );
};

export const CustomSliderWithBaseStep = () => {
  const [activeStep, setActiveStep] = useState(0);

  const totalSteps = 2;

  const goForward = () => {
    if (activeStep < totalSteps) {
      setActiveStep(activeStep + 1);
    }
  };

  const goBack = () => {
    if (activeStep > 0) {
      setActiveStep(activeStep - 1);
    }
  };

  const steps = [
    <BaseStep
      key="step_0"
      goBack={goBack}
      goBackTitle="You can't see me ;)"
      hideBack={true}
    >
      <Button onClick={goForward}> Next </Button> <br />
      Hello There
    </BaseStep>,
    <BaseStep
      key="step_1"
      goBack={goBack}
      goBackTitle="You can see me D:"
      hideBack={false}
    >
      General Kenobi
    </BaseStep>
  ];

  return (
    <CustomSlider
      steps={steps}
      activeStep={activeStep}
      isModal={true}
      slideWidth="480px"
    />
  );
};
