import React, { useEffect, useState } from 'react';
import { action } from '@storybook/addon-actions';
import Button from '../Input/Button/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { BrowserRouter as Router } from 'react-router-dom';

const buttonStory = {
  title: 'Button',
  component: Button
};

export default buttonStory;

export const Default = () => <Button onClick={action('clicked')}> Primary </Button>;
export const LightPrimary = () => <Button color="light-primary" onClick={action('clicked')}> Light Primary </Button>;
export const DarkPrimary = () => <Button color="dark-primary" onClick={action('clicked')}> Dark Primary </Button>;
export const WhitePrimary = () => <Button color="white-primary" onClick={action('clicked')}> Primary </Button>;
export const Secondary = () => <Button onClick={action('clicked')} color="secondary"> Secondary </Button>;
export const Warning = () => <Button onClick={action('clicked')} color="warning"> Warning </Button>;
export const Error = () => <Button onClick={action('clicked')} color="error"> Error </Button>;
export const Confirm = () => <Button onClick={action('clicked')} color="confirm"> Confirm </Button>;
export const Discard = () => <Button onClick={action('clicked')} color="discard"> Discard </Button>;
export const FilledGradient = () => <Button onClick={action('clicked')} color="gradient"> Gradient </Button>;

export const Flat = () => <Button onClick={action('clicked')} style="flat"> Flat </Button>;

export const WithIcon = () => (
  <>
    <Button startIcon={<FontAwesomeIcon icon={['fas', 'wallet']} />}> UPPERCASE </Button> <br />
    <Button startIcon={<FontAwesomeIcon icon={['fas', 'user']} />}> Left </Button> <br />
    <Button endIcon={<FontAwesomeIcon icon={['fas', 'user']} />}> Right </Button> <br />
  </>
);
export const Disabled = () => <Button onClick={action('This should not appear')} disabled> Disabled </Button>;
export const ButtonsWithLink = () => (
  <Router>
    <Button onClick={action('clicked')} link="/app"> Go somewhere in app </Button>
    <Button onClick={action('clicked')} link="https://google.com" linkType="external"> Go somewhere outside </Button>
  </Router>
);
export const LoadingButtons = () => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const intervalRef = setInterval(() => {
      setLoading((prev) => !prev);
    }, 2000);
    return () => {
      clearInterval(intervalRef);
    }
  }, []);

  return (
    <>
      <Button onClick={action('This should not appear')} loading={loading}> Loadinggggg </Button>
      <Button onClick={action('clicked')} color="secondary" loading={loading}> Button </Button>
      <Button onClick={action('clicked')} color="confirm" loading={loading}> Confirm </Button>
      <Button onClick={action('clicked')} color="warning" loading={loading}> Warning </Button>
      <Button onClick={action('clicked')} color="error" loading={loading}> Error </Button>
    </>
  );
};

export const Outlined = () => <Button onClick={action('clicked')} variant="outlined"> Button </Button>;
export const OutlinedLightPrimary = () => <Button onClick={action('clicked')} variant="outlined" color="light-primary"> Button </Button>;
export const OutlinedNeutral = () => <Button onClick={action('clicked')} variant="outlined" color="neutral"> Button </Button>;
