/**
 * Utility function for something that is not implemented
 */
export const unimplementedFunction = () => {};
/**
 * Utility promise function for something that is not implemented
 */

export const unimplementedPromise = () => Promise.resolve();