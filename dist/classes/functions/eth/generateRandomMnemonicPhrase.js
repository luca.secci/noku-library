import * as bip39 from 'bip39';
export const generateRandomMnemonicPhrase = async () => {
  let mnemonic = '';
  let isValidPhrase = false;

  while (!isValidPhrase) {
    mnemonic = bip39.generateMnemonic();
    isValidPhrase = bip39.validateMnemonic(mnemonic);
  }

  return mnemonic;
};