import eth4you from 'eth4you';
import BigNumber from 'bignumber.js';
import { TooltipObject } from '../../TooltipObject/TooltipObject';
export const getTooltipInfoETHLike = (conversionRates = {}, gasPrice, gasLimit, estimatedMinutes) => {
  let tooltipObject = {};

  if (gasPrice && gasLimit && gasLimit > 0) {
    // (gasPrice [GWEI] * 10^9) [WEI] * (gasLimit [WEI]) = [WEI]
    // 1 WEI = 10^(-18) ETH, 1 GWEI = 10^(-9) ETH
    // etherizeN(WEI -> ETH)
    const gasPriceBN = new BigNumber(gasPrice);
    const gasLimitBN = new BigNumber(gasLimit);
    const baseEthFee = eth4you.etherizeN(gasPriceBN.times(gasLimitBN).times(new BigNumber(10).pow(9)));
    const baseETHFeeBN = new BigNumber(baseEthFee); // Make needed conversions

    const needMulti = [];

    for (const key in conversionRates) {
      if (Object.hasOwnProperty.call(conversionRates, key)) {
        const element = conversionRates[key];

        if (key !== 'ts' && key !== 'delayed') {
          needMulti.push({
            currency: key,
            value: baseETHFeeBN.times(element)
          });
        }
      }
    }

    tooltipObject = new TooltipObject(estimatedMinutes, [...needMulti]);
  }

  return tooltipObject;
};