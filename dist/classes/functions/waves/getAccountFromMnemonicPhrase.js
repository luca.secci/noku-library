import waves4you from 'waves4you';
/**
 * @param {"mainnet"|"testnet"} network
 * @param {string} mnemonicPhrase
 * @return {Promise<{phrase: any, privateKey: string, address: string}>}
 */

export const getAccountFromMnemonicPhrase = async (network, mnemonicPhrase) => {
  return waves4you.getAccountFromSeed(mnemonicPhrase, network);
};