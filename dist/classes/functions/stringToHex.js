function d2h(d) {
  return d.toString(16);
}
/**
 * @param {string} tmp
 */


export const stringToHex = tmp => {
  var str = '';
  var i = 0;
  var len = tmp.length;
  var c;

  for (; i < len; i += 1) {
    c = tmp.charCodeAt(i);
    str += d2h(c);
  }

  return str;
};
export default stringToHex;