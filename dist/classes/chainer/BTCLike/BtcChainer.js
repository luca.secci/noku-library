function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import * as btc4you from 'btc4you';
import eth4you from 'eth4you';
import BigNumber from 'bignumber.js';
import Chainer from '../Chainer';
import { hexToString } from '../../functions/hexToString';
import { getTooltipInfoBTCLike } from '../../functions/btc/getTooltipInfoBTCLike';
import stringToHex from '../../functions/stringToHex';
import asyncSleep from '../../../utils/asyncSleep';
import { BTCConstants } from '../../functions/getChainConstants';

class BTCChainer extends Chainer {
  constructor(chain = 'BTC', socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance) {
    super(chain, socketEndpoint, socketSwapEndpoint);
    this._hd = undefined; // Set constants

    this.setUnlockMethods(BTCConstants.unlockMethods);
    this.setWalletTypes(BTCConstants.walletTypes);
    this.setChainInfo(BTCConstants.chainInfo);
    this.intervalCheckHd = undefined;
    this.maxIntervalBeforeCheckHd = 5 * 60 * 1000;
    this.maxIntervalBeforeCheckUtxos = 10 * 1000; // this._address è il primo indirizzo di ricezione fondi libero e disponibile, non il primo della catena degli indirizzi di ricezione

    this.initWalletTypes();
    this._getUTXOFromHD = getUTXOFromHD;
    this._checkHDWallet = checkHDWallet;
    this._sendRawTransaction = sendRawTransaction;
    this._sendRawTransactionWithParams = sendRawTransactionWithParams;
    this._getTransaction = getTransaction;
    this._getBalance = getBalance;
  }

  initWalletTypes() {
    this.defaultWalletType = this._walletTypes[0].value;
    this.defaultDerivationPath = btc4you.getBIPDerivationPath(this._walletTypes[0].purpose, this.getChain(), 0);
  }

  dispose() {
    if (this.intervalCheckHd) {
      clearInterval(this.intervalCheckHd);
    }

    return super.dispose();
  }

  async validateAddress(address) {
    try {
      return btc4you.validateAddress(address, this.getChain());
    } catch (ex) {}

    return false;
  }

  async setPrivateKey(extendedPrivateKey) {
    const type = btc4you.getTypeFromExtendedPrivateKey(extendedPrivateKey, this.getChain());
    const firstReceivingAddress = btc4you.getAddress(btc4you.getLevelFromExtendedPrivateKey(extendedPrivateKey, this.getChain(), 0, 0), btc4you.getTypeFromExtendedPrivateKey(extendedPrivateKey, this.getChain()));
    let hd = {
      address: firstReceivingAddress,
      type: type,
      derivationPath: undefined,
      // per usi futuri ed incerti (ad es. "m/49'/0'/0'"). resta il problema che non so come ricavarlo dalla PK
      extendedPrivateKey: extendedPrivateKey,
      chain: this.getChain(),
      receiving: {
        addresses: [],
        privatekeys: [],
        sequenceoffset: 0,
        limitinarow: 10,
        zeroinarow: 0,
        addressestocheck: 10,
        freeaddress: -1
      },
      sending: {
        addresses: [],
        privatekeys: [],
        sequenceoffset: 0,
        limitinarow: 5,
        zeroinarow: 0,
        addressestocheck: 5,
        freeaddress: -1
      },
      utxos: {
        lastupdate: Math.floor(new Date(0)),
        array: []
      }
    };
    this.isHDWallet = true;
    hd = this._addWalletsToHD(hd, true);
    hd = this._addWalletsToHD(hd, false);
    sessionStorage.setItem('private-key-' + firstReceivingAddress, extendedPrivateKey);
    const mnemonicPhrase = this.getMnemonicPhrase();

    if (mnemonicPhrase) {
      sessionStorage.setItem('mnemonic-phrase-' + firstReceivingAddress, mnemonicPhrase);
    }

    this.hd = hd;

    try {
      await this._getHdInfo(hd);
      this.unlock();
      await super.setPrivateKey(extendedPrivateKey);
      return extendedPrivateKey;
    } catch (ex) {
      return false;
    }
  } // by default (first = false) returns the REAL receiving address (the first 'free' receiving address) of the hd wallet, not the first one!!!


  getAddress(first = false) {
    if (this.address && !first) {
      return this.address;
    }

    if (!this.unlocked) {
      return this.address;
    }

    let address;

    if (this._hd) {
      address = first ? this._hd.receiving.addresses[0] : this._hd.receiving.addresses[this._hd.receiving.freeaddress];
    }

    if (address) {
      return address;
    }

    return false;
  }

  get firstAddress() {
    if (!this.unlocked) {
      return this.address;
    }

    if (this._hd) {
      return this._hd.receiving.addresses[0];
    }

    return null;
  } // HD getter


  get hd() {
    if (this._hd) {
      return this._hd;
    }

    const hdFromStorage = JSON.parse(sessionStorage.getItem('hd-wallet-' + this.firstAddress));
    this.hd = hdFromStorage;
    return this._hd;
  } // HD Setter


  set hd(value) {
    if (value) {
      var _this$onAddressListCh;

      this._hd = _objectSpread({}, value); // this.createAndDispatchEvent(ChainerEvents.HDWalletChanged, this.addressList);

      (_this$onAddressListCh = this.onAddressListChange) === null || _this$onAddressListCh === void 0 ? void 0 : _this$onAddressListCh.call(this, this.addressList);
      sessionStorage.setItem('hd-wallet-' + value.address, JSON.stringify(value));
    }
  }

  generateJson(password, _additionalInfo = {}) {
    // additionalInfo è per usi futuri
    const privateKey = this.getPrivateKey();

    if (!privateKey) {
      throw new Error('no private key');
    }

    const privateKeyHex = stringToHex(privateKey);
    const additionalInfo = {};

    if (this.hd) {
      additionalInfo.type = this.hd.type;
      additionalInfo.derivationPath = this.hd.derivationPath;
    }

    const string = eth4you.toV3(privateKeyHex, password, this.getName(), undefined, this.getChain(), additionalInfo, this.getMnemonicPhrase());
    return JSON.parse(string);
  }

  async unlockJsonWallet(text = '{}', password = undefined) {
    try {
      const json = JSON.parse(text);
      const fileContent = eth4you.fromFile_remastered(text, password);
      let privateKey = fileContent.key;
      this.setMnemonicPhrase(fileContent.mnemonicPhrase);

      if (!privateKey) {
        throw new Error('wrong password');
      }

      if (json && (json.chain || '').toUpperCase() !== this.chain) {
        throw new Error('JSON mismatch with current private key');
      }

      privateKey = hexToString(privateKey);
      await this.setPrivateKey(privateKey);

      if (json.wallet_name) {
        this.setName(json.wallet_name);
      }

      return true;
    } catch (ex) {
      throw new Error(ex.message);
    }
  }

  async generateRandomMnemonicPhrase() {
    const mnemonic = btc4you.generateBIP32Mnemonic();
    this.setMnemonicPhrase(mnemonic);
    return mnemonic;
  }

  async getAccountFromMnemonicPhrase(mnemonicPhrase = '', options = {}, saveMnemonicPhrase) {
    if (!options.derivationPath) {
      options.derivationPath = this.defaultDerivationPath;
    }

    if (!options.type) {
      options.type = this.defaultWalletType;
    }

    this.setMnemonicPhrase(mnemonicPhrase, saveMnemonicPhrase);
    const privateKey = btc4you.getExtendedPrivateKey(btc4you.getSeedFromMnemonic(mnemonicPhrase), this.getChain(), options.derivationPath, options.type);
    await this.setPrivateKey(privateKey);

    if (this.hd) {
      this.hd.derivationPath = options.derivationPath;
    }

    return this;
  }

  getDerivationPath(purpose, account, change, index) {
    return btc4you.getBIPDerivationPath(purpose, this.getChain(), account, change, index);
  }

  async loadFromStore(address) {
    if (!(await this.validateAddress(address))) {
      throw new Error('invalid address');
    } // await super.loadFromStore(address);


    this.address = address;
    this.getMnemonicPhrase();
    const privateKey = sessionStorage.getItem('private-key-' + address);

    if (privateKey) {
      var _this$hd, _this$hd$receiving;

      this.setPrivateKey(privateKey);
      this.unlock();
      this.hd = JSON.parse(sessionStorage.getItem('hd-wallet-' + address));

      if (((_this$hd = this.hd) === null || _this$hd === void 0 ? void 0 : (_this$hd$receiving = _this$hd.receiving) === null || _this$hd$receiving === void 0 ? void 0 : _this$hd$receiving.freeaddress) > -1) {
        this.isHDWallet = true;
        this.address = this.hd.receiving.addresses[this.hd.receiving.freeaddress];
        await this._postLoadHDWalletFromStore(this.hd);
      } else {
        await this.setPrivateKey(privateKey);

        this._keepHDWalletUpdated();
      }

      return privateKey;
    }

    return false;
  }

  _addWalletsToHD(hd, isReceiving) {
    const account = isReceiving ? hd.receiving : hd.sending;
    const indexStart = (account.addresses || []).length;
    const newAddressesLength = isReceiving ? 20 : 5;
    const newWallets = btc4you.getArrayOfLevelsFromExtendedPrivateKey(hd.extendedPrivateKey, hd.chain, isReceiving ? 0 : 1, indexStart, newAddressesLength);

    for (let i = 0; i < newWallets.length; i++) {
      if (isReceiving) {
        hd.receiving.addresses.push(btc4you.getAddress(newWallets[i], hd.type));
        hd.receiving.privatekeys.push(btc4you.getPrivateKey(newWallets[i], hd.type));
      } else {
        hd.sending.addresses.push(btc4you.getAddress(newWallets[i], hd.type));
        hd.sending.privatekeys.push(btc4you.getPrivateKey(newWallets[i], hd.type));
      }
    }

    this.hd = hd;
    return hd;
  }

  async _getHdInfo(hd) {
    while (hd.receiving.addresses.length < hd.receiving.sequenceoffset + hd.receiving.addressestocheck) {
      hd = this._addWalletsToHD(hd, true);
    }

    const requestReceiving = [hd.chain, hd.receiving.addresses.slice(hd.receiving.sequenceoffset, hd.receiving.sequenceoffset + hd.receiving.addressestocheck), hd.receiving.sequenceoffset, hd.receiving.limitinarow, hd.receiving.zeroinarow];

    while (hd.sending.addresses.length < hd.sending.sequenceoffset + hd.sending.addressestocheck) {
      hd = this._addWalletsToHD(hd, false);
    }

    const requestSending = [hd.chain, hd.sending.addresses.slice(hd.sending.sequenceoffset, hd.sending.sequenceoffset + hd.sending.addressestocheck), hd.sending.sequenceoffset, hd.sending.limitinarow, hd.sending.zeroinarow];
    let updateComplete = false;

    while (!updateComplete) {
      try {
        const receivingCheckPromise = hd.receiving.freeaddress > -1 ? null : this._checkHDWallet(...requestReceiving);
        const sendingCheckPromise = hd.sending.freeaddress > -1 ? null : this._checkHDWallet(...requestSending);
        const responses = await Promise.all([receivingCheckPromise, sendingCheckPromise]);

        if (responses[0]) {
          const responseReceiving = responses[0];
          hd.receiving.sequenceoffset = responseReceiving.sequenceoffset;
          hd.receiving.limitinarow = responseReceiving.limitinarow;
          hd.receiving.zeroinarow = responseReceiving.zeroinarow;
          hd.receiving.freeaddress = responseReceiving.freeaddress;
        }

        if (responses[1]) {
          const responseSending = responses[1];
          hd.sending.sequenceoffset = responseSending.sequenceoffset;
          hd.sending.limitinarow = responseSending.limitinarow;
          hd.sending.zeroinarow = responseSending.zeroinarow;
          hd.sending.freeaddress = responseSending.freeaddress;
        }

        if (hd.receiving.freeaddress > -1 && hd.sending.freeaddress > -1) {
          hd.lastupdate = Math.floor(Date.now());
        }

        this.hd = hd;
        updateComplete = true;
      } catch (ex) {
        // ignore the fail and keep trying...
        console.log(ex);
        await asyncSleep(500);
      }
    }

    if (hd.receiving.freeaddress > -1 && hd.sending.freeaddress > -1) {
      this.address = hd.receiving.addresses[hd.receiving.freeaddress];
      return hd;
    } else {
      return await this._getHdInfo(hd);
    }
  }

  async _postLoadHDWalletFromStore(hd) {
    if (hd) {
      if (Math.floor(Date.now() - hd.lastupdate < this.maxIntervalBeforeCheckHd) && hd.receiving.freeaddress !== -1 && hd.sending.freeaddress !== -1) {
        const timeout = hd.lastupdate ? Math.max(0, this.maxIntervalBeforeCheckHd - Math.floor(Date.now() - hd.lastupdate)) : 0;
        setTimeout(() => {
          if (this.hd) {
            hd.receiving.freeaddress = -1;
            hd.sending.freeaddress = -1;

            this._getHdInfo(hd);

            this._keepHDWalletUpdated();
          }
        }, timeout);
        return hd;
      } else {
        hd.receiving.freeaddress = -1;
        hd.sending.freeaddress = -1;
        await this._getHdInfo(hd);

        this._keepHDWalletUpdated();

        return hd;
      }
    }
  }

  async _keepHDWalletUpdated() {
    this.intervalCheckHd = setInterval(() => {
      if (this.hd) {
        this.hd.receiving.freeaddress = -1;
        this.hd.sending.freeaddress = -1;

        this._getHdInfo(this.hd);
      } else {
        if (this.intervalCheckHd) {
          clearInterval(this.intervalCheckHd);
        }
      }
    }, this.maxIntervalBeforeCheckHd);
  }

  async getUTXOFromHD(hd) {
    let utxos;

    if (hd) {
      if (Math.floor(Date.now() - hd.utxos.lastupdate > this.maxIntervalBeforeCheckUtxos)) {
        const addresses = [];
        Array.prototype.push.apply(addresses, hd.receiving.addresses.slice(0, hd.receiving.freeaddress + 5));
        Array.prototype.push.apply(addresses, hd.sending.addresses.slice(0, hd.sending.freeaddress + 5));

        while (utxos === undefined) {
          try {
            utxos = await this._getUTXOFromHD(addresses, hd.chain);
          } catch (ex) {
            // ignore the fail and keep trying...
            console.log(ex);
            await asyncSleep(500);
          }
        }

        hd.utxos.array = utxos;
        hd.utxos.lastupdate = new Date().getTime();
      } else {
        utxos = hd.utxos.array;
      }
    }

    return utxos;
  }

  _generateTransactionFromHDWallet(hd, utxoarray, to, amount, feerate, fixedFee, hasChange) {
    for (let i = 0; i < utxoarray.length; i++) {
      let index = hd.sending.addresses.findIndex(address => address === utxoarray[i].address);

      if (index > -1) {
        utxoarray[i].privateKey = hd.sending.privatekeys[index];
      } else {
        index = hd.receiving.addresses.findIndex(address => address === utxoarray[i].address);

        if (index > -1) {
          utxoarray[i].privateKey = hd.receiving.privatekeys[index];
        }
      }
    }

    return btc4you.generateTransaction(utxoarray, hasChange ? hd.sending.addresses[hd.sending.freeaddress] : '', [{
      address: to,
      value: amount
    }], hd.chain, hd.type, fixedFee ? null : feerate, fixedFee);
  }

  _estimateTransactionSizeFromHDWallet(hd, utxoarray, to, amount, hasChange) {
    for (let i = 0; i < utxoarray.length; i++) {
      let index = hd.sending.addresses.findIndex(address => address === utxoarray[i].address);

      if (index > -1) {
        utxoarray[i].privateKey = hd.sending.privatekeys[index];
      } else {
        index = hd.receiving.addresses.findIndex(address => address === utxoarray[i].address);

        if (index > -1) {
          utxoarray[i].privateKey = hd.receiving.privatekeys[index];
        }
      }
    }

    return btc4you.estimateTransactionSize(utxoarray, hasChange ? hd.sending.addresses[hd.sending.freeaddress] : '', [{
      address: to,
      value: amount
    }], hd.chain, hd.type);
  }

  _postSendTransactionHDWallet(hd, hasChange) {
    if (hasChange) {
      hd.sending.freeaddress += 1;
      hd.sending.sequenceoffset += 1;
      hd.sending.zeroinarow = 0;

      while (hd.sending.addresses.length < hd.sending.sequenceoffset + hd.sending.addressestocheck) {
        hd = this._addWalletsToHD(hd, false);
      }

      this.hd = hd;
    }

    if (this._socketClient) {
      setTimeout(() => {
        const currentAddress = [];
        Array.prototype.push.apply(currentAddress, hd.receiving.addresses.slice(0, hd.receiving.freeaddress + 5));
        Array.prototype.push.apply(currentAddress, hd.sending.addresses.slice(0, hd.sending.freeaddress + 5));
        const query = {
          currentaddress: currentAddress,
          symbol: this.getChain()
        };
        this.emitGetBalance(query);
        this.emitGetTransactions(query);
      }, 1000 * 15);
    }

    return hd;
  }
  /**
  * Generate a transaction
  * @param {string} _fromAddress
  * @param {string} _toAddress
  * @param {Token} _currency unapplicable for BTCLike
  * @param {string} _value transaction value SAT,
  * @param {string} _data data field in transaction (unapplicable for BTCLike)
  * @param {number|string} feePrice fee price in SAT/B
  * @param {boolean} isFullBalanceTransaction if should send all transfer
  * @param {{ balance: string, [key: string]: any }} options
  * @return {Promise<{
    *   fee: string,
    *   totalFeeTx: BigNumber,
    *   rawTx: {
    *     chainId: number,
    *     [key: string]: any,
    *   },
    *   message?: string,
    * }>}
    */


  async generateTransaction(_fromAddress, _toAddress, _c, _value, _d, feePrice, isFullTransaction) {
    const defaultResponse = {
      fee: undefined,
      rawTx: undefined
    };

    try {
      const value = _value || '0x0';
      feePrice = parseInt(feePrice);
      const utxoArray = await this.getUTXOFromHD(this.hd);
      let BTCLikeTx = {};

      if (isFullTransaction) {
        let maxOutput = 0;

        for (let i = 0; i < utxoArray.length; i++) {
          maxOutput += utxoArray[i].value;
        }

        const sizeBytes = this._estimateTransactionSizeFromHDWallet(this.hd, utxoArray, _toAddress, Math.floor(maxOutput - btc4you.getAverageTransactionSize(this.getChain(), this._hd.type) * feePrice), false);

        const diffAmount = maxOutput - sizeBytes * feePrice;

        if (diffAmount > 0) {
          BTCLikeTx = this._generateTransactionFromHDWallet(this.hd, utxoArray, _toAddress, Math.floor(diffAmount), null, sizeBytes * feePrice, false);
        } else {
          throw new Error('Insufficient funds for sending this transaction. Try with lower fees');
        }
      } else {
        BTCLikeTx = this._generateTransactionFromHDWallet(this.hd, utxoArray, _toAddress, Math.floor(BigNumber(value).times(100000000).toFixed(0)), feePrice, null, true);
      }

      const fee = BigNumber(BTCLikeTx.fee).div(100000000).toString(10);
      return {
        fee: fee,
        totalFeeTx: fee,
        rawTx: BTCLikeTx.hex
      };
    } catch (ex) {
      return _objectSpread(_objectSpread({}, defaultResponse), {}, {
        message: ex.toString()
      });
    }
  }
  /**
   * @param {string} rawTx Raw transaction
   */


  async sendRawTransaction(rawTx) {
    return await this._sendRawTransaction(rawTx, undefined, this.getChain());
  }
  /**
   * @param {{rawTx: string}} param0 Signed tx
   * @param {{toAddress: string}} params
   */


  async sendSwapTransaction({
    rawTx
  }, params) {
    try {
      var _params$toAddress;

      const fromAddress = this.getAddress(true);
      const toAddress = (_params$toAddress = params === null || params === void 0 ? void 0 : params.toAddress) !== null && _params$toAddress !== void 0 ? _params$toAddress : this.getAddress(true);
      const type = params === null || params === void 0 ? void 0 : params.type;
      const swapSessionId = params === null || params === void 0 ? void 0 : params.swapSessionId;
      return await this._sendRawTransactionWithParams(rawTx, {
        fromAddress,
        toAddress
      }, this.getChain(), {
        type,
        swapSessionId
      });
    } catch (error) {
      throw new Error(error.message || error);
    }
  }

  async getTransaction(id) {
    if (!id) {
      throw new Error('missing parameter hash');
    }

    try {
      const chain = this.getChain();
      return await this._getTransaction(chain, id);
    } catch (ex) {}

    return null;
  } // WEBSOCKET FUNCTIONS & CALLBACKS


  wsocketInit(intervals, intervalBalance = true, intervalTransactions = true, query = {}, intervalInfoSendFunds = true) {
    const firstAddress = this.getAddress(true);
    let searchQuery = [];

    if (this.hd) {
      Array.prototype.push.apply(searchQuery, this.hd.receiving.addresses.slice(0, this.hd.receiving.freeaddress + 5));
      Array.prototype.push.apply(searchQuery, this.hd.sending.addresses.slice(0, this.hd.sending.freeaddress + 5));
    } else {
      searchQuery = firstAddress;
    }

    super.wsocketInit(intervals, intervalBalance, intervalTransactions, _objectSpread({
      currentaddress: searchQuery
    }, query), intervalInfoSendFunds);
    super.reAddSubscriptions();
  }

  setVariablesSubscription(type, result) {
    switch (type) {
      case 'gasPriceConfiguration':
        {
          const gasPriceList = [];

          for (let i = 0; i < Object.keys(result.levels).length; i++) {
            gasPriceList.push({
              name: Object.keys(result.levels)[i],
              value: result.levels[Object.keys(result.levels)[i]]
            });
          }

          result = {
            gasPriceList
          };
          this.gasPriceList = gasPriceList;
          break;
        }

      case 'balance':
        result = {
          lastUpdate: new Date().getTime(),
          // last unix timestamp of balance update
          balance: result.BTCBalance,
          // BTC balance
          rawBalance: result.BTCBalanceSatoshi,
          // BTC balance in satoshi
          maxDelay: result.maxDelay,
          type: result.type,
          // type of address
          balanceMulti: result.BTCPrice,
          // equivalent of 1 BTC in different chains
          totalBalance: result.BTCBalance,
          // equivalent of total balance in ethereum
          totalBalanceEquivalent: result.total // equivalent of total balance in different chains

        };
        this.cachedBalance = result;
        break;

      case 'transactions':
        break;

      default:
        break;
    }

    return result;
  } // onBalance(callback) {
  //   if (this.socketClient) {
  //     this.socketClient.on("balance", balance => {
  //       this.balance = {
  //         lastUpdate: new Date().getTime(), //last unix timestamp of balance update
  //         balance: balance.BTCBalance, //BTC balance
  //         rawBalance: balance.BTCBalanceSatoshi, //BTC balance in satoshi
  //         maxDelay: balance.maxDelay,
  //         type: balance.type, //type of address
  //         balanceMulti: balance.BTCPrice, //equivalent of 1 BTC in different chains
  //         totalBalance: balance.BTCBalance, //equivalent of total balance in ethereum
  //         totalBalanceEquivalent: balance.total //equivalent of total balance in different chains
  //       };
  //       callback(this.balance);
  //     });
  //   }
  // }
  // onTransactions(callback) {
  //   if (this.socketClient) {
  //       this.socketClient.on("transactions", data => {
  //         let transactions = [];
  //         const pow = parseFloat(`1e${this.chainInfo.decimals}`);
  //         for (var tx of data.transactions) {
  //           let transaction = {
  //             ...tx,
  //             date: moment(tx.ts * 1000).format("MMMM YYYY, DD - HH:mm:ss"),
  //             action: 'transfer',
  //             fee: tx.fee / pow,
  //             value: tx.value / pow,
  //             status: parseInt(tx.blockNumber) <= 0 ? "pending" : "",
  //           }
  //           for(var i of (transaction.inputs || [])){
  //             i.btcValue = i.value / pow;
  //           }
  //           for(var o of (transaction.outputs || [])){
  //             o.btcValue = o.value / pow;
  //           }
  //           transactions.push(transaction);
  //         }
  //           callback({transactions: transactions, totalItems: data.totalitems || 0});
  //       });
  //   }
  // }
  // onGasPriceConfiguration(callback) {
  //   if (this.socketClient) {
  //       this.socketClient.on("gasPriceConfiguration", message => {
  //           let result = {};
  //           let gasPriceList = [];
  //           for(let i = 0; i < Object.keys(message.levels).length; i++){
  //             gasPriceList.push({
  //               name: Object.keys(message.levels)[i],
  //               value: message.levels[Object.keys(message.levels)[i]],
  //             })
  //           }
  //           result.gasPriceList = gasPriceList;
  //           callback(result);
  //       });
  //   }
  // }


  get addressList() {
    if (this.hd) {
      const addressList = []; // Receiving addresses
      // freeAddress is the index of the first free address

      const hdReceiving = this.hd.receiving;
      const receivingAddressList = hdReceiving.addresses.slice(0, hdReceiving.freeaddress + 5); // Sending addresses

      const hdSending = this.hd.sending;
      const sendingAddressList = hdSending.addresses.slice(0, hdSending.freeaddress + 5);
      addressList.push(...receivingAddressList);
      addressList.push(...sendingAddressList);
      return addressList;
    }

    return [this.firstAddress];
  }

  async getBalance(_noTokens, _limitTokens, cancelToken) {
    let response;
    const chain = this.getChain();

    if (this.hd) {
      const addresses = [];
      Array.prototype.push.apply(addresses, this.hd.receiving.addresses.slice(0, this.hd.receiving.freeaddress + 5));
      Array.prototype.push.apply(addresses, this.hd.sending.addresses.slice(0, this.hd.sending.freeaddress + 5));
      response = await this._getBalance(chain, addresses, cancelToken);
    } else {
      response = await this._getBalance(chain, this.getAddress(true), cancelToken);
    }

    this.setBalance(response);
    return response;
  } // feerate (SAT/BYTE) * transaction size (BYTE) (da get average transaction size) / 10^8 = BITCOIN


  getTooltipInfo(multi, feeRate, ...params) {
    var _this$_hd;

    return getTooltipInfoBTCLike(multi, feeRate, this.getChain(true), this === null || this === void 0 ? void 0 : (_this$_hd = this._hd) === null || _this$_hd === void 0 ? void 0 : _this$_hd.type, this.getChainInfo());
  }

  async lock() {
    const address = this.getAddress(true);
    super.lock(address);
    this.hd = undefined;
    this.mnemonicPhrase = undefined;
    this.intervalCheckHd = undefined;
    sessionStorage.removeItem('hd-wallet-' + address);
    this.address = address;
    return true;
  }

  getExternalExplorerLink(transactionId) {
    return `https://sochain.com/tx/${this.getChain()}/${transactionId}`;
  }

}

export default BTCChainer;