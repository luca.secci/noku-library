import BTCChainer from './BtcChainer';
import { ZECConstants } from '../../functions/getChainConstants';

class ZECChainer extends BTCChainer {
  constructor(chain = 'ZEC', socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance) {
    super(chain, socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance);
    this.setChainInfo(ZECConstants.chainInfo);
    this.setWalletTypes(ZECConstants.walletTypes);
    super.initWalletTypes();
  }

}

export default ZECChainer;