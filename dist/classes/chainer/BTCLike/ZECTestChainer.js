import ZECChainer from './ZECChainer';
import { ZECTestConstants } from '../../functions/getChainConstants';

class ZECTestChainer extends ZECChainer {
  constructor(chain = 'ZECTEST', socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance) {
    super(chain, socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance);
    this._isTestnet = true;
    this.setChainInfo(ZECTestConstants.chainInfo);
  }

}

export default ZECTestChainer;