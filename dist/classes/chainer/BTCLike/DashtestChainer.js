import DashChainer from './DashChainer';
import { DashTestConstants } from '../../functions/getChainConstants';

class DashTestChainer extends DashChainer {
  constructor(chain = 'DASHTEST', socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance) {
    super(chain, socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance);
    this._isTestnet = true;
    this.setChainInfo(DashTestConstants.chainInfo);
  }

}

export default DashTestChainer;