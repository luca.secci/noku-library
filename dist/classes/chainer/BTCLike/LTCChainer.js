import { LTCConstants } from '../../functions/getChainConstants';
import BtcChainer from './BtcChainer';

class LTCChainer extends BtcChainer {
  constructor(chain = 'LTC', socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance) {
    super(chain, socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance);
    this.setChainInfo(LTCConstants.chainInfo);
    this.setWalletTypes(LTCConstants.walletTypes);
    super.initWalletTypes();
  }

}

export default LTCChainer;