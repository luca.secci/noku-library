import * as btc4you from 'btc4you';
import BTCChainer from './BtcChainer';
import { BCHConstants } from '../../functions/getChainConstants';

class BCHChainer extends BTCChainer {
  constructor(chain = 'BCH', socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance) {
    super(chain, socketEndpoint, socketSwapEndpoint, getUTXOFromHD, checkHDWallet, sendRawTransaction, sendRawTransactionWithParams, getTransaction, getBalance);
    this.setChainInfo(BCHConstants.chainInfo);
    this.setWalletTypes(BCHConstants.walletTypes);
    super.initWalletTypes();
  }

  _generateTransactionFromHDWallet(hd, utxoarray, to, amount, feerate, fixedFee, hasChange) {
    for (let i = 0; i < utxoarray.length; i++) {
      let index = hd.sending.addresses.findIndex(address => address.substring(address.indexOf(':') + 1) === utxoarray[i].address);

      if (index > -1) {
        utxoarray[i].privateKey = hd.sending.privatekeys[index];
      } else {
        index = hd.receiving.addresses.findIndex(address => address.substring(address.indexOf(':') + 1) === utxoarray[i].address);

        if (index > -1) {
          utxoarray[i].privateKey = hd.receiving.privatekeys[index];
        }
      }
    }

    return btc4you.generateTransaction(utxoarray, hasChange ? hd.sending.addresses[hd.sending.freeaddress] : '', [{
      address: to,
      value: amount
    }], hd.chain, hd.type, fixedFee ? null : feerate, fixedFee);
  }

  _estimateTransactionSizeFromHDWallet(hd, utxoarray, to, amount, hasChange) {
    for (let i = 0; i < utxoarray.length; i++) {
      let index = hd.sending.addresses.findIndex(address => address.substring(address.indexOf(':') + 1) === utxoarray[i].address);

      if (index > -1) {
        utxoarray[i].privateKey = hd.sending.privatekeys[index];
      } else {
        index = hd.receiving.addresses.findIndex(address => address.substring(address.indexOf(':') + 1) === utxoarray[i].address);

        if (index > -1) {
          utxoarray[i].privateKey = hd.receiving.privatekeys[index];
        }
      }
    }

    return btc4you.estimateTransactionSize(utxoarray, hasChange ? hd.sending.addresses[hd.sending.freeaddress] : '', [{
      address: to,
      value: amount
    }], hd.chain, hd.type);
  }

  getExternalExplorerLink(transactionId) {
    return `https://blockchair.com/bitcoin-cash/transaction/${transactionId}`;
  }

}

export default BCHChainer;