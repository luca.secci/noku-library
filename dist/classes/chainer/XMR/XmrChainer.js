function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// import xmr4you from 'xmr4you';
import eth4you from 'eth4you';
import BigNumber from 'bignumber.js';
import { format } from 'date-fns';
import Chainer from '../Chainer';
import { XMRChainInfo } from '../../functions/getChainInfo';
import { getBlockie } from '../../functions/getBlockie';
import { stringToHex } from '../../functions/stringToHex';
import { hexToString } from '../../functions/hexToString'; // Avoid xmr.method() call errors

const xmr4you = {};
export const xmrChainerConstants = {
  chainInfo: XMRChainInfo,
  unlockMethods: [{
    text: 'Private Keys',
    method: 'xmr_pk'
  }, {
    text: 'Mnemonic Phrase / Seed',
    method: 'xmr_seed'
  }, {
    text: 'UTC / JSON File',
    method: 'json'
  }]
};

class XMRChainer extends Chainer {
  constructor(chain = 'XMR', socketEndpoint = '', socketSwapEndpoint = '', _getBalance, _sendRawTransaction, _getFakeOutputsByBlock, _getIsKeyImageSpent, _discardFakeTxFromAccount) {
    super(chain, socketEndpoint, socketSwapEndpoint);
    this.setChainInfo(xmrChainerConstants.chainInfo);
    this.setUnlockMethods(xmrChainerConstants.unlockMethods); // XMR has two keys, a spend key and a view key

    this._privateKey = {
      spendKey: null,
      viewKey: null
    };
    this.accountIndex = 0;
    this.completeTransactionsHistory = [];
    this.utxo = [];
    this.fakeUtxosByBlockNumber = {};
    this.keyImgesChecks = {}; // Custom methods

    this._getBalance = _getBalance;
    this._sendRawTransaction = _sendRawTransaction;
    this._getIsKeyImageSpent = _getIsKeyImageSpent;
    this._getFakeOutputsByBlock = _getFakeOutputsByBlock;
    this._discardFakeTxFromAccount = _discardFakeTxFromAccount;
  }

  getAddress() {
    if (!this.unlocked && this.address) {
      return this.address;
    }

    if (this._privateKey && this._privateKey.spendKey && this._privateKey.viewKey) {
      const publicSpendKey = xmr4you.getPublicKeyFromPrivateKey(this._privateKey.spendKey);
      this.address = xmr4you.getSubaddress(this._privateKey.viewKey, publicSpendKey, this.accountIndex, 0).address;
      return this.address;
    }

    return false;
  }

  async _getFakeTXO(utxos = []) {
    if (utxos.length === 0) {
      throw new Error('No UTXO available');
    }

    const blocks = [];

    for (let i = 0; i < utxos.length; i++) {
      const utxo = utxos[i];

      if (!this.fakeUtxosByBlockNumber[utxo.blockNumber]) {
        blocks.push(utxo.blockNumber);
      }
    }

    const fakeInputsResponse = await this._getFakeOutputsByBlock(blocks);

    if (blocks.length === fakeInputsResponse.data.length) {
      for (let i = 0; i < fakeInputsResponse.data.length; i++) {
        this.fakeUtxosByBlockNumber[blocks[i]] = fakeInputsResponse.data[i];
      }
    } else {
      throw new Error('Request fake UTXO failed');
    }
  }

  fromSatoshiToString(sat) {
    const decimals = this.getChainInfo().decimals;
    let BN_sat;

    if (!BigNumber.isBigNumber(sat)) {
      BN_sat = new BigNumber(sat);
    } else {
      BN_sat = sat;
    }

    const BN_res = BN_sat.dividedBy(new BigNumber(10).exponentiatedBy(decimals)).decimalPlaces(decimals);
    return BN_res.toString(10);
  }
  /**
  * Generate a transaction
  * feePrice = SAT/B
  * value = SAT
  * { fee [BTC], rawTx [hex], totalFeeTx [BTC]}
  * @param {string} _fromAddress
  * @param {string} _toAddress
  * @param {never} _currency Unsupported
  * @param {string} _value transaction value "1.123",
  * @param {string} _data Unsupported
  * @param {number|string} feePrice feePrice in SAT/B
  * @param {boolean} isFullBalanceTransaction if should send all transfer
  * @param {{ balance: string, [key: string]: any }} options
  * @return {Promise<{
    *   fee: any,
    *   totalFeeTx: string,
    *   rawTx: {
    *     chainId: number,
    *     [key: string]: any,
    *   },
    *   message?: string,
    * }>}
    */


  async generateTransaction(_fromAddress, _toAddress, _currency, _value, _data, feePrice, isFullTransaction, options = {}) {
    const defaultResponse = {
      fee: undefined,
      rawTx: undefined
    };

    try {
      const param_from = _fromAddress || this.getAddress();

      const param_to = _toAddress;

      if (!param_from || !param_to) {
        throw new Error('recipient not specified');
      }

      const param_privateKeys = this.getPrivateKey();

      if (!param_privateKeys.viewKey || !param_privateKeys.spendKey) {
        throw new Error('missing private keys');
      }

      const param_usingOutputs = []; // Maybe we need a function to select properly txos...

      const utxoToSpent = this.utxo;

      for (let i = 0; i < (utxoToSpent || []).length; i++) {
        const utxo = utxoToSpent[i];
        param_usingOutputs.push({
          amount: `${utxo.amount}`,
          publicKey: utxo.publicKey,
          globalIndex: utxo.globalIndex,
          index: utxo.index,
          transactionPublicKey: utxo.transactionPublicKey,
          rct: utxo.rct,
          blockNumber: utxo.blockNumber
        });
      }

      if ((param_usingOutputs || []).length < 1) {
        throw new Error('missing TXOs. Try later or get funds');
      }

      await this._getFakeTXO(utxoToSpent);
      const param_mixinOutputs = [];

      for (let i = 0; i < param_usingOutputs.length; i++) {
        const output = param_usingOutputs[i];
        const fakeOutputs = [];

        for (let j = 0; j < this.fakeUtxosByBlockNumber[output.blockNumber].length; j++) {
          fakeOutputs.push({
            globalIndex: this.fakeUtxosByBlockNumber[output.blockNumber][j].global_index,
            publicKey: this.fakeUtxosByBlockNumber[output.blockNumber][j].public_key,
            rct: this.fakeUtxosByBlockNumber[output.blockNumber][j].rct
          });
        }

        param_mixinOutputs.push({
          amount: '0',
          outputs: fakeOutputs
        });
      }

      const {
        decimals
      } = this.getChainInfo();
      const BN_value = new BigNumber(_value);
      const BN_valueSatoshi = BN_value.multipliedBy(`1e${decimals}`);
      const param_amount = BN_valueSatoshi.toString(10);
      const BN_feePrice = new BigNumber(feePrice);
      const BN_feePriceSatoshi = BN_feePrice.multipliedBy(`1e${decimals}`);
      const param_feePrice = BN_feePriceSatoshi.toString(10);
      const param_paymentId = (options || {}).paymentId;
      const param_priority = 1;
      const param_network = xmr4you.NETWORKS.MAINNET; // console.log('param_usingOutputs');
      // console.log(param_usingOutputs);
      // console.log('param_mixinOutputs');
      // console.log(param_mixinOutputs);

      const tx = await xmr4you.createTransaction(param_from, param_privateKeys, param_to, param_usingOutputs, param_mixinOutputs, param_amount, param_feePrice, param_paymentId, param_priority, undefined, undefined, param_network, undefined, isFullTransaction); // console.log('tx');
      // console.log(tx);

      if (tx && tx.success) {
        return {
          fee: this.fromSatoshiToString(tx.fee),
          totalFeeTx: this.fromSatoshiToString(tx.fee),
          rawTx: tx.rawTransaction
        };
      } else {
        return _objectSpread(_objectSpread({}, defaultResponse), {}, {
          message: tx.message
        });
      }
    } catch (ex) {
      // console.log(ex);
      return _objectSpread(_objectSpread({}, defaultResponse), {}, {
        message: ex.toString()
      });
    }
  }

  async sendRawTransaction(rawTx) {
    try {
      const responseSend = await this._sendRawTransaction(rawTx, undefined, this.getChain()); // let responseSend = await axios.post(`/xmr/raw_transaction`, {
      //   tx: rawTx
      // });

      this.utxo = [];
      this.fakeUtxosByBlockNumber = {};

      if ((responseSend || {}).data) {
        if (responseSend.data.success) {
          return '';
        } else {
          const errorInfos = (responseSend.data || {}).response;

          if (errorInfos.double_spend) {
            throw new Error('Are you trying to do a double spending transaction? If you have sent a transaction in the past 20 minutes, please wait.');
          } else if (errorInfos.fee_too_low) {
            throw new Error('Fee too low, try to increase fees!');
          } else if (errorInfos.invalid_input) {
            throw new Error('Invalid input. Try again later...');
          } else if (errorInfos.invalid_output) {
            throw new Error('Invalid output');
          } else if (errorInfos.low_mixin) {
            throw new Error('Low mixin');
          } else if (errorInfos.not_rct) {
            throw new Error('Not RCT tx');
          } else if (errorInfos.not_relayed) {
            throw new Error('Transaction not relayed. Maybe the node is offline... Try again later or contact us!');
          } else if (errorInfos.overspend) {
            throw new Error('Overspending!!');
          } else if (errorInfos.sanity_check_failed) {
            throw new Error('Sanity Check Failed');
          } else if (errorInfos.too_big) {
            throw new Error('Too big transaction!');
          } else if (errorInfos.too_few_outputs) {
            throw new Error('Too few outputs!');
          } else if (errorInfos.untrusted) {
            throw new Error('Untrusted transaction');
          } else if (errorInfos.reason) {
            throw new Error(errorInfos.reason);
          } else {
            throw new Error('Unknown error. Try again later...');
          }
        }
      }

      throw new Error('Transaction lost');
    } catch (e) {
      throw new Error(e.message || e);
    }
  }

  async lock() {
    const address = this.getAddress(true);
    super.lock(address);
    this._privateKey = {
      spendKey: null,
      viewKey: null
    };
    this.accountIndex = 0;
    return true;
  }

  async validateAddress(address) {
    return await xmr4you.validateAddress(address);
  }

  async loadFromStore(address) {
    const isValidAddress = await this.validateAddress(address);

    if (!isValidAddress) {
      throw new Error('invalid address');
    } // await super.loadFromStore(address);


    this.address = address;
    const encodedPrivateKeys = sessionStorage.getItem('private-key-' + address);

    if (encodedPrivateKeys) {
      const {
        spendKey,
        viewKey
      } = JSON.parse(encodedPrivateKeys);
      this.setPrivateKey(spendKey, viewKey);
      this.unlock();
      return {
        spendKey,
        viewKey
      };
    }

    return false;
  }

  async setPrivateKey(privateSpendKey, privateViewKey) {
    if (!privateSpendKey) {
      throw new Error('no privateSpendKey key');
    }

    if (!privateViewKey) {
      throw new Error('no privateViewKey key');
    }

    const privateKey = {
      spendKey: privateSpendKey,
      viewKey: privateViewKey
    };
    this._privateKey = privateKey;
    const root_address = xmr4you.getSubaddress(privateViewKey, xmr4you.getPublicKeyFromPrivateKey(privateSpendKey), this.accountIndex).address;
    sessionStorage.setItem('private-key-' + root_address, JSON.stringify(this._privateKey));
    const mnemonicPhrase = this.getMnemonicPhrase();

    if (mnemonicPhrase) {
      sessionStorage.setItem('mnemonic-phrase-' + root_address, mnemonicPhrase);
    }

    this.unlock();
    return this._privateKey;
  }

  getBlockie() {
    const chain = this.getChain();
    const address = this.getAddress();
    return getBlockie(chain, address);
  }

  async getAccountFromMnemonicPhrase(mnemonicPhrase = '', accountIndex = 0, options = {}) {
    const wallet = await xmr4you.getWalletFromMnemonic(mnemonicPhrase);
    await this.setPrivateKey(wallet.private.spendKey, wallet.private.viewKey);
    this.setMnemonicPhrase(mnemonicPhrase);
    return wallet;
  }

  generateJson(password, _additionalInfo = {}) {
    // additionalInfo è per usi futuri
    const privateKeys = this.getPrivateKey();

    if (!privateKeys.spendKey || !privateKeys.viewKey) {
      throw new Error('no private key');
    }

    const privateKeyHex = stringToHex(`${privateKeys.spendKey} ${privateKeys.viewKey}`);
    const additionalInfo = {
      accountIndex: this.accountIndex
    };
    const string = eth4you.toV3(privateKeyHex, password, this.getName(), undefined, this.getChain(), additionalInfo, this.getMnemonicPhrase());
    return JSON.parse(string);
  }

  async unlockJsonWallet(text = '{}', password = undefined) {
    try {
      const json = JSON.parse(text);
      const fileContent = eth4you.fromFile_remastered(text, password);
      const json_privateKeys = fileContent.key;
      this.setMnemonicPhrase(fileContent.mnemonicPhrase);

      if (!json_privateKeys) {
        throw new Error('wrong password');
      }

      if (json && (json.chain || '').toUpperCase() !== this.chain) {
        throw new Error('JSON mismatch with current private key');
      }

      const privateKeys = hexToString(json_privateKeys).split(' ');
      const privateSpendKey = privateKeys[0];
      const privateViewKey = privateKeys[1];
      this.accountIndex = json.accountIndex;
      await this.setPrivateKey(privateSpendKey, privateViewKey);

      if (json.wallet_name) {
        this.setName(json.wallet_name);
      }

      return true;
    } catch (ex) {
      throw new Error(ex.message);
    }
  }

  async generateRandomMnemonicPhrase() {
    const newRandomWallet = await xmr4you.createNewWallet();
    this.setMnemonicPhrase(newRandomWallet.mnemonic);
    sessionStorage.setItem('xmr-new-wallet-' + newRandomWallet.address, true);
    return newRandomWallet.mnemonic;
  }

  async checkKeyImages(keyImages = []) {
    const result = {};
    const keyImagesToBeChecked = [];

    for (let i = 0; i < keyImages.length; i++) {
      const keyImage = keyImages[i];

      if (this.keyImgesChecks[keyImage]) {
        result[keyImage] = this.keyImgesChecks[keyImage];
      } else {
        keyImagesToBeChecked.push(keyImage);
      }
    }

    if (keyImagesToBeChecked.length > 0) {
      var _keyImagesCheckRespon;

      const keyImagesCheckResponse = await this._getIsKeyImageSpent(keyImagesToBeChecked); // await axios.post("/xmr/is_key_image_spent", {
      //   key_images: keyImagesToBeChecked
      // });

      const keyImagesChecks = (_keyImagesCheckRespon = keyImagesCheckResponse === null || keyImagesCheckResponse === void 0 ? void 0 : keyImagesCheckResponse.data) !== null && _keyImagesCheckRespon !== void 0 ? _keyImagesCheckRespon : {};

      for (var key in keyImagesChecks) {
        this.keyImgesChecks[key] = keyImagesChecks[key];
        result[key] = keyImagesChecks[key];
      }
    }

    return result;
  }

  async checkTransactionsForBalance(transactions, multi) {
    if (this._privateKey.viewKey && this._privateKey.spendKey) {
      const outputByKeyImage = {};
      const keyImagesCalculated = [];

      for (let i = 0; i < transactions.length; i++) {
        const outputs = transactions[i].outputs || [];

        for (let j = 0; j < outputs.length; j++) {
          if (outputs[j].match) {
            const transactionPublicKey = xmr4you.getTransactionPublicKeyFromExtra(transactions[i].extra);
            const keyImage = await xmr4you.getKeyImage(this._privateKey.viewKey, transactionPublicKey, this._privateKey.spendKey, xmr4you.getPublicKeyFromPrivateKey(this._privateKey.spendKey), j);
            keyImagesCalculated.push(keyImage);
            outputByKeyImage[keyImage] = _objectSpread(_objectSpread({}, outputs[j]), {}, {
              transactionPublicKey: transactionPublicKey,
              blockNumber: transactions[i].blockNumber
            });
          }
        }
      }

      const keyImagesChecks = await this.checkKeyImages(keyImagesCalculated);
      let BN_balance_satoshi = new BigNumber(0);
      this.utxo = [];

      for (var keyImage in outputByKeyImage) {
        if (!keyImagesChecks[keyImage]) {
          this.utxo.push(outputByKeyImage[keyImage]);
          BN_balance_satoshi = BN_balance_satoshi.plus(outputByKeyImage[keyImage].amount);
        }
      }

      const decimals = this.getChainInfo().decimals;
      const BN_balance = BN_balance_satoshi.dividedBy(new BigNumber(10).exponentiatedBy(decimals)).decimalPlaces(decimals);
      const totalBalanceEquivalent = {
        eth: new BigNumber(multi.XMR.ETH).multipliedBy(BN_balance).toFixed(18),
        usd: new BigNumber(multi.XMR.USD).multipliedBy(BN_balance).toFixed(2),
        eur: new BigNumber(multi.XMR.EUR).multipliedBy(BN_balance).toFixed(2),
        chf: new BigNumber(multi.XMR.CHF).multipliedBy(BN_balance).toFixed(2),
        cad: new BigNumber(multi.XMR.CAD).multipliedBy(BN_balance).toFixed(2),
        jpy: new BigNumber(multi.XMR.JPY).multipliedBy(BN_balance).toFixed(3),
        cny: new BigNumber(multi.XMR.CNY).multipliedBy(BN_balance).toFixed(2),
        rub: new BigNumber(multi.XMR.RUB).multipliedBy(BN_balance).toFixed(2),
        aud: new BigNumber(multi.XMR.AUD).multipliedBy(BN_balance).toFixed(2),
        btc: new BigNumber(multi.XMR.BTC).multipliedBy(BN_balance).toFixed(8)
      };
      const balance = {
        lastUpdate: new Date().getTime(),
        // last unix timestamp of balance update
        balance: BN_balance.toString(10),
        // Monero balance
        balanceMulti: multi.XMR,
        // equivalent of 1 bitcoin in different chains
        rawBalance: BN_balance_satoshi.toString(10),
        // monero balance in satoshi
        price: multi.XMR,
        // ethereum price
        totalBalance: BN_balance.toString(10),
        // equivalent of total balance in monero
        totalBalanceEquivalent: totalBalanceEquivalent // equivalent of total balance in different chains

      };
      return balance;
    } else {
      throw new Error('Without private keys, balance is unavailable');
    }
  }

  async sendDiscardedTransactionsHashes(transactionsHashes, subAddressIndex = 0) {
    try {
      if ((transactionsHashes || []).length <= 0) {
        return true;
      }

      if (!this._privateKey.spendKey || !this._privateKey.viewKey) {
        throw new Error('Unlock this wallet');
      }

      const discardReponse = await this._discardFakeTxFromAccount(this._privateKey.viewKey, xmr4you.getPublicKeyFromPrivateKey(this._privateKey.spendKey), this.accountIndex, subAddressIndex, transactionsHashes);
      return ((discardReponse || {}).data || {}).success;
    } catch (ex) {
      // console.log(ex);
      throw new Error(ex.message);
    }
  }

  async checkTransactionsForTransactionsHistory(transactions = [], sendDiscardedTransactions = false) {
    if (this._privateKey.viewKey && this._privateKey.spendKey) {
      const transactionsResult = []; // standard

      const outputByKeyImage = {};
      const keyImagesCalculated = []; // console.log(transactions);

      for (let i = 0; i < transactions.length; i++) {
        if (transactions[i].hasAtLeastOneMatchedOutput) {
          const outputs = transactions[i].outputs || [];

          for (let j = 0; j < outputs.length; j++) {
            if (outputs[j].match) {
              const transactionPublicKey = xmr4you.getTransactionPublicKeyFromExtra(transactions[i].extra);
              const keyImage = await xmr4you.getKeyImage(this._privateKey.viewKey, transactionPublicKey, this._privateKey.spendKey, xmr4you.getPublicKeyFromPrivateKey(this._privateKey.spendKey), j);
              keyImagesCalculated.push(keyImage);
              outputByKeyImage[keyImage] = outputs[j];
            }
          }
        }
      }

      const keyImagesChecks = await this.checkKeyImages(keyImagesCalculated); // fine standard

      const discardedTransactionsHashes = [];

      for (let i = 0; i < transactions.length; i++) {
        const transaction = transactions[i];
        transaction.isSpentByThisAddress = false;
        let amountSent = 0;
        let amountReceived = 0;

        for (let j = 0; j < (transaction.inputs || []).length; j++) {
          const input = transaction.inputs[j];

          if (keyImagesChecks[input.keyImage]) {
            transaction.isSpentByThisAddress = true;
            amountSent += outputByKeyImage[input.keyImage].amount;
            input.amount = outputByKeyImage[input.keyImage].amount;
          }
        }

        if (transaction.hasAtLeastOneMatchedOutput) {
          for (let j = 0; j < (transaction.outputs || []).length; j++) {
            const output = transaction.outputs[j];
            transaction.isSelfTransaction = true;

            if (output.match) {
              amountReceived += output.amount;
            } else {
              // if an output doesnt match, it's not a self transaction
              transaction.isSelfTransaction = false;
            }
          }
        }

        let value = 0;

        if (transaction.isSpentByThisAddress) {
          value = -1 * (amountSent - amountReceived - transaction.fee);
        } else {
          value = amountReceived;
        }

        if (transaction.isSpentByThisAddress || transaction.hasAtLeastOneMatchedOutput) {
          transactionsResult.push(_objectSpread(_objectSpread({}, transaction), {}, {
            isSpentByThisAddress: transaction.isSpentByThisAddress,
            rawValue: value,
            value: this.fromSatoshiToString(value),
            fee: this.fromSatoshiToString(transaction.fee),
            action: 'transfer',
            ts: transaction.ts * 1000,
            date: format(transaction.ts * 1000, 'MMMM YYYY, dd - HH:mm:ss'),
            txHash: transaction.txHash,
            confirmations: transaction.confirmations,
            status: transaction.status || ''
          }));
        } else {
          discardedTransactionsHashes.push(transaction.txHash);
        }
      }

      if (sendDiscardedTransactions) {
        try {
          await this.sendDiscardedTransactionsHashes(discardedTransactionsHashes);
        } catch (ex) {
          console.error(ex); // console.log(ex);
        }
      }

      return transactionsResult;
    } else {
      throw new Error('Without private keys, transactions are unavailable');
    }
  } // WEBSOCKET FUNCTIONS & CALLBACKS


  wsocketInit(intervals, intervalBalance = true, intervalTransactions = true, _query = {}, intervalInfoSendFunds = true) {
    const privateKey = this.getPrivateKey();

    if (privateKey !== null && privateKey !== void 0 && privateKey.spendKey && privateKey !== null && privateKey !== void 0 && privateKey.viewKey) {
      const query = _objectSpread({
        account_index: this.accountIndex,
        // provvisorio
        subaddress_index: 0
      }, _query);

      super.wsocketInit(intervals, intervalBalance, intervalTransactions, query, intervalInfoSendFunds);
      const socketClient = this.getSocketclient();
      socketClient.on('connect', () => setTimeout(() => socketClient.emit('xmrSetupKey', {
        private_view_key: this._privateKey.viewKey,
        public_spend_key: xmr4you.getPublicKeyFromPrivateKey(this._privateKey.spendKey),
        is_new_wallet: sessionStorage.getItem('xmr-new-wallet-' + this.getAddress(true))
      }), 10));
    }
  }

  setVariablesSubscription(type, result) {
    const fees = result.fees;
    const BN_fees = new BigNumber(fees);
    const BN_highFees = BN_fees.multipliedBy(4);

    switch (type) {
      case 'gasPriceConfiguration':
        result = {
          gasPriceList: [{
            name: 'Normal',
            value: this.fromSatoshiToString(BN_fees)
          }, {
            name: 'High',
            value: this.fromSatoshiToString(BN_highFees)
          }]
        };
        break;

      case 'balance':
        result = this.checkTransactionsForBalance(result.transactions, result.multi);
        break;

      case 'transactions':
        result = this.prepareTransactionsList(result.transactions, result.pendingTransactions);
        break;

      default:
        break;
    }

    return result;
  }

  async getBalance(subAddressIndex = 0) {
    try {
      if (!this._privateKey.spendKey || !this._privateKey.viewKey) {
        throw new Error('Unlock this wallet');
      }

      const transactionsListReponse = await this._getBalance(this._privateKey.viewKey, xmr4you.getPublicKeyFromPrivateKey(this._privateKey.spendKey), this.accountIndex, subAddressIndex);
      const transactions = transactionsListReponse.data.transactions || [];
      const multi = transactionsListReponse.data.multi;
      const balance = await this.checkTransactionsForBalance(transactions, multi);
      return balance;
    } catch (ex) {
      // console.log(ex);
      throw new Error(ex.message);
    }
  }

  emitGetTransactions(query) {
    const currentPage = parseInt((query || {}).currentpage || 1);
    const lastCurrentPage = parseInt((this._lastQuery || {}).currentpage || 1);
    const tsl = (query || {}).tsl;
    const tsh = (query || {}).tsh;
    const lastTsl = (this._lastQuery || {}).tsl;
    const lastTsh = (this._lastQuery || {}).tsh;

    if (currentPage !== lastCurrentPage || tsl !== lastTsl || tsh !== lastTsh) {
      this._lastQuery = _objectSpread(_objectSpread({}, this._lastQuery), query);

      for (var functionId in this.subscriptions.transactions) {
        this.subscriptions.transactions[`${functionId}`](this.prepareTransactionsListResponse(this.completeTransactionsHistory.transactions, this.completeTransactionsHistory.pendingTransactions));
      }

      return;
    }

    super.emitGetTransactions(query);
  }

  async prepareTransactionsListResponse(transactionsHistory, pendingTransactionsHistory) {
    let totalTransactionsList = (pendingTransactionsHistory || []).concat(transactionsHistory || []);

    if (this._lastQuery.tsl && this._lastQuery.tsh) {
      const tslTimestamp = new Date(this._lastQuery.tsl).getTime();
      const tshTimestamp = new Date(this._lastQuery.tsh).getTime();
      totalTransactionsList = totalTransactionsList.filter(elem => {
        return elem.ts > tslTimestamp && elem.ts < tshTimestamp;
      });
    }

    const currentPage = parseInt((this._lastQuery || {}).currentpage || 1);
    const totalTransactionsListFiltered = totalTransactionsList.slice((currentPage - 1) * 12, currentPage * 12);
    this.completeTransactionsHistory = {
      transactions: transactionsHistory,
      pendingTransactions: pendingTransactionsHistory
    };
    const transactionsByDay = {};
    totalTransactionsListFiltered.forEach(tx => {
      // ts is in seconds
      const date = new Date(tx.ts);
      const key = format(date, 'yyyy-MM-dd');

      if (transactionsByDay[key]) {
        const currentTransactions = transactionsByDay[key];
        transactionsByDay[key] = [...currentTransactions, tx];
      } else {
        transactionsByDay[key] = [tx];
      }
    });
    const result = {
      transactions: transactionsByDay,
      totalItems: totalTransactionsList.length
    }; // console.log({...result});

    return result;
  }

  async prepareTransactionsList(transactions, pendingTransactions) {
    const transactionsHistory = await this.checkTransactionsForTransactionsHistory(transactions, true);
    const pendingTransactionsHistory = await this.checkTransactionsForTransactionsHistory(pendingTransactions, false);
    return await this.prepareTransactionsListResponse(transactionsHistory, pendingTransactionsHistory);
  }

  emitXmrTask() {
    if (this.socketClient && this._privateKey.spendKey && this._privateKey.viewKey) {
      const query = {
        chain: this.getChain(),
        private_view_key: this._privateKey.viewKey,
        public_spend_key: xmr4you.getPublicKeyFromPrivateKey(this._privateKey.spendKey),
        account_index: this.accountIndex,
        subaddress_index: 0,
        // provvisorio
        is_new_wallet: sessionStorage.getItem('xmr-new-wallet-' + this.getAddress(true))
      };
      this.socketClient.emit('xmrTask', query);
    }
  }

  async getTooltipInfo(balance, feeRate, ...params) {
    const tooltipObject = {};
    return tooltipObject;
  }

  getExternalExplorerLink(transactionId) {
    return `https://moneroblocks.info/tx/${transactionId}`;
  }

}

export default XMRChainer;