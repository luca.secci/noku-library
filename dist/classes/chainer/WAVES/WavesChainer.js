function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import waves4you from 'waves4you';
import eth4you from 'eth4you';
import BigNumber from 'bignumber.js';
import { format } from 'date-fns';
import Chainer from '../Chainer';
import { generateRandomMnemonicPhrase } from '../../functions/waves/generateRandomMnemonicPhrase';
import { getAccountFromMnemonicPhrase } from '../../functions/waves/getAccountFromMnemonicPhrase';
import { hexToString } from '../../functions/hexToString';
import { stringToHex } from '../../functions/stringToHex';
import { WavesConstants } from '../../functions/getChainConstants';

class WavesChainer extends Chainer {
  constructor(chain = 'WAVES', socketEndpoint, socketSwapEndpoint, _getBalance, _estimateFees, _sendRawTransaction, _sendRawTransactionWithParams, _getAddressByAlias, _getTransaction) {
    super(chain, socketEndpoint, socketSwapEndpoint);
    this.setChainInfo(WavesConstants.chainInfo);
    this.setUnlockMethods(WavesConstants.unlockMethods);
    this.addressesByAlias = new Map(); // Set custom methods

    this._getBalance = _getBalance;
    this._estimateFees = _estimateFees;
    this._sendRawTransaction = _sendRawTransaction;
    this._sendRawTransactionWithParams = _sendRawTransactionWithParams;
    this._getAddressByAlias = _getAddressByAlias;
    this._getTransaction = _getTransaction;
  }

  getNetwork() {
    return this.getChain() === 'WAVES' ? 'mainnet' : 'testnet';
  }

  async getAddressByAlias(alias = '') {
    if (alias.length >= 4 && alias.length <= 30) {
      try {
        const cachedAlias = this.addressesByAlias.get(alias);

        if (cachedAlias) {
          return cachedAlias;
        }

        const chain = this.getChain();
        const network = this.getNetwork();
        const address = await this._getAddressByAlias(chain, alias);

        if (waves4you.isValidAddress(address, network)) {
          this.addressesByAlias.set(alias, address);
          return address;
        }
      } catch (ex) {}
    }

    return null;
  }

  async validateAddress(addressOrAlias) {
    const network = this.getChain() === 'WAVES' ? 'mainnet' : 'testnet';

    if (waves4you.isValidAddress(addressOrAlias, network)) {
      return true;
    } else {
      try {
        const addressByAlias = await this.getAddressByAlias(addressOrAlias);

        if (addressByAlias) {
          return true;
        }
      } catch (ex) {}
    }

    return false;
  }

  getAddress() {
    let address = this.address;

    if (address) {
      const cachedAlias = this.addressesByAlias.get(address); // not so clear, but sometimes the address can the alias

      if (cachedAlias) {
        return cachedAlias;
      }

      return address;
    } // DO NOT REMOVE THIS!!


    if (!this.unlocked) {
      return undefined;
    }

    const privateKey = this.getPrivateKey();

    if (!privateKey) {
      throw new Error('no private key');
    }

    const network = this.getNetwork();
    address = waves4you.getAddressFromPrivateKey(privateKey, network);
    this.address = address;
    return address;
  }

  async generateRandomMnemonicPhrase() {
    const network = this.getNetwork();
    return await generateRandomMnemonicPhrase(network);
  }

  async setPrivateKey(privateKey) {
    const network = this.getNetwork();
    const address = waves4you.getAddressFromPrivateKey(privateKey, network);
    this.address = address;
    super.setPrivateKey(privateKey);
    this.unlock();
    return privateKey;
  }

  async getAccountFromMnemonicPhrase(mnemonicPhrase = '', index = 0, options = {}, saveMnemonicPhrase) {
    const network = this.getNetwork();
    const account = await getAccountFromMnemonicPhrase(network, mnemonicPhrase);
    this.setMnemonicPhrase(account.phrase, saveMnemonicPhrase);
    this.setPrivateKey(account.privateKey);
    return this;
  }

  async unlockJsonWallet(text = '{}', password = undefined) {
    try {
      const json = JSON.parse(text);
      const fileContent = eth4you.fromFile_remastered(text, password);
      const privateKeyHex = fileContent.key;
      const privateKey = hexToString(privateKeyHex);

      if (privateKey) {
        await this.setPrivateKey(privateKey);
        this.setMnemonicPhrase(fileContent.mnemonicPhrase);

        if (json.wallet_name) {
          this.setName(json.wallet_name);
        }
      } else {
        throw new Error('wrong password');
      }

      return true;
    } catch (ex) {
      throw new Error(ex.message);
    }
  }

  async loadFromStore(address) {
    const isValidAddress = await this.validateAddress(address);

    if (!isValidAddress) {
      throw new Error('invalid address');
    } // await super.loadFromStore(address);


    this.address = address;
    const privateKey = sessionStorage.getItem('private-key-' + address);

    if (privateKey) {
      this.setPrivateKey(privateKey);
      return privateKey;
    }

    return false;
  } // additionalInfo è per usi futuri


  generateJson(password, _additionalInfo = {}) {
    const privateKey = this.getPrivateKey();
    const privateKeyHex = stringToHex(privateKey);

    if (!privateKey) {
      throw new Error('no private key');
    }

    const string = eth4you.toV3(privateKeyHex, password, this.getName(), undefined, this.getChain(), undefined, this.getMnemonicPhrase());
    return JSON.parse(string);
  }

  async lock() {
    const address = this.getAddress();
    super.lock(address);
    return true;
  }

  async getBalance(...additionalParams) {
    const chain = this.getChain();
    const address = this.getAddress();
    let balance = await this._getBalance(chain, address, ...additionalParams);
    this.setBalance(balance);
    return balance;
  }
  /*
  RESEARCH ONLY SECTION
   async createTokenTransaction(_name, _description, _quantity, _reiussable, _decimals, options = {}) {
     try {
      if (!_name) {
        throw new Error('Missing name');
      }
      const network = (this.getChain() === "WAVES" ? 'mainnet' : 'testnet');
      const name = _name.toString();
      const description = (_description || '').toString();
      const decimals = parseInt(_decimals);
      const reiussable = !!_reiussable;
      const BN_quantity = (new BigNumber(_quantity.toString())).multipliedBy((new BigNumber(10)).exponentiatedBy(decimals)).decimalPlaces(0);
      const quantity = BN_quantity.toString(10);
       let params = {
        name: name,
        description: description,
        quantity: parseInt(quantity),
        sender: this.getAddress(),
        senderPublicKey: waves4you.getPublicKeyFromPrivateKey(this.getPrivateKey()),
        reiussable: reiussable,
        decimals: parseInt(decimals)
      }
       const responseFees = await AxiosCaller.post("/waves/estimate_fees", {
        type: 3,
        chain: this.getChain(),
        params: {
          ...params,
        }
      });
       console.log(network);
      console.log(responseFees);
       return;
     } catch (ex) {
      console.log(ex);
    }
   }
  */
  /// return { fee, rawTx }

  /**
  * Generate a transaction
  * @param {string} _fromAddress
  * @param {string} _toAddress
  * @param {Token} currency if defined the asset to transfer
  * @param {string} _value transaction value "1.123",
  * @param {string} _data [ignored] data field in transaction
  * @param {number|string} _fee [ignored] fee
  * @param {boolean} isFullBalanceTransaction if should send all transfer
  * @param {{ balance: string, [key: string]: any }} options
  * @return {Promise<{
    *   fee: any,
    *   totalFeeTx: string,
    *   feesAsset: string,
    *   rawTx: {
    *     chainId: number,
    *     [key: string]: any,
    *   },
    *   message?: string,
    * }>}
    */


  async generateTransaction(_fromAddress, _toAddress, currency, _value, _data, _fee, isFullBalanceTransaction, options = {}) {
    const network = this.getNetwork();
    const defaultResponse = {
      fee: undefined,
      rawTx: undefined,
      totalFeeTx: undefined,
      message: ''
    };
    const chainInfo = this.getChainInfo();
    const chain = this.getChain();

    try {
      let toAddress;

      if (waves4you.isValidAddress(_toAddress, network)) {
        toAddress = _toAddress;
      } else {
        const addressByAlias = await this.getAddressByAlias(_toAddress);

        if (addressByAlias) {
          toAddress = addressByAlias;
        } else {
          throw new Error('Recipient address not valid');
        }
      }

      let fee;
      let feeFloating = '0';
      let rawTx = {};
      let newValue;
      let feesAsset;
      const assetId = currency === null || currency === void 0 ? void 0 : currency.id;
      const isAssetTransfer = !!assetId;
      const decimals = isAssetTransfer ? currency.decimals : chainInfo.decimals;
      const valueBN = new BigNumber(_value.toString()).multipliedBy(new BigNumber(10).exponentiatedBy(decimals)).decimalPlaces(0);
      const wavesBalanceBN = new BigNumber(options.balance.toString()).multipliedBy(new BigNumber(10).exponentiatedBy(chainInfo.decimals)).decimalPlaces(0);

      if (valueBN.isLessThanOrEqualTo(0)) {
        return defaultResponse;
      }

      const params = {
        sender: this.getAddress(),
        senderPublicKey: waves4you.getPublicKeyFromPrivateKey(this.getPrivateKey()),
        recipient: toAddress.toString(),
        amount: valueBN.toString(10)
      }; // is a token/asset transfer

      if (isAssetTransfer) {
        const assetBalanceBN = new BigNumber(currency.balance.toString()).multipliedBy(new BigNumber(10).exponentiatedBy(chainInfo.decimals)).decimalPlaces(0);
        params.assetId = assetId;
        const estimateFeesBody = {
          type: 4,
          chain,
          params
        };
        const responsesFeeData = await this._estimateFees(estimateFeesBody);
        const feesAssetId = responsesFeeData.feeAssetId;
        feesAsset = responsesFeeData.assetInfo;
        const feesDecimals = feesAssetId ? feesAsset.decimals : chainInfo.decimals;
        const feesBN = new BigNumber((responsesFeeData.feeAmount || 0).toString());

        if (valueBN.isGreaterThan(assetBalanceBN)) {
          throw new Error('Insufficient balance');
        }

        fee = feesBN.toString(10);
        params.fee = fee;
        feeFloating = new BigNumber(fee).dividedBy(new BigNumber(10).exponentiatedBy(feesDecimals)).decimalPlaces(feesDecimals); // sto trasferendo un asset e pago le fee con quell'asset

        if (feesAsset) {
          if (feesBN.isGreaterThan(assetBalanceBN)) {
            throw new Error('Insufficient balance');
          }

          if (isFullBalanceTransaction) {
            const netValueBN = assetBalanceBN.minus(feesBN);

            if (netValueBN.isLessThan(0)) {
              throw new Error('Insufficient balance');
            }

            newValue = netValueBN.dividedBy(new BigNumber(10).exponentiatedBy(feesDecimals)).decimalPlaces(feesDecimals).toString(10);
            params.amount = netValueBN.toString(10);
            rawTx = waves4you.createTransaction(4, params, this.getPrivateKey(), network);
          } else {
            if (feesBN.isGreaterThan(assetBalanceBN.minus(valueBN))) {
              throw new Error('Insufficient balance');
            }

            rawTx = waves4you.createTransaction(4, params, this.getPrivateKey(), network);
          }
        } else {
          // sto trasferendo un asset e pago le fee in waves
          if (feesBN.isGreaterThan(wavesBalanceBN)) {
            throw new Error('Insufficient balance');
          }

          if (isFullBalanceTransaction) {
            // trasferisco tutto ciò che ho di un asset ma pago le fee in waves
            const netValueBN = wavesBalanceBN; // usa questo per sicurezza

            params.amount = netValueBN.toString(10); // useless?

            rawTx = waves4you.createTransaction(4, params, this.getPrivateKey(), network);
          } else {
            rawTx = waves4you.createTransaction(4, params, this.getPrivateKey(), network);
          }
        }
      } else {
        const estimateFeesBody = {
          type: 4,
          chain: this.getChain(),
          params
        };
        const responsesFeeData = await this._estimateFees(estimateFeesBody);
        const feesBN = new BigNumber((responsesFeeData.feeAmount || 0).toString());

        if (feesBN.isGreaterThan(wavesBalanceBN)) {
          throw new Error('Insufficient balance');
        }

        fee = feesBN.toString(10);
        params.fee = fee;
        feeFloating = new BigNumber(fee).dividedBy(new BigNumber(10).exponentiatedBy(chainInfo.decimals)).decimalPlaces(chainInfo.decimals);

        if (isFullBalanceTransaction) {
          const netValueBN = wavesBalanceBN.minus(feesBN);

          if (netValueBN.isLessThan(0)) {
            throw new Error('Insufficient balance');
          }

          newValue = netValueBN.dividedBy(new BigNumber(10).exponentiatedBy(chainInfo.decimals)).decimalPlaces(chainInfo.decimals).toString(10);
          params.amount = netValueBN.toString(10);
          rawTx = waves4you.createTransaction(4, params, this.getPrivateKey(), network);
        } else {
          if (feesBN.isGreaterThan(wavesBalanceBN.minus(valueBN))) {
            throw new Error('Insufficient balance');
          }

          rawTx = waves4you.createTransaction(4, params, this.getPrivateKey(), network);
        }
      }

      return {
        fee: feeFloating,
        rawTx: rawTx,
        totalFeeTx: feeFloating,
        feesAsset: feesAsset,
        newValue: newValue
      };
    } catch (e) {
      return _objectSpread(_objectSpread({}, defaultResponse), {}, {
        message: e
      });
    }
  }
  /**
   * @param {*} rawTx
   */


  async sendRawTransaction(rawTx) {
    const data = {
      type: rawTx.type,
      params: rawTx
    };
    return await this._sendRawTransaction(rawTx, data, this.getChain());
  }

  async sendSwapTransaction(rawTx, params) {
    var _params$toAddress;

    const fromAddress = this.getAddress();
    const toAddress = (_params$toAddress = params === null || params === void 0 ? void 0 : params.toAddress) !== null && _params$toAddress !== void 0 ? _params$toAddress : this.getAddress();
    const type = rawTx.type;
    const body = {
      fromAddress,
      toAddress,
      type,
      params: rawTx
    };
    const response = await this._sendRawTransactionWithParams('', body, this.getChain());
    return response;
  }

  async getTransaction(id) {
    if (!id) {
      throw new Error('missing parameter id');
    }

    try {
      const chain = this.getChain();
      return await this._getTransaction(chain, id);
    } catch (error) {
      return null;
    }
  } // WEBSOCKET FUNCTIONS & CALLBACKS


  setVariablesSubscription(type, result) {
    switch (type) {
      case 'gasPriceConfiguration':
        result = {
          gasPriceList: [{
            name: 'Normal',
            value: 1
          }]
        };
        break;

      case 'balance':
        result = this.balance = {
          lastUpdate: new Date().getTime(),
          // last unix timestamp of balance update
          balance: result.wavesBalance,
          // waves balance
          balanceMulti: result.wavesPrice,
          // equivalent of 1 wave in different chains
          rawBalance: result.wavesRawBalance,
          // waves balance in wavelet
          maxDelay: result.maxDelay,
          tokens: result.tokens,
          // tokens list
          type: result.type,
          // type of address
          price: result.wavesPrice,
          // waves price
          totalBalance: result.wavesBalance,
          // equivalent of total balance in waves
          totalBalanceEquivalent: result.total // equivalent of total balance in different chains

        };
        break;

      case 'transactions':
        result = this.prepareTransactionsListResponse(result.transactions);
        break;

      default:
        break;
    }

    return result;
  }

  prepareTransactionsListResponse(transactions) {
    let totalTransactionsList = [...transactions];

    if (this._lastQuery.tsl && this._lastQuery.tsh) {
      const tslTimestamp = new Date(this._lastQuery.tsl).getTime();
      const tshTimestamp = new Date(this._lastQuery.tsh).getTime();
      totalTransactionsList = totalTransactionsList.filter(elem => {
        return elem.ts > tslTimestamp && elem.ts < tshTimestamp;
      });
    }

    const currentPage = parseInt((this._lastQuery || {}).currentpage || 1);
    const totalTransactionsListFiltered = totalTransactionsList.slice((currentPage - 1) * 6, currentPage * 6);
    this.completeTransactionsHistory = {
      transactions: transactions
    };
    const transactionsByDay = {};
    totalTransactionsListFiltered.forEach(tx => {
      // ts is in seconds
      const date = new Date(tx.ts * 1000);
      const key = format(date, 'yyyy-MM-dd');

      if (transactionsByDay[key]) {
        const currentTransactions = transactionsByDay[key];
        transactionsByDay[key] = [...currentTransactions, _objectSpread({}, tx)];
      } else {
        transactionsByDay[key] = [_objectSpread({}, tx)];
      }
    });
    const result = {
      transactions: transactionsByDay,
      totalItems: totalTransactionsList.length
    };
    return result;
  }

  emitGetTransactions(query) {
    const currentPage = parseInt((query || {}).currentpage || 1);
    const lastCurrentPage = parseInt((this._lastQuery || {}).currentpage || 1);
    const tsl = (query || {}).tsl;
    const tsh = (query || {}).tsh;
    const lastTsl = (this._lastQuery || {}).tsl;
    const lastTsh = (this._lastQuery || {}).tsh;

    if (currentPage !== lastCurrentPage || tsl !== lastTsl || tsh !== lastTsh) {
      this._lastQuery = _objectSpread(_objectSpread({}, this._lastQuery), query);

      for (var functionId in this.subscriptions.transactions) {
        // console.log(this.completeTransactionsHistory);
        this.subscriptions.transactions[`${functionId}`](this.prepareTransactionsListResponse(this.completeTransactionsHistory.transactions));
      }

      return;
    }

    super.emitGetTransactions(query);
  }

  getTooltipInfo(balance, feeRate, ...params) {
    const tooltipObject = {};
    return tooltipObject;
  }

  getExternalExplorerLink(txId) {
    return ` https://wavesexplorer.com/tx/${txId}`;
  }

}

export default WavesChainer;