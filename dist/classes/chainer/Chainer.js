function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import eth4you from 'eth4you';
import { io } from 'socket.io-client';
import { equalsInsensitive } from '../../utils/equalsInsensitive';
import { textElision } from '../../utils/textElision';
import isEmptyObject from '../../utils/isEmptyObject';
import { validateProvider } from '../../utils/web3/providers';
import { removeWalletFromExternalList } from '../../utils/web3/externalListStorage';
import { getBlockie } from '../functions/getBlockie';
import { getThemeObject } from '../functions/getThemeObject';

function generateRandomId() {
  return eth4you.randomId();
}

const baseChainInfo = {
  symbol: '$',
  ticker: 'USD',
  logo: undefined,
  hasFungibleTokens: false,
  hasNonFungibleTokens: false,
  hasGas: false,
  // transaction_fees = gas_limit * gas_price
  hasDataInTransaction: false,
  hasFees: true,
  decimals: 0,
  feeUnitLabel: '$',
  createWithBlockies: false,
  // chainId: null,
  customChain: null,
  isEthereumLike: false,
  isBitcoinLike: false,
  isWavesLike: false
};

class Chainer {
  /**
   * Base chainer class
   * @param {string} chain The chainer chain
   * @param {string} socketEndpoint Websocket endpoint to listen to balance/transactions/etc.
   * @param {string} socketSwapEndpoint Websocket endpoint to listen to swap
   * @param {} _parseBalance If defined parses the balance when calling getBalance
   */
  constructor(chain = 'USD', socketEndpoint, socketSwapEndpoint, _parseBalance = b => b) {
    if (!chain) {
      throw new Error('no selected chain');
    }

    this.isChainer = true;
    this._name = '';
    this._id = generateRandomId();
    this._address = "";
    this._chain = chain;
    this._wallet = undefined;
    this._balance = undefined;
    this._transactions = undefined;
    this._privateKey = undefined;
    this._mnemonicPhrase = undefined;
    this._unlockMethods = undefined;
    this._isTestnet = false;
    this._unlocked = false;
    this._gasPriceList = null;
    this._baseFee = null; // Common function

    this._parseBalance = _parseBalance; // BTCLIKE

    this._walletTypes = [];
    this._isHDWallet = false;
    this._hd = null; // WEB SOCKET

    this._socketClient = undefined;
    this._subscriptions = [];
    this._socketEndpoint = socketEndpoint; // WEB SOCKET SWAP

    this._socketClientSwap = undefined;
    this._socketSwapEndpoint = socketSwapEndpoint; // this._initializingSocketClientSwap = false;
    // OTHER INFO

    this._lastQuery = {
      currentpage: 1
    }; // Selected Web3 External provider (Metamask, Ledger, Trezor, etc.)

    this._externalProvider = null; // Supported Web3 External providers (Metamask, Ledger, Trezor, etc.)

    this._supportedExternalProviders = null; // The method that has been used to unlock this wallet

    this._unlockMethod = null;
    this._chainInfo = _objectSpread({}, baseChainInfo);
  }
  /**
   * Method that validates an address
   * @param {string} address Any blockchain address to validate 
   * @returns If the address is valid
   */


  async validateAddress(address) {
    return !!address;
  }
  /**
   * Method that validates an address or Alias
   * @param {string} addressLike Any blockchain address or Alias (ENS for example) to validate.
   * @returns {Promise<string?>} Returns the evaluated address (if is an Alias) or the given address if it is valid for the blockchain.
   */


  async resolveAddressOrENS(addressLike) {
    const isValidAddress = await this.validateAddress(addressLike);

    if (isValidAddress) {
      return addressLike;
    }
  }
  /* GETTERS/SETTERS */


  get unlockMethods() {
    return this._unlockMethods;
  }

  set unlockMethods(value) {
    this._unlockMethods = value;
  }

  get supportedExternalProviders() {
    return this._supportedExternalProviders;
  }

  set supportedExternalProviders(value) {
    this._supportedExternalProviders = value;
  }

  get externalProvider() {
    return this._externalProvider;
  }

  set externalProvider(value) {
    var _list$find;

    // Check against null/undefined and find the provider
    const list = this.supportedExternalProviders;
    const providerInList = list === null || list === void 0 ? void 0 : (_list$find = list.find) === null || _list$find === void 0 ? void 0 : _list$find.call(list, provider => {
      var _value$type;

      return equalsInsensitive(provider.type, (_value$type = value === null || value === void 0 ? void 0 : value.type) !== null && _value$type !== void 0 ? _value$type : value);
    });

    if (providerInList) {
      this._externalProvider = validateProvider(providerInList); // Auto-unlock wallet since it is external
      // this.unlocked = true;
    }
  }

  get unlocked() {
    return this._unlocked;
  }

  set unlocked(value) {
    var _this$onUnlockedChang;

    (_this$onUnlockedChang = this.onUnlockedChange) === null || _this$onUnlockedChang === void 0 ? void 0 : _this$onUnlockedChang.call(this, value);
    this._unlocked = value;
  }

  get chainInfo() {
    return this._chainInfo;
  }

  set chainInfo(value) {
    this._chainInfo = value;
  }

  get address() {
    return this._address;
  }

  set address(value) {
    this._address = value;
  }

  get firstAddress() {
    return this.address;
  }

  get addressList() {
    return [this.firstAddress];
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
  }

  get chain() {
    return this._chain;
  }

  set chain(value) {
    this._chain = value;
  }

  get cachedBalance() {
    return this._parseBalance(this._balance);
  }

  set cachedBalance(value) {
    this._balance = value;
  }

  get unlockedMethod() {
    return this._unlockedMethod;
  }

  set unlockedMethod(value) {
    this._unlockedMethod = value;
  }

  get gasPriceList() {
    return this._gasPriceList;
  }

  set gasPriceList(value) {
    this._gasPriceList = value;
  }

  get baseFee() {
    return this._baseFee;
  }

  set baseFee(value) {
    this._baseFee = value;
  }

  async setPrivateKey(privateKey, savePrivateKey = true) {
    if (!privateKey) {
      throw new Error('no private key');
    }

    this._privateKey = privateKey;
    this.unlock();
    this._externalProvider = null;
    await removeWalletFromExternalList(this.firstAddress);

    if (savePrivateKey) {
      sessionStorage.setItem('private-key-' + this.firstAddress, privateKey);
    }

    return privateKey;
  }

  setAddress(address) {
    this.address = address;
  }
  /**
   * @param {boolean} firstAddress Param only for BTC-Like chainers
   */


  getAddress(firstAddress = false) {
    if (firstAddress) {
      return this.firstAddress;
    }

    return this.address;
  }

  getPrivateKey() {
    return this._privateKey;
  }

  setWallet(wallet) {
    this.wallet = wallet;
    return wallet;
  }

  getWallet() {
    return this.wallet;
  }

  setBalance(balance) {
    this.cachedBalance = balance;
    return balance;
  }

  getBalanceCached() {
    return this.cachedBalance;
  }
  /**
   * This function should be always overriden! (and be a promise)
   */


  getBalance() {
    return Promise.resolve(this._parseBalance(this.cachedBalance));
  }

  setTransactions(transactions) {
    this._transactions = transactions;
    return transactions;
  }

  getTransactions() {
    return this._transactions;
  }

  setName(name) {
    //! deprecated
    this.name = name;
  }

  getName() {
    //! deprecated
    return this.name;
  }

  setSocketEndpoint(endpoint) {
    this._socketEndpoint = endpoint;
  }

  getSocketEndpoint() {
    return this._socketEndpoint;
  }

  setChainInfo(chainInfo) {
    this._chainInfo = chainInfo;
  }

  getChainInfo() {
    return this._chainInfo;
  }

  setUnlockMethods(unlockMethods) {
    this._unlockMethods = unlockMethods;
  }

  getUnlockMethods() {
    return this._unlockMethods;
  }
  /* GETTERS */


  get blockie() {
    return getBlockie(this.chain, this.firstAddress.toLowerCase());
  }

  get isCustomChain() {
    return !!this._chainInfo.customChain;
  }

  get customChain() {
    return this._chainInfo.customChain;
  }

  get hasCachedBalance() {
    return !this._balance || isEmptyObject(this._balance);
  }

  get isExternalSigner() {
    return !!this.externalProvider;
  }

  get id() {
    return this._id;
  }

  get eventDispatcher() {
    return this._eventDispatcher;
  }

  getChain() {
    return this.chain;
  }

  getId() {
    //! deprecated
    return this.id;
  }

  isTestnet() {
    return this._isTestnet;
  }

  getBlockie() {
    return this.blockie;
  }

  getSubscriptions() {
    return this._subscriptions;
  }

  getDecimals() {
    const chainInfo = this.getChainInfo();
    return chainInfo.decimals;
  }

  getSocketclient() {
    //! deprecated
    return this.getSocketClient();
  }

  getSocketClient() {
    return this._socketClient;
  }

  getSocketClientSwap() {
    return this._socketClientSwap;
  }
  /* CHAIN INFO GETTERS */


  get logo() {
    return this._chainInfo.logo;
  }

  get symbol() {
    return this._chainInfo.symbol;
  }

  get ticker() {
    return this._chainInfo.ticker;
  }

  get chainId() {
    return this._chainInfo.chainId;
  }

  get isEthereumLike() {
    return this._chainInfo.isEthereumLike;
  }

  get isBitcoinLike() {
    return this._chainInfo.isBitcoinLike;
  }

  get isWavesLike() {
    return this._chainInfo.isWavesLike;
  }

  getTicker() {
    //! deprecated
    return this.ticker;
  }

  getSymbol() {
    //! deprecated
    return this.symbol;
  }

  getLogo() {
    //! deprecated
    return this.logo;
  }
  /* JSON WALLET */


  getJsonWalletName(brandName, nickname, email) {
    let chain = this.chain;

    if (brandName.toLowerCase() === "luxochain") {
      chain = "LUXOCHAIN";
    }

    return `${brandName}Wallet - ${chain} - ${nickname !== null && nickname !== void 0 ? nickname : email} - ${textElision(this.firstAddress)} - ${this.name}.json`;
  }
  /* MNEMONIC PHRASE */

  /**
   * @param {string} mnemonicPhrase Mnemonic phrase
   */


  setMnemonicPhrase(mnemonicPhrase = '', saveMnemonicPhrase = true) {
    this.mnemonicPhrase = mnemonicPhrase;

    if (saveMnemonicPhrase) {
      sessionStorage.setItem('mnemonic-phrase-' + this.getAddress(true), mnemonicPhrase);
    }
  }

  getMnemonicPhrase() {
    return this.mnemonicPhrase;
  }

  set mnemonicPhrase(value) {
    this._mnemonicPhrase = value;
  }

  get mnemonicPhrase() {
    let mnemonicPhrase = this._mnemonicPhrase;

    if (!mnemonicPhrase) {
      mnemonicPhrase = sessionStorage.getItem('mnemonic-phrase-' + this.firstAddress);
      this._mnemonicPhrase = mnemonicPhrase;
    }

    return mnemonicPhrase;
  }
  /* UN/LOCKING METHODS */


  async lock(address) {
    this._privateKey = undefined;
    this.unlocked = false;

    if (address) {
      sessionStorage.removeItem('private-key-' + address);
      sessionStorage.removeItem('mnemonic-phrase-' + address);
    }

    return true;
  }

  unlock() {
    this.unlocked = true;
  }

  isUnlocked() {
    return this._unlocked;
  }
  /* METHODS */


  async getTransaction(id, checkPending) {
    throw new Error('Unimplemented!');
  }
  /**
   * Cleans chainer state closing all websockets
   */


  dispose() {
    this.wsocketClose();
    this.removeSocketSwap();
    return true;
  }

  getExternalExplorerLink(transactionHash) {
    throw new Error('Unimplemented!');
  }
  /* WEB SOCKET METHODS */


  wsocketInit(intervals, intervalBalance = true, intervalTransactions = true, query = {}, intervalInfoSendFunds = true) {
    const url = this.getSocketEndpoint();

    if (url) {
      // intervals -> send only basic info, not trigger interval for [balance, txs]
      this._socketClient = io(url, {
        withCredentials: true,
        query: _objectSpread({
          chain: this.chain,
          currentaddress: this.getAddress(true),
          fromAddress: this.getAddress(true),
          intervals,
          nativeCoin: this.getTicker(),
          intervalBalance,
          intervalTransactions,
          intervalInfoSendFunds
        }, query)
      });
      this.reAddSubscriptions();
    }
  }

  reAddSubscriptions() {
    for (const type of Object.keys(this._subscriptions)) {
      for (const functionId in this._subscriptions[type]) {
        if (Object.hasOwnProperty.call(this._subscriptions[type], functionId)) {
          this.subscribe(type, this._subscriptions[type][`${functionId}`]);
        }
      }
    }
  }

  wsocketClose() {
    if (this._socketClient) {
      this._socketClient.disconnect();
    }
  }

  addSubscription(type, callback) {
    if (!this._subscriptions[type]) {
      this._subscriptions[type] = [];
    }

    const id = generateRandomId();
    this._subscriptions[type][id] = callback;
    this.subscribe(type, callback);
    return id;
  }

  removeSubscription(type, id) {
    if (this._subscriptions[type]) {
      this._socketClient.removeListener(type);

      delete this._subscriptions[type][id];
    }
  }

  subscribe(type, callback) {
    if (this._socketClient) {
      this._socketClient.on(type, result => {
        if (type === 'balance') {
          this._balance = result;
        }

        if (this.setVariablesSubscription) {
          result = this.setVariablesSubscription(type, result);
        }

        callback(result);
      });
    }
  }

  emitRefreshAll() {
    if (this._socketClient) {
      const query = {
        chain: this.chain
      };

      this._socketClient.emit('refreshAll', query);
    }
  }

  emitGetTransactions(query) {
    if (this._socketClient) {
      query = _objectSpread(_objectSpread(_objectSpread({}, this._lastQuery), query), {}, {
        chain: this.chain
      });

      this._socketClient.emit('getTransaction', query);

      this._lastQuery = query;
    }
  }

  emitGetBalance(query) {
    query = _objectSpread(_objectSpread(_objectSpread({}, this._lastQuery), query), {}, {
      chain: this.chain
    });

    this._socketClient.emit('getBalance', query);

    this._lastQuery = query;
  }

  async loadFromStore(address) {
    throw new Error('Unimplemented loadFromStore');
  }
  /* WEB SOCKET SWAP METHODS */


  loadSwap(timeoutMs = 20000, params = {}) {
    const urlSocketSwap = this._socketSwapEndpoint;
    const timeoutPromise = new Promise((_, reject) => {
      const timeoutId = setTimeout(() => {
        clearTimeout(timeoutId);
        reject(`loadSwap() timeout out in ${timeoutMs} ms!`);
      }, timeoutMs);
    });
    const createSwapSocketPromise = new Promise((resolve, reject) => {
      var _this$_socketClientSw;

      if (!((_this$_socketClientSw = this._socketClientSwap) !== null && _this$_socketClientSw !== void 0 && _this$_socketClientSw.connected)) {
        this._socketClientSwap = io(urlSocketSwap, {
          withCredentials: true,
          query: _objectSpread({
            chain: this.chain,
            fromAddress: this.getAddress(true),
            nativeCoin: this.ticker,
            useV3: false
          }, params)
        });

        this._socketClientSwap.on('connect', resolve);

        this._socketClientSwap.on('connect_error', reject);
      }
    });
    return Promise.race([timeoutPromise, createSwapSocketPromise]);
  }

  removeSocketSwap() {
    if (this._socketClientSwap) {
      this._socketClientSwap.disconnect();
    }
  }

  onMessageSwap(callback) {
    if (this._socketClientSwap) {
      this._socketClientSwap.on('message', callback);
    }
  } // deprecated
  // onGasPriceSwap(callback) {
  //     if (this._socketClientSwap) {
  //         this._socketClientSwap.on("gasprice", data => {
  //             callback(data);
  //         });
  //     }
  // }


  onNewExchangeQuoteSwap(callback) {
    if (this._socketClientSwap) {
      this._socketClientSwap.on('newExchangeQuote', callback);
    }
  }
  /* BTC-LIKE */


  get hd() {
    return this._hd;
  }

  set hd(value) {
    this._hd = _objectSpread({}, value);
  }

  get isHDWallet() {
    return this._isHDWallet;
  }

  set isHDWallet(value) {
    this._isHDWallet = value;
  }

  setWalletTypes(walletTypes) {
    this._walletTypes = walletTypes;
  }

  getWalletTypes() {
    return this._walletTypes;
  }
  /* THEME OBJECT */
  //! Deprecated: use the getter


  getThemeObject() {
    return this.themeObject;
  }

  get themeObject() {
    return getThemeObject(this.chain);
  }
  /* TOOLTIP INFO */


  getTooltipInfo() {
    return {};
  }

}

export default Chainer;