import NokuChainChainer from './NokuChainChainer';
import { NokuChainTestConstants } from '../../functions/getChainConstants';

class NokuChaintestChainer extends NokuChainChainer {
  constructor(chain = 'NOKU_CHAINTEST', socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance = b => b, _initOptions) {
    super(chain, socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance, _initOptions);
    this._isTestnet = true;
    this._customChain = NokuChainTestConstants.customChain;
    this.setChainInfo(NokuChainTestConstants.chainInfo);
  }

}

export default NokuChaintestChainer;