import EthChainer from './EthChainer';
import { BscConstants } from '../../functions/getChainConstants';

class BscChainer extends EthChainer {
  constructor(chain = 'BSC', socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance = b => b, _initOptions) {
    super(chain, socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance, _initOptions);
    this.unlockMethods = BscConstants.unlockMethods;
    this._customChain = BscConstants.customChain;
    this.setChainInfo(BscConstants.chainInfo);
  }

  getExternalExplorerLink(hash) {
    return `https://bscscan.com/tx/${hash}`;
  }

}

export default BscChainer;