import EthChainer from './EthChainer';
import { NokuChainConstants } from '../../functions/getChainConstants';

class NokuChainChainer extends EthChainer {
  constructor(chain = 'NOKU_CHAIN', socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance = b => b, _initOptions) {
    super(chain, socketEndpoint, socketSwapEndpoint, _getNonce, _sendRawTransaction, _estimateGas, _getAddressByENS, _getBalance, _getTransaction, _sendRawTransactionWithParams, _parseBalance, _initOptions);
    this.unlockMethods = NokuChainConstants.unlockMethods;
    this._customChain = NokuChainConstants.customChain;
    this.setChainInfo(NokuChainConstants.chainInfo);
  }

  getExternalExplorerLink(hash) {
    return null;
  }

}

export default NokuChainChainer;