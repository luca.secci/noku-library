const _excluded = ["onClick", "clickable", "shadow", "bodyClassName", "bodyStyle"];

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React from 'react';
/**
 * CroppedBordersCard
 * @prop {Function} onClick on card click callback
 * @prop {string} bodyClassName="" additional card classes
 * @prop {Object} bodyStyle={} additional card style
 */

const CroppedBordersCard = /*#__PURE__*/React.forwardRef((_ref, ref) => {
  let {
    onClick = () => null,
    clickable = false,
    shadow = true,
    bodyClassName = '',
    bodyStyle = {}
  } = _ref,
      props = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/React.createElement("div", {
    className: `noku-library-cropped-borders-card-body ${bodyClassName}`,
    onClick: onClick,
    ref: ref,
    style: bodyStyle
  }, " ", props.children, " ");
});
export default CroppedBordersCard;