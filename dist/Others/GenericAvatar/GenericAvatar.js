const _excluded = ["avatarClassName", "avatarStyle", "size", "image", "hasBadge", "badgeContent", "badgeClassName", "children", "imageSize"];

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Badge from '@material-ui/core/Badge';
import classes from './GenericAvatar.module.scss';

const withBadge = (Avatar, badgeContent, badgeClassName) => /*#__PURE__*/React.createElement(Badge, {
  overlap: "circle",
  anchorOrigin: {
    vertical: 'bottom',
    horizontal: 'right'
  },
  badgeContent: badgeContent,
  className: badgeClassName
}, Avatar);

const GenericAvatar = _ref => {
  let {
    avatarClassName = '',
    avatarStyle = {},
    size = 'medium',
    image,
    hasBadge = false,
    badgeContent,
    badgeClassName,
    children,
    imageSize = ''
  } = _ref,
      props = _objectWithoutProperties(_ref, _excluded);

  const InnerAvatar = /*#__PURE__*/React.createElement(Avatar, {
    className: `${classes.muiCustom} ${classes.genericAvatar} ${size === 'medium' ? classes.medium : classes.small} ${imageSize === 'cover' ? classes.cover : ''} ${avatarClassName}`,
    style: avatarStyle,
    src: image
  }, children);
  return hasBadge ? withBadge(InnerAvatar, badgeContent, badgeClassName) : InnerAvatar;
};

export default GenericAvatar;