import React from 'react';
import BackButton from '../../Input/BackButton/BackButton';
import classes from './BaseStep.module.scss';
/**
 * @prop {boolean} hideBack=false if true will hide the button
 * @prop {Function} goBack callback called when button is clicked
 * @prop {string} goBackTitle button text
 * @prop {JSX} children
 */

const BaseStep = ({
  goBack,
  goBackTitle,
  hideBack,
  children
}) => /*#__PURE__*/React.createElement("div", {
  className: classes.step
}, /*#__PURE__*/React.createElement(BackButton, {
  goBack: goBack,
  goBackTitle: goBackTitle,
  hideBack: hideBack
}), children);

export default BaseStep;