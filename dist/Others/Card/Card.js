const _excluded = ["onClick", "clickable", "shadow", "bodyClassName", "bodyStyle", "children"];

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React from 'react';
/**
 * Card
 * @prop {Function} onClick on card click callback
 * @prop {boolean} clickable=false if true will apply additional style to the card
 * @prop {boolean} shadow=true if true will apply a box-shadow
 * @prop {string} bodyClassName="" additional card classes
 * @prop {Object} bodyStyle={} additional card style
 */

const Card = /*#__PURE__*/React.forwardRef((_ref, ref) => {
  let {
    onClick = () => null,
    clickable = false,
    shadow = true,
    bodyClassName = '',
    bodyStyle = {},
    children
  } = _ref,
      props = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/React.createElement("div", _extends({
    className: `noku-library-card-body ${clickable ? 'clickable' : ''} ${shadow ? 'shadow' : ''} ${bodyClassName}`,
    onClick: onClick,
    ref: ref,
    style: bodyStyle
  }, props), children);
});
Card.displayName = "Card";
export default Card;