function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import ViewSlider from 'react-view-slider';
import classes from './CustomSlider.module.scss';
const baseSettings = {
  spacing: 1.1,
  keepViewsMounted: true,
  // avoid some height problems
  animateHeight: false,
  fillParent: false,
  // avoid true otherwise it will fill all the space
  transitionDuration: 500 // default speed

};
export const CustomSlider = props => {
  const renderView = ({
    index,
    active,
    transitionState
  }) => props.steps ? props.steps[index] : /*#__PURE__*/React.createElement(React.Fragment, null, " ");

  return /*#__PURE__*/React.createElement(React.Fragment, null, props.beforeSlider, /*#__PURE__*/React.createElement(ViewSlider, _extends({}, baseSettings, props.settings, {
    activeView: props.activeStep,
    renderView: renderView,
    numViews: props.steps.length,
    viewportClassName: !props.isModal ? classes.centerViewport : '',
    innerViewWrapperStyle: !props.isModal && props.slideWidth ? {
      minWidth: props.slideWidth
    } : {}
  })), props.afterSlider);
};
export default CustomSlider;