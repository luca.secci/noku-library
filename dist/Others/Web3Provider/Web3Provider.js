import React from "react"; // import { Web3ReactProvider } from "@web3-react/core";

import { Web3ReactProvider } from "web3-react/packages/core/dist";
import { Web3Provider as EthersWeb3Provider } from "@ethersproject/providers";
export const getLibrary = provider => {
  const library = new EthersWeb3Provider(provider);
  library.pollingInterval = 8000;
  return library;
};
export const Web3Provider = ({
  children
}) => /*#__PURE__*/React.createElement(Web3ReactProvider, {
  getLibrary: getLibrary
}, children);
export default Web3Provider;