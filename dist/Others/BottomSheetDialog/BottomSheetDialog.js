import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import { makeStyles } from '@material-ui/core/styles';
import Button from '../../Input/Button/Button';
import classes from './BottomSheetDialog.module.scss';

const BottomSheetDialog = ({
  isVisible,
  title,
  onBackdropClick,
  showCancel = true,
  cancelText = 'Cancel',
  zIndex = 2,
  children
}) => {
  const useStyles = makeStyles(() => ({
    backdrop: {
      zIndex
    }
  }));
  const backdropClasses = useStyles();
  return /*#__PURE__*/React.createElement("div", {
    className: `${classes.dialogWrapper} ${isVisible ? classes.visible : ''}`,
    style: {
      zIndex
    }
  }, /*#__PURE__*/React.createElement(Backdrop, {
    className: backdropClasses.backdrop,
    open: isVisible,
    onClick: onBackdropClick
  }), /*#__PURE__*/React.createElement("div", {
    className: `${classes.bottomSheetDialogWrapper} ${isVisible ? classes.visible : ''}`,
    style: {
      zIndex: zIndex + 1
    }
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.header
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.draggableZone
  }), /*#__PURE__*/React.createElement("div", {
    className: classes.title
  }, " ", /*#__PURE__*/React.createElement("span", null, title), showCancel ? /*#__PURE__*/React.createElement(Button, {
    style: "flat",
    onClick: onBackdropClick
  }, " ", cancelText, " ") : /*#__PURE__*/React.createElement(React.Fragment, null, " "))), /*#__PURE__*/React.createElement("div", {
    className: classes.body
  }, children)));
};

export default BottomSheetDialog;