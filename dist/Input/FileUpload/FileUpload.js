function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Avatar } from "@material-ui/core";
import React, { useCallback, useMemo, useState } from "react";
import { useDropzone } from "react-dropzone";
import { useTranslation } from "react-i18next";
import { readImageFile } from "../../utils/readImageFile";
import Button from "../Button/Button";

const FileUpload = ({
  multiple = false,
  acceptedFiles = "image/jpg, image/jpeg, image/png",
  maxSize = 5 * 1024 * 1024,
  dropped = () => {},
  dropRejected = () => {},
  showPreview = false,
  title = "",
  subtitle = "",
  className = "",
  hasImagePreview = true
}) => {
  const {
    t
  } = useTranslation("noku-library");
  const [uploadSrc, setUploadSrc] = useState();
  const [fileName, setFileName] = useState();
  const getImageSrc = useCallback(async file => {
    try {
      const imgSrc = await readImageFile(file);
      setUploadSrc(imgSrc);
      setFileName(file === null || file === void 0 ? void 0 : file.name);
    } catch (error) {
      console.error(error);
    }
  }, []);
  const onDrop = useCallback(acceptedFiles => {
    getImageSrc(acceptedFiles[0]);
    dropped(multiple ? acceptedFiles : acceptedFiles[0]);
  }, [dropped, getImageSrc, multiple]);
  const onDropRejected = useCallback(rejectedFiles => {
    dropRejected(rejectedFiles);
    setUploadSrc();
  }, [dropRejected]);
  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragReject
  } = useDropzone({
    multiple,
    acceptedFiles,
    maxSize,
    onDrop,
    onDropRejected
  });
  const UploaderBody = useMemo(() => {
    let Preview = /*#__PURE__*/React.createElement(React.Fragment, null);

    if (hasImagePreview) {
      Preview = /*#__PURE__*/React.createElement(Avatar, {
        classes: {
          root: "avatar"
        }
      }, /*#__PURE__*/React.createElement("img", {
        src: uploadSrc,
        alt: "preview"
      }));
    } else {
      Preview = /*#__PURE__*/React.createElement("div", {
        className: "file-name"
      }, fileName);
    }

    if (showPreview && uploadSrc) {
      return /*#__PURE__*/React.createElement(React.Fragment, null, Preview, /*#__PURE__*/React.createElement(Button, {
        buttonClassName: "button",
        startIcon: /*#__PURE__*/React.createElement(FontAwesomeIcon, {
          icon: "folder"
        }),
        startIconClassName: "icon",
        color: "gradient"
      }, t("changeFile")));
    } else {
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
        className: "title"
      }, title), /*#__PURE__*/React.createElement("div", {
        className: "subtitle"
      }, subtitle), /*#__PURE__*/React.createElement(Button, {
        buttonClassName: "button",
        startIcon: /*#__PURE__*/React.createElement(FontAwesomeIcon, {
          icon: "folder"
        }),
        startIconClassName: "icon",
        color: "gradient"
      }, t("chooseFile")));
    }
  }, [fileName, hasImagePreview, showPreview, subtitle, title, uploadSrc]);
  return /*#__PURE__*/React.createElement("div", _extends({}, getRootProps(), {
    className: `noku-library-file-upload file-upload ${className} ${isDragActive ? "dropping" : ""} ${isDragReject ? "rejecting" : ""}`
  }), UploaderBody, /*#__PURE__*/React.createElement("input", getInputProps()));
};

export default FileUpload;