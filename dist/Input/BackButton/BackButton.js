import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
/**
 * @prop {boolean} hideBack=false if true will hide the button
 * @prop {Function} goBack callback called when button is clicked
 * @prop {string} goBackTitle button text
 * @prop {JSX} icon override arrow-left icon with something else
 * @prop {string} wrapperClassName additional wrapper classes
 * @prop {string} buttonClassName additional button classes
 * @prop {string} titleClassName additional title classes
 * @prop {JSX} children
 */

const BackButton = ({
  hideBack = false,
  goBack,
  goBackTitle = 'Back',
  icon,
  wrapperClassName = '',
  buttonClassName = '',
  titleClassName = '',
  children
}) => /*#__PURE__*/React.createElement(React.Fragment, null, hideBack ? /*#__PURE__*/React.createElement(React.Fragment, null, " ") : /*#__PURE__*/React.createElement("div", {
  className: `noku-library-back-button ${wrapperClassName}`
}, /*#__PURE__*/React.createElement("div", {
  className: `back ${buttonClassName}`,
  onClick: goBack
}, icon !== null && icon !== void 0 ? icon : /*#__PURE__*/React.createElement(FontAwesomeIcon, {
  className: 'arrow-icon',
  icon: "arrow-left"
}), /*#__PURE__*/React.createElement("span", {
  className: `title ${titleClassName}`
}, goBackTitle)), children));

export default BackButton;