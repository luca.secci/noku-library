import React from 'react';

const HamburgerButton = ({
  isOpen = false,
  onClick = () => {}
}) => /*#__PURE__*/React.createElement("div", {
  className: `noku-library-hamburger ${isOpen ? "open" : ""}`,
  onClick: onClick
}, /*#__PURE__*/React.createElement("div", null), /*#__PURE__*/React.createElement("div", null), /*#__PURE__*/React.createElement("div", null));

export default HamburgerButton;