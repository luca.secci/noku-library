const _excluded = ["elementConfig", "changed", "disabled", "defaultValue", "stateKey", "value", "type", "placeholder", "label", "labelClassName", "beforeIcon", "beforeIconClassName", "afterIcon", "afterIconClassName", "error", "errorMessage", "wrapperClassName", "inputClassName", "blurred", "inputContainerClassName", "rounded", "fluid", "required", "showRequiredAsterisk", "showPasswordSwitch"];

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Select from '../Select/Select';
import Checkbox from '../Checkbox/Checkbox';
export const BaseInput = /*#__PURE__*/React.forwardRef((_ref, ref) => {
  let {
    elementConfig = {},
    changed = () => null,
    disabled = false,
    defaultValue,
    stateKey,
    value,
    type,
    placeholder,
    label,
    labelClassName,
    beforeIcon,
    beforeIconClassName,
    afterIcon,
    afterIconClassName,
    error,
    errorMessage = '',
    wrapperClassName,
    inputClassName,
    blurred = () => null,
    inputContainerClassName,
    rounded = false,
    fluid,
    required,
    showRequiredAsterisk = true,
    showPasswordSwitch = false
  } = _ref,
      props = _objectWithoutProperties(_ref, _excluded);

  let InputElement = /*#__PURE__*/React.createElement(React.Fragment, null, " ");
  const [showPassword, setShowPassword] = useState(false);

  const generalConfig = _objectSpread(_objectSpread({}, elementConfig), {}, {
    value,
    onChange: e => {
      var _e$target$value, _e$target;

      return changed(stateKey, (_e$target$value = e === null || e === void 0 ? void 0 : (_e$target = e.target) === null || _e$target === void 0 ? void 0 : _e$target.value) !== null && _e$target$value !== void 0 ? _e$target$value : e, e);
    },
    onBlur: e => {
      var _e$target$value2, _e$target2;

      return blurred(stateKey, (_e$target$value2 = e === null || e === void 0 ? void 0 : (_e$target2 = e.target) === null || _e$target2 === void 0 ? void 0 : _e$target2.value) !== null && _e$target$value2 !== void 0 ? _e$target$value2 : e, e);
    },
    className: `input ${inputClassName}`,
    disabled,
    defaultValue,
    placeholder,
    required
  });

  switch (type) {
    case 'textarea':
      InputElement = /*#__PURE__*/React.createElement("textarea", _extends({
        ref: ref
      }, generalConfig));
      break;

    case 'select':
      InputElement = /*#__PURE__*/React.createElement(Select, _extends({
        ref: ref
      }, generalConfig));
      break;
    // MAYBEDO

    case 'checkbox':
      InputElement = /*#__PURE__*/React.createElement(Checkbox, generalConfig);
      break;

    case 'password':
      InputElement = /*#__PURE__*/React.createElement("input", _extends({
        ref: ref,
        type: showPassword ? 'text' : 'password'
      }, generalConfig));
      break;

    default:
      InputElement = /*#__PURE__*/React.createElement("input", _extends({
        ref: ref,
        type: type
      }, generalConfig));
      break;
  }

  const Label = label ? /*#__PURE__*/React.createElement("label", {
    className: `label ${labelClassName}`
  }, label, showRequiredAsterisk && required ? '*' : '') : /*#__PURE__*/React.createElement(React.Fragment, null, " ");
  const BeforeIcon = beforeIcon ? /*#__PURE__*/React.createElement("span", {
    className: `icon before-icon ${beforeIconClassName}`
  }, beforeIcon) : /*#__PURE__*/React.createElement(React.Fragment, null, " ");
  let AfterIcon = /*#__PURE__*/React.createElement(React.Fragment, null, " ");

  if (afterIcon) {
    AfterIcon = /*#__PURE__*/React.createElement("span", {
      className: `icon after-icon ${afterIconClassName}`
    }, afterIcon);
  } else if (showPasswordSwitch) {
    AfterIcon = /*#__PURE__*/React.createElement("span", {
      className: `icon after-icon ${afterIconClassName}`,
      onClick: () => setShowPassword(!showPassword)
    }, /*#__PURE__*/React.createElement(FontAwesomeIcon, {
      icon: ['fas', showPassword ? 'eye-slash' : 'eye']
    }));
  }

  const ErrorMessage = /*#__PURE__*/React.createElement("div", {
    className: 'error-message'
  }, errorMessage);
  return /*#__PURE__*/React.createElement("div", {
    className: `noku-library-base-input-wrapper ${error ? 'error' : ''} ${fluid ? 'fluid' : ''} ${rounded ? 'rounded' : ''} ${disabled ? 'disabled' : ''} ${type} ${wrapperClassName}`
  }, Label, /*#__PURE__*/React.createElement("div", {
    className: `input-container ${type} ${inputContainerClassName}`
  }, BeforeIcon, InputElement, AfterIcon), ErrorMessage);
});
export default BaseInput;