const _excluded = ["croppedBordersClassName", "croppedBordersVariant", "children", "buttonClassName"];

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React from 'react';
import croppedGradientBorders from "./croppedBorders/button-cropped-border-gradient.svg";
import Button from '../Button/Button';
export const CroppedBordersButton = _ref => {
  let {
    croppedBordersClassName = "",
    croppedBordersVariant = "",
    children,
    buttonClassName = ""
  } = _ref,
      props = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/React.createElement(Button, _extends({
    buttonClassName: `noku-library-cropped-borders-button ${buttonClassName}`,
    color: "light-primary"
  }, props), /*#__PURE__*/React.createElement("img", {
    src: croppedGradientBorders,
    className: `cropped-borders ${croppedBordersClassName}`,
    alt: ""
  }), /*#__PURE__*/React.createElement("span", {
    className: "text"
  }, children));
};
export default CroppedBordersButton;