import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const LargeButton = ({
  onClick,
  isActive,
  href,
  icon,
  text
}) => /*#__PURE__*/React.createElement("button", {
  onClick: onClick,
  className: `noku-library-large-button large-button ${isActive ? 'active' : ''}`
}, href ? /*#__PURE__*/React.createElement("a", {
  href: href,
  className: "link"
}, "\xA0") : null, /*#__PURE__*/React.createElement("div", {
  className: "text"
}, icon ? /*#__PURE__*/React.createElement("div", {
  className: 'icon'
}, /*#__PURE__*/React.createElement(FontAwesomeIcon, {
  icon: icon
})) : null, /*#__PURE__*/React.createElement("span", null, " ", text, " ")), /*#__PURE__*/React.createElement("span", {
  className: "chevron"
}, /*#__PURE__*/React.createElement(FontAwesomeIcon, {
  icon: 'angle-right'
})));

export default LargeButton;