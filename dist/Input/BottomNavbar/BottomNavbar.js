import React from 'react';

const FloatingActionButton = ({
  content,
  onClick
}) => /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
  className: "noku-library-floating-action-button",
  onClick: onClick
}, content), /*#__PURE__*/React.createElement("div", {
  className: "nav-item nav-item-casper"
}));

const BottomNavbar = ({
  menuItems = [],
  bottomNavbarClassName,
  itemClicked = () => null,
  autoWidth = false
}) => {
  const hasFab = menuItems.find(item => item.isFab);
  let basicStyle = {};

  if (autoWidth) {
    basicStyle = {
      width: `${100 / menuItems.length}%`
    };
  }

  return /*#__PURE__*/React.createElement("div", {
    className: `noku-library-bottom-navbar ${hasFab ? 'fab-cutout' : ''} ${bottomNavbarClassName}`
  }, menuItems.map(({
    content,
    isActive,
    isFab,
    style
  }, index) => isFab ? /*#__PURE__*/React.createElement(FloatingActionButton, {
    key: `bottom_navbar_item_${index}`,
    content: content,
    onClick: _ => itemClicked(index)
  }) : /*#__PURE__*/React.createElement("div", {
    key: `bottom_navbar_item_${index}`,
    className: `nav-item${basicStyle.width ? '' : ' no-auto-width'}`,
    onClick: _ => itemClicked(index),
    style: basicStyle
  }, /*#__PURE__*/React.createElement("div", {
    className: `nav-item-content ${isActive ? 'active' : ''}`,
    style: style !== null && style !== void 0 ? style : {}
  }, content))));
};

export default BottomNavbar;