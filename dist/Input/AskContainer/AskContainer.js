const _excluded = ["askContainerAction", "quantity", "tokenName", "addressTo", "feeText"];

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React, { useState, useEffect } from 'react';
import asyncSleep from "../../utils/asyncSleep";
import Button from "../../Input/Button/Button";
import Loader from "../../Output/Loader/Loader";
import Modal from "../../Output/Modal/Modal";

const AskContainer = _ref => {
  var _props$children, _props$progressText, _props$finishText;

  let {
    askContainerAction,
    quantity,
    tokenName,
    addressTo,
    feeText
  } = _ref,
      props = _objectWithoutProperties(_ref, _excluded);

  const [visible, setVisible] = useState(false);
  const [containerClasses, setContainerClasses] = useState('');

  const handleContainerClasses = async action => {
    console.log('handleContainerClasses');
    console.log(action);

    let _classes;

    switch (action) {
      case 'ask':
        _classes = 'ask';
        break;

      case 'load':
        _classes = 'ask load';
        break;

      case 'finish':
        _classes = 'ask load finish';
        break;

      default:
        break;
    }

    if (_classes) {
      setVisible(true);
      setContainerClasses(_classes);
    } else {
      const time = props.isMobile ? 150 : 650;
      setContainerClasses(_classes);
      await asyncSleep(time);
      setVisible(false);
      await asyncSleep(300);
      props.askClosed && props.askClosed();
    }
  };

  useEffect(() => {
    handleContainerClasses(askContainerAction);
  }, [askContainerAction, handleContainerClasses]);
  const content = visible ? /*#__PURE__*/React.createElement("div", {
    className: `noku-library-ask-container ${containerClasses} ${props.askContainerClassName}`
  }, /*#__PURE__*/React.createElement("div", {
    className: "finish-step"
  }, /*#__PURE__*/React.createElement("div", {
    className: "ask-layer"
  }, /*#__PURE__*/React.createElement("div", {
    className: "message"
  }, /*#__PURE__*/React.createElement("div", null, (_props$children = props.children) !== null && _props$children !== void 0 ? _props$children : /*#__PURE__*/React.createElement("span", null, "Are you sure to send ", /*#__PURE__*/React.createElement("span", {
    className: "quantity"
  }, " ", quantity, " ", tokenName, " "), "to ", /*#__PURE__*/React.createElement("span", {
    className: "address"
  }, " ", addressTo, " "), " ?")), /*#__PURE__*/React.createElement("div", {
    className: "fee"
  }, feeText)), /*#__PURE__*/React.createElement("div", {
    className: `buttons ${props.isMobile ? 'column-reverse' : ''}`
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: props.execute,
    color: props.isMobile ? 'secondary' : 'confirm'
  }, "Confirm"), /*#__PURE__*/React.createElement(Button, {
    onClick: props.discard,
    style: props.isMobile ? 'flat' : ''
  }, "Discard"))), /*#__PURE__*/React.createElement("div", {
    className: "loader-layer"
  }, /*#__PURE__*/React.createElement(Loader, null), /*#__PURE__*/React.createElement("svg", {
    className: 'checkmark finish-load',
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 52 52"
  }, /*#__PURE__*/React.createElement("circle", {
    className: 'checkmark__circle',
    cx: "26",
    cy: "26",
    r: "25",
    fill: "none"
  }), /*#__PURE__*/React.createElement("path", {
    className: 'checkmark__check',
    fill: "none",
    d: "M14.1 27.2l7.1 7.2 16.7-16.8"
  })), /*#__PURE__*/React.createElement("div", {
    className: 'progress'
  }, (_props$progressText = props.progressText) !== null && _props$progressText !== void 0 ? _props$progressText : 'Sending', " in progress..."), /*#__PURE__*/React.createElement("div", {
    className: 'finish'
  }, (_props$finishText = props.finishText) !== null && _props$finishText !== void 0 ? _props$finishText : 'Transaction sent')))) : /*#__PURE__*/React.createElement(React.Fragment, null, " ");
  return props.isMobile ? /*#__PURE__*/React.createElement(Modal, {
    open: visible,
    onCloseModal: props.discardExecute,
    isMobile: true
  }, content) : content;
};
/**
 * TODO: Update Docs
 * @prop {boolean} isMobile true if devices is mobile
class AskContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      containerClasses: null,
      visible: false
    }
  }

  discardExecute = (e) => {
    e && e.preventDefault && e.preventDefault();
    this.props.discardExecute && this.props.discardExecute();
  }

  execute = (e) => {
    e && e.preventDefault && e.preventDefault();
    this.props.execute && this.props.execute();
  }

  getContainerClasses() {
    return this.state.containerClasses;
  }

  setContainerClasses = async (action) => {

    let _classes = null;

    switch (action) {
      case 'ask':
        _classes = classes.ask;
        break;
      case 'load':
        _classes = `${classes.ask} ${classes.load}`;
        break;
      case 'finish':
        _classes = `${classes.ask} ${classes.load} ${classes.finish}`;
        break;
      default:
        break;
    }

    if (_classes) {
      this._isMounted && this.setState({ visible: true }, () => {
        this.setState({ containerClasses: _classes });
      });
    } else {
      let timeout = this.props.isMobile ? 150 : 650;
      this._isMounted && this.setState({ containerClasses: _classes }, async () => {
        await asyncSleep(timeout);

        this._isMounted && this.setState({ visible: false }, async () => {
          await asyncSleep(300);
          this.props.askClosed && this.props.askClosed();
        });
      });
    }
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.askContainerAction !== this.props.askContainerAction) {
      this.setContainerClasses(this.props.askContainerAction);
    }
  }

  render() {
    // let containerCss = this.getContainerClasses(this.props.askContainerAction);
    let containerCss = this.getContainerClasses();
    let quantity = this.props.amountToSend;
    let addressTo = this.props.addressTo;
    let tokenName = this.props.tokenName ? this.props.tokenName : ((this.props.token || {}.token) || {}).name;
    let feeText = `Fee ${this.props.totalFeeTx || 0} ${this.props.chainName}`;

    let content = <div className={`flexContainer spaceBetween ${classes.jsActionContainer} ${classes.maxContainer} ${containerCss}`}>
      <div className={`${classes.finishStep} ${this.state.visible ? `${classes.visible}` : ''}`}>
        <div className={classes.askLayer}>
          <div className={classes.message}>
            <div>
              {
                this.props.children ??
                <>
                  Are you sure to send <span className={classes.jsQuantity}>{quantity} {tokenName} </span>
                  to <span className={classes.jsAddress}> {addressTo} </span> ?
                </>
              }
            </div>
            <div className={classes.jsFee}>{feeText}</div>
          </div>
          <div className={`${classes.buttonsContainer} flexContainer ${this.props.isMobile ? 'columnReverse' : ''}`}>
            {this.props.isMobile ?
              <>
                <Button
                  onClick={this.execute}
                  className={`largeButtonMobile`}
                  type={"secondary"}
                >
                  Confirm
                </Button>
                <Button
                  onClick={this.discardExecute}
                  className={`largeButtonMobile`}
                >
                  Discard
                </Button>
              </>
              :
              <>
                <Button
                  onClick={this.execute}
                  className={`largeButtonMobile`}
                  type={"confirm"}
                >
                  Confirm
                </Button>
                <Button
                  onClick={this.discardExecute}
                  className={`largeButtonMobile`}
                  type={"discard"}
                >
                  Discard
                </Button>
              </>
            }

          </div>
        </div>
        <div className={classes.loaderLayer}>
          <Loader className={classes.loaderAction} />
          <svg
            className={`${classes.checkmark} ${classes.finishload}`}
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 52 52"
          >
            <circle
              className={classes.checkmark__circle}
              cx="26"
              cy="26"
              r="25"
              fill="none"
            />
            <path
              className={classes.checkmark__check}
              fill="none"
              d="M14.1 27.2l7.1 7.2 16.7-16.8"
            />
          </svg>
          <div className={classes.progress}>{this.props.progressText ?? "Sending"} in progress...</div>
          <div className={classes.finish}>{this.props.finishText ?? "Transaction sent"}</div>
        </div>
      </div>
    </div>

    return (this.props.isMobile ? <Modal
      open={this.state.visible}
      onCloseModal={this.discardExecute}
      actions={false}
    >
      {content}
    </Modal> : content
    );
  }
}
*/


export default AskContainer;