import React from "react";
import { FormControl, InputLabel, MenuItem, Select as MuiSelect } from "@material-ui/core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SelectIcon = ({
  className = ""
}) => /*#__PURE__*/React.createElement(FontAwesomeIcon, {
  className: `${className} select-icon`,
  icon: "chevron-down"
});

const Select = ({
  onChange = () => {},
  value = "",
  options = [],
  label = "",
  variant = "outlined",
  size = "medium",
  color = "primary",
  className = "",
  labelClassName = "",
  selectClassName = "",
  menuItemClassName = ""
}) => {
  return /*#__PURE__*/React.createElement(FormControl, {
    classes: {
      root: `noku-library-select ${size} ${variant} ${color} ${className}`
    },
    variant: variant,
    size: size
  }, /*#__PURE__*/React.createElement(InputLabel, {
    classes: {
      root: `label ${labelClassName}`
    }
  }, label), /*#__PURE__*/React.createElement(MuiSelect, {
    value: value,
    onChange: e => onChange(e.target.value),
    label: label,
    IconComponent: SelectIcon,
    classes: {
      select: `select ${selectClassName}`
    }
  }, options.map(({
    key,
    label,
    value
  }) => /*#__PURE__*/React.createElement(MenuItem, {
    key: `select-option-${key}`,
    classes: {
      root: `noku-library-select-menu-item ${menuItemClassName}`
    },
    value: value
  }, label))));
};

export default Select;