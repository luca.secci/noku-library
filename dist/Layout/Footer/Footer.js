const _excluded = ["className", "innerClassName", "Logo", "socialLinks"],
      _excluded2 = ["className"];

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React from 'react';
import { format } from 'date-fns';
import { IconButton } from '@material-ui/core';
export const Footer = _ref => {
  let {
    className = "",
    innerClassName = "",
    Logo = props => /*#__PURE__*/React.createElement(React.Fragment, null),
    socialLinks
  } = _ref,
      props = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/React.createElement("div", {
    className: `noku-library-footer ${className}`
  }, /*#__PURE__*/React.createElement("div", {
    className: `inner ${innerClassName}`
  }, /*#__PURE__*/React.createElement("div", {
    className: `copyright-and-logo`
  }, /*#__PURE__*/React.createElement(Logo, {
    className: `logo`
  }), /*#__PURE__*/React.createElement("div", {
    className: `copyright`
  }, "Copyright ", format(new Date(), "yyyy"), " Noku")), /*#__PURE__*/React.createElement("div", {
    className: `social-links`
  }, socialLinks.map(_ref2 => {
    let {
      key,
      link = "#",
      Icon,
      IconProps: {
        className: iconClassName = ""
      }
    } = _ref2,
        IconProps = _objectWithoutProperties(_ref2.IconProps, _excluded2);

    return /*#__PURE__*/React.createElement(IconButton, {
      href: link,
      target: "_blank",
      referrerPolicy: "no-referrer",
      key: key,
      classes: {
        root: "icon-button"
      }
    }, /*#__PURE__*/React.createElement(Icon, _extends({
      className: `icon ${iconClassName}`
    }, IconProps)));
  }))));
};
export default Footer;