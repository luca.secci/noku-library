const _excluded = ["onHamburgerClick", "logoSrc", "reverseLogoSrc", "items", "logoComponent", "rightSlot", "logoLink"];

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React from "react";
import { Link, NavLink } from "react-router-dom";

const MenuItem = ({
  destination,
  title,
  linkType,
  onItemClick,
  exact = true,
  forceActive = false,
  highlight = false
}) => {
  if (linkType === 'internal') {
    return /*#__PURE__*/React.createElement(NavLink, {
      className: `link ${forceActive ? 'active' : ''} ${highlight ? 'link-highlight' : ''}`,
      activeClassName: "active",
      to: destination,
      exact: exact,
      onClick: onItemClick
    }, title);
  } else {
    return /*#__PURE__*/React.createElement("a", {
      className: `link ${forceActive ? 'active' : ''} ${highlight ? 'link-highlight' : ''}`,
      href: destination,
      onClick: onItemClick
    }, " ", title, " ");
  }
};

export const NokuHeader = _ref => {
  let {
    onHamburgerClick = () => {},
    logoSrc,
    reverseLogoSrc,
    items = [],
    logoComponent,
    rightSlot = /*#__PURE__*/React.createElement(React.Fragment, null),
    logoLink = "/"
  } = _ref,
      props = _objectWithoutProperties(_ref, _excluded);

  let PrimaryLogo = () => /*#__PURE__*/React.createElement("img", {
    className: "logo-image primary",
    src: logoSrc,
    alt: "logo"
  }); // let SecondaryLogo = <img className="logo-image secondary" src={reverseLogoSrc} alt="logo" />;


  if (logoComponent) {
    PrimaryLogo = logoComponent;
  }

  return /*#__PURE__*/React.createElement("div", {
    className: "noku-library-header"
  }, /*#__PURE__*/React.createElement("div", {
    className: "hamburger",
    onClick: onHamburgerClick
  }, /*#__PURE__*/React.createElement("div", null), /*#__PURE__*/React.createElement("div", null), /*#__PURE__*/React.createElement("div", null)), /*#__PURE__*/React.createElement("div", {
    className: "logo"
  }, /*#__PURE__*/React.createElement(Link, {
    className: "logo-link",
    to: logoLink
  }, /*#__PURE__*/React.createElement(PrimaryLogo, null))), /*#__PURE__*/React.createElement("div", {
    className: `links`
  }, items.map((item = {}, i) => /*#__PURE__*/React.createElement(MenuItem, _extends({
    key: `navigator-menu-item-${i}`
  }, item)))), /*#__PURE__*/React.createElement("div", {
    className: "right-slot"
  }, rightSlot));
};
export default NokuHeader;