const _excluded = ["items", "logoSrc", "logoComponent", "rightSlot", "showHamburger", "linksClassName", "withMobileMenu", "onHamburgerClick", "hideLogo"];

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React, { useCallback, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
/**
 * @typedef {Object} MenuItems
 * @property {string} destination The link destination
 * @property {string} title What to show
 * @property {"internal"|"external"} linkType The type of the link, external or internal, when internal the link is rendered with <NavLink>
 * @property {(e: Event) => {}} onItemClick a callback to execute when this item has been clicked
 */

const MenuItem = ({
  destination,
  title,
  linkType,
  onItemClick,
  exact = true
}) => {
  if (linkType === 'internal') {
    return /*#__PURE__*/React.createElement(NavLink, {
      className: "link",
      activeClassName: "active",
      to: destination,
      exact: exact,
      onClick: onItemClick
    }, title);
  } else {
    return /*#__PURE__*/React.createElement("a", {
      className: "link",
      href: destination,
      onClick: onItemClick
    }, " ", title, " ");
  }
};
/**
 * @param {{ items: MenuItems[], logoSrc: string, rightSlot: JSX.Element, linksClassName: string, withMobileMenu: boolean, onHamburgerClick: Function }} props
 */


const MainNavigator = _ref => {
  let {
    items = [],
    logoSrc,
    logoComponent,
    rightSlot,
    showHamburger,
    linksClassName = '',
    withMobileMenu = true,
    onHamburgerClick = () => {},
    hideLogo = false
  } = _ref,
      props = _objectWithoutProperties(_ref, _excluded);

  let PrimaryLogo = () => /*#__PURE__*/React.createElement("img", {
    className: "logo-image primary",
    src: logoSrc,
    alt: "logo"
  }); // let SecondaryLogo = <img className="logo-image secondary" src={reverseLogoSrc} alt="logo" />;


  if (logoComponent) {
    PrimaryLogo = logoComponent;
  } // Mobile navigator logic


  const [isOpen, setIsOpen] = useState(false); // Override hamburger click

  const handleHamburgerClick = useCallback(() => {
    const newValue = !isOpen;
    setIsOpen(newValue);
    onHamburgerClick(newValue);
  }, [isOpen, onHamburgerClick]);
  return /*#__PURE__*/React.createElement("div", {
    className: `noku-library-navigator ${isOpen ? 'open' : ''} ${withMobileMenu ? 'with-mobile-menu' : ''}`
  }, /*#__PURE__*/React.createElement("div", {
    className: `hamburger ${showHamburger === true ? 'show' : ''} ${showHamburger === false ? 'hide' : ''}`,
    onClick: handleHamburgerClick
  }, /*#__PURE__*/React.createElement("div", null), /*#__PURE__*/React.createElement("div", null), /*#__PURE__*/React.createElement("div", null)), !hideLogo ? /*#__PURE__*/React.createElement("div", {
    className: "logo"
  }, /*#__PURE__*/React.createElement(Link, {
    className: "logo-link",
    to: "/"
  }, /*#__PURE__*/React.createElement(PrimaryLogo, null))) : /*#__PURE__*/React.createElement(React.Fragment, null), /*#__PURE__*/React.createElement("div", {
    className: `links ${linksClassName}`
  }, items.map((item = {}, i) => /*#__PURE__*/React.createElement(MenuItem, _extends({
    key: `navigator-menu-item-${i}`
  }, item)))), /*#__PURE__*/React.createElement("div", {
    className: "right-slot"
  }, rightSlot));
};

export default MainNavigator;