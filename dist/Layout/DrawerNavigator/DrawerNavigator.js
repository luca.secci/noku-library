function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { forwardRef, useMemo } from 'react';
import { NavLink } from 'react-router-dom';
import { Avatar, Divider, Drawer, List, ListItem } from '@material-ui/core';
import getUserAvatar from '../../utils/getUserAvatar';
/*
Example menu item: {
  destination: "/home",
  title: "Homepage",
  linkType: "internal",
  onItemClick: () => console.log("hello"),
  exactMatch: false
}
*/

const MenuItem = ({
  destination,
  title,
  linkType,
  onItemClick,
  exactMatch = true
}) => {
  const LinkComponent = useMemo(() => {
    var _linkType$toLowerCase;

    if ((linkType === null || linkType === void 0 ? void 0 : (_linkType$toLowerCase = linkType.toLowerCase) === null || _linkType$toLowerCase === void 0 ? void 0 : _linkType$toLowerCase.call(linkType)) === 'internal') {
      return /*#__PURE__*/forwardRef((linkProps, ref) => /*#__PURE__*/React.createElement(NavLink, _extends({
        ref: ref,
        activeClassName: "active",
        to: destination,
        exact: exactMatch
      }, linkProps)));
    } else {
      return /*#__PURE__*/forwardRef((linkProps, ref) =>
      /*#__PURE__*/
      // Disabled because we pass children as props
      // eslint-disable-next-line jsx-a11y/anchor-has-content
      React.createElement("a", _extends({
        ref: ref,
        href: destination
      }, linkProps)));
    }
  }, [destination, exactMatch, linkType]);
  return /*#__PURE__*/React.createElement(ListItem, {
    classes: {
      root: `menu-item link`
    },
    button: true,
    onClick: onItemClick,
    component: LinkComponent
  }, title);
};

export const DrawerNavigator = ({
  accountId,
  isOpen = false,
  onDrawerClose,
  accountAvatar = "",
  accountFullName = "",
  accountEmail = "",
  drawerRootClassName = "",
  drawerContentClassName = "",
  appMenuItems = [],
  otherMenuItems = [],
  onAccountInfoClick = () => {},
  accountsUrl = "#",
  footer = /*#__PURE__*/React.createElement(React.Fragment, null),
  variant = "temporary"
}) => /*#__PURE__*/React.createElement(Drawer, {
  className: `noku-library-drawer ${drawerRootClassName}`,
  open: isOpen,
  onClose: onDrawerClose,
  transitionDuration: 250,
  PaperProps: {
    className: `content ${drawerContentClassName}`
  },
  variant: variant
}, /*#__PURE__*/React.createElement("div", {
  className: "upper-content"
}, /*#__PURE__*/React.createElement("a", {
  className: "account-avatar-wrapper",
  href: accountsUrl,
  onClick: onAccountInfoClick
}, /*#__PURE__*/React.createElement(Avatar, {
  src: getUserAvatar(accountAvatar, accountId),
  className: "avatar"
})), /*#__PURE__*/React.createElement("a", {
  className: "user-info",
  href: accountsUrl,
  onClick: onAccountInfoClick
}, accountFullName ? /*#__PURE__*/React.createElement("div", {
  className: "full-name"
}, accountFullName) : /*#__PURE__*/React.createElement(React.Fragment, null), accountEmail ? /*#__PURE__*/React.createElement("div", {
  className: "email"
}, accountEmail) : /*#__PURE__*/React.createElement(React.Fragment, null)), (appMenuItems === null || appMenuItems === void 0 ? void 0 : appMenuItems.length) > 0 ? /*#__PURE__*/React.createElement(List, {
  className: "app-items"
}, appMenuItems.map(item => /*#__PURE__*/React.createElement(MenuItem, _extends({
  key: item.title
}, item)))) : /*#__PURE__*/React.createElement(React.Fragment, null), (appMenuItems === null || appMenuItems === void 0 ? void 0 : appMenuItems.length) > 0 && (otherMenuItems === null || otherMenuItems === void 0 ? void 0 : otherMenuItems.length) > 0 ? /*#__PURE__*/React.createElement("div", {
  className: "divider"
}, /*#__PURE__*/React.createElement(Divider, null)) : /*#__PURE__*/React.createElement(React.Fragment, null), (otherMenuItems === null || otherMenuItems === void 0 ? void 0 : otherMenuItems.length) > 0 ? /*#__PURE__*/React.createElement(List, {
  className: "other-menu-items"
}, otherMenuItems.map(item => /*#__PURE__*/React.createElement(MenuItem, _extends({
  key: item.title
}, item)))) : /*#__PURE__*/React.createElement(React.Fragment, null)), /*#__PURE__*/React.createElement("div", {
  className: "footer"
}, footer));
export default DrawerNavigator;