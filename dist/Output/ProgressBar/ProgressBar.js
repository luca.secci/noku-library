import React from 'react';
/**
 * @prop {number} progressPercent this is the percent (so from 0 to 100)
 * @prop {string} backgroundClassName additional background class
 * @prop {string} foregroundClassName additional foreground (this is the colored bar) class
 */

const ProgressBar = ({
  progressPercent,
  backgroundClassName,
  foregroundClassName
}) => /*#__PURE__*/React.createElement("div", {
  className: `noku-library-progress-bar background ${backgroundClassName}`
}, /*#__PURE__*/React.createElement("div", {
  style: {
    width: `${progressPercent}%`
  },
  className: `foreground ${foregroundClassName}`
}));

export default ProgressBar;