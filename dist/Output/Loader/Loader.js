import React from 'react';
import classes from './Loader.module.scss';
/**
 * @param {*} props
 */

export const Loader = props => /*#__PURE__*/React.createElement("div", {
  className: `${classes.loaderLayer} ${props.loaderLayerClassName}`
}, /*#__PURE__*/React.createElement("svg", {
  className: `${classes.loaderNoku} ${props.svgClassName}`,
  version: "1.1",
  id: "Layer_2",
  xmlns: "http://www.w3.org/2000/svg",
  xlink: "http://www.w3.org/1999/xlink",
  x: "0px",
  y: "0px",
  viewBox: "0 0 41 44",
  space: "preserve"
}, /*#__PURE__*/React.createElement("g", {
  id: "Layer_1",
  className: "layer"
}, /*#__PURE__*/React.createElement("path", {
  id: "Shape_5_",
  className: "st0",
  d: "M31.6,27.2c0,8.4-6.8,15.2-15.2,15.2S1.2,35.6,1.2,27.2S8,12,16.4,12h0.1C24.9,12,31.6,18.8,31.6,27.2z"
})), /*#__PURE__*/React.createElement("g", {
  id: "Layer_3",
  className: "layer"
}, /*#__PURE__*/React.createElement("path", {
  id: "Shape_4_",
  className: "st0",
  d: "M39.8,9.2c0,2.8-2.3,5-5,5s-5-2.3-5-5s2.3-5,5-5l0,0C37.5,4.2,39.8,6.5,39.8,9.2z"
})), /*#__PURE__*/React.createElement("line", {
  className: "line st1",
  x1: "31.1",
  y1: "12.5",
  x2: "26.8",
  y2: "16.8"
})));
export default Loader;