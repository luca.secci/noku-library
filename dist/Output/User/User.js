import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Avatar } from "@material-ui/core";
import React, { useMemo } from "react";
import { useTranslation } from "react-i18next"; // import defaultAvatar from "../../assets/images/avatar.svg";

import getUserAvatar from "../../utils/getUserAvatar"; // Avatar, name, email kyclevel, kycimage, baseAccountsUrl

export const User = ({
  userId = "",
  nickname,
  email,
  kycLevel = 1,
  avatar,
  accountsUrl = "",
  logoutUrl = "",
  accountDetailsUrl = "",
  accountLanguageUrl = "",
  upgradeKYCLevelUrl = "",
  fontAwesomeFamily = "fas",
  showKYC = true,
  showAccountDetails = true,
  showLogout = true,
  showLanguage = false,
  handleLogout = null
}) => {
  const {
    t
  } = useTranslation('noku-library');

  if (accountsUrl) {
    if (!accountDetailsUrl) {
      accountDetailsUrl = `${accountsUrl}/app/home/account_detail`;
    }

    if (!upgradeKYCLevelUrl) {
      upgradeKYCLevelUrl = `${accountsUrl}/app/home/kyc`;
    }

    if (!logoutUrl) {
      logoutUrl = `${accountsUrl}/logout`;
    }

    if (!accountLanguageUrl) {
      accountLanguageUrl = `${accountsUrl}/app/home/settings/language`;
    }
  }

  const renderLogout = useMemo(() => {
    if (handleLogout) {
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("span", {
        className: "menu-item logout",
        onClick: e => handleLogout(e)
      }, /*#__PURE__*/React.createElement(FontAwesomeIcon, {
        className: "icon",
        icon: [fontAwesomeFamily, "sign-out-alt"]
      }), /*#__PURE__*/React.createElement("span", {
        className: "text"
      }, t("logout"))));
    } else if (showLogout) {
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("a", {
        className: "menu-item logout",
        href: logoutUrl
      }, /*#__PURE__*/React.createElement(FontAwesomeIcon, {
        className: "icon",
        icon: [fontAwesomeFamily, "sign-out-alt"]
      }), /*#__PURE__*/React.createElement("span", {
        className: "text"
      }, t("logout"))));
    } else {
      return /*#__PURE__*/React.createElement(React.Fragment, null);
    }
  }, [handleLogout, logoutUrl, fontAwesomeFamily, showLogout]);
  const avatarSrc = useMemo(() => getUserAvatar(avatar, userId), [avatar, userId]);
  return /*#__PURE__*/React.createElement("div", {
    className: "noku-library-user"
  }, /*#__PURE__*/React.createElement(Avatar, {
    src: avatarSrc,
    className: "avatar"
  }), /*#__PURE__*/React.createElement("div", {
    className: "arrow"
  }, /*#__PURE__*/React.createElement(FontAwesomeIcon, {
    icon: "sort-down"
  })), /*#__PURE__*/React.createElement("div", {
    className: "dropdown"
  }, /*#__PURE__*/React.createElement("div", {
    className: "user-info"
  }, /*#__PURE__*/React.createElement(Avatar, {
    src: avatarSrc,
    className: "avatar"
  }), /*#__PURE__*/React.createElement("div", {
    className: "nickname-and-email"
  }, /*#__PURE__*/React.createElement("div", {
    className: "nickname"
  }, nickname), /*#__PURE__*/React.createElement("div", {
    className: "email"
  }, email)), showKYC ? /*#__PURE__*/React.createElement("img", {
    className: "kyc-image",
    src: `https://static.noku.io/assets/noku/kyc-levels/${kycLevel}.svg`,
    alt: "Kyc level"
  }) : /*#__PURE__*/React.createElement(React.Fragment, null)), showKYC ? /*#__PURE__*/React.createElement("a", {
    className: "kyc-block",
    href: upgradeKYCLevelUrl
  }, /*#__PURE__*/React.createElement("div", {
    className: "text"
  }, t("upgradeKYCLevel")), /*#__PURE__*/React.createElement("div", {
    className: "chip"
  }, t("level"), " ", kycLevel)) : /*#__PURE__*/React.createElement(React.Fragment, null), showAccountDetails ? /*#__PURE__*/React.createElement("a", {
    className: "menu-item account-details",
    href: accountDetailsUrl
  }, /*#__PURE__*/React.createElement(FontAwesomeIcon, {
    className: "icon",
    icon: [fontAwesomeFamily, "user-circle"]
  }), /*#__PURE__*/React.createElement("span", {
    className: "text"
  }, t("accountDetails"))) : /*#__PURE__*/React.createElement(React.Fragment, null), showLanguage ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: "divider"
  }), /*#__PURE__*/React.createElement("a", {
    className: "menu-item account-details",
    href: accountLanguageUrl
  }, /*#__PURE__*/React.createElement(FontAwesomeIcon, {
    className: "icon",
    icon: [fontAwesomeFamily, "globe"]
  }), /*#__PURE__*/React.createElement("span", {
    className: "text"
  }, t("language")))) : /*#__PURE__*/React.createElement(React.Fragment, null), /*#__PURE__*/React.createElement("div", {
    className: "divider"
  }), renderLogout));
};
export default User;