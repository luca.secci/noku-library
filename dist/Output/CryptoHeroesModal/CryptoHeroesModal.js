function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import Modal from '../Modal/Modal';
import { CryptoHeroesModalBody } from '../CryptoHeroesModalBody/CryptoHeroesModalBody';
import { CryptoHeroesModalFooter } from '../CryptoHeroesModalFooter/CryptoHeroesModalFooter';
import classes from './CryptoHeroesModal.module.scss';
import CurrentTokenStatus from '../../utils/cryptoHeroes/CurrentTokenStatus';
export const CryptoHeroesModal = ({
  id,
  metadata = {},
  isOpen = false,
  onClose = () => {},
  openMarketPlaceModal = () => {},
  withMarketplace = false,
  tokenStatus = CurrentTokenStatus.Balance
}) => {
  return /*#__PURE__*/React.createElement(Modal, {
    isOpen: isOpen,
    onClose: onClose // actions={Footer}
    ,
    bodyClassName: classes.body,
    showCancel: false,
    modalClassName: classes.modal,
    actionClassName: classes.actions,
    showCloseIcon: true,
    externalCloseIconButton: true // closeIconButtonClassName={classes.closeIcon}
    ,
    headerClassName: classes.header,
    titleClassName: classes.title,
    paperClassName: classes.paper,
    dividerClassName: classes.divider,
    scroll: "body"
  }, /*#__PURE__*/React.createElement(CryptoHeroesModalBody, _extends({
    id: id,
    openMarketPlaceModal: openMarketPlaceModal,
    withMarketplace: withMarketplace,
    tokenStatus: tokenStatus
  }, metadata)), /*#__PURE__*/React.createElement(CryptoHeroesModalFooter, {
    id: id,
    metadata: metadata
  }));
};
export default CryptoHeroesModal;