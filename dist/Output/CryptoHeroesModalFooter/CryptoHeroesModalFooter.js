import React from 'react';
import { getCryptoHeroesCardNumber } from '../../utils/cryptoHeroes/getCardNumber';
import { getCryptoHeroesEditionYearFromMetadata } from '../../utils/cryptoHeroes/getEdition';
import { getCryptoHeroesRarityLabelFromMetadata } from '../../utils/cryptoHeroes/getRarityLabel';
import { getCryptoHeroesSeriesLabelFromMetadata } from '../../utils/cryptoHeroes/getSeriesLabel';
import classes from './CryptoHeroesModalFooter.module.scss';
export const CryptoHeroesModalFooter = ({
  id,
  metadata,
  footerClassName
}) => {
  var _getCryptoHeroesRarit, _getCryptoHeroesEditi, _getCryptoHeroesSerie;

  return /*#__PURE__*/React.createElement("div", {
    className: `${classes.footer} ${footerClassName}`
  }, /*#__PURE__*/React.createElement("div", {
    className: `${classes.row}`
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.column
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.firstRow
  }, (_getCryptoHeroesRarit = getCryptoHeroesRarityLabelFromMetadata(metadata)) !== null && _getCryptoHeroesRarit !== void 0 ? _getCryptoHeroesRarit : " "), /*#__PURE__*/React.createElement("div", {
    className: classes.secondRow
  }, "#", getCryptoHeroesCardNumber(id))), /*#__PURE__*/React.createElement("div", {
    className: classes.column
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.firstRow
  }, (_getCryptoHeroesEditi = getCryptoHeroesEditionYearFromMetadata(metadata)) !== null && _getCryptoHeroesEditi !== void 0 ? _getCryptoHeroesEditi : " "), /*#__PURE__*/React.createElement("div", {
    className: classes.secondRow
  }, (_getCryptoHeroesSerie = getCryptoHeroesSeriesLabelFromMetadata(metadata)) !== null && _getCryptoHeroesSerie !== void 0 ? _getCryptoHeroesSerie : " "))));
};
export default CryptoHeroesModalFooter;