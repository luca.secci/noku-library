import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconButton, Tooltip } from '@material-ui/core';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import Button from "../../Input/Button/Button";
import { CurrentTokenStatus } from '../../utils/cryptoHeroes/CurrentTokenStatus';
import classes from './CryptoHeroesModalBody.module.scss';
export const Asset = ({
  asset = "",
  multiplier = ""
}) => asset && multiplier ? /*#__PURE__*/React.createElement("div", {
  className: classes.asset
}, " ", asset, " x ", multiplier, " ") : /*#__PURE__*/React.createElement(React.Fragment, null);
export const CryptoHeroesModalBody = ({
  image = "",
  attributes = {},
  id = "",
  openMarketPlaceModal = () => {},
  withMarketplace = false,
  tokenStatus = CurrentTokenStatus.Balance
}) => {
  const {
    t
  } = useTranslation("noku-library");
  const MarketplaceButton = useMemo(() => {
    if (withMarketplace) {
      switch (tokenStatus) {
        case CurrentTokenStatus.Balance:
          return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
            className: classes.title
          }, t("chBuyModalTitle")), /*#__PURE__*/React.createElement("div", {
            className: classes.description
          }, t("chBuyModalDescription")), /*#__PURE__*/React.createElement(Button, {
            variant: "outlined",
            classes: {
              root: classes.cart,
              disabled: classes.disabled
            },
            size: "small",
            onClick: openMarketPlaceModal
          }, t("sellNow")));

        case CurrentTokenStatus.CryptoHeroes:
          return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
            className: classes.title
          }, t("chBuyModalTitle")), /*#__PURE__*/React.createElement("div", {
            className: classes.description
          }, t("chBuyModalDescription")), /*#__PURE__*/React.createElement(Tooltip, {
            arrow: true,
            placement: "bottom",
            title: t("cardDisabled")
          }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Button, {
            variant: "outlined",
            classes: {
              root: classes.cart,
              disabled: classes.disabled
            },
            disabled: true,
            size: "small"
          }, t("sellNow")))));

        case CurrentTokenStatus.Marketplace:
          return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
            className: classes.title
          }, t("chUnlockModalTitle")), /*#__PURE__*/React.createElement("div", {
            className: classes.description
          }, t("chUnlkockModalDescription")), /*#__PURE__*/React.createElement(Button, {
            variant: "outlined",
            size: "small",
            onClick: openMarketPlaceModal
          }, t("unlockNow")));

        default:
          break;
      }
    }

    return /*#__PURE__*/React.createElement(React.Fragment, null);
  }, [openMarketPlaceModal, t, tokenStatus, withMarketplace]);
  return /*#__PURE__*/React.createElement("div", {
    className: classes.contentWrapper
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.firstHalf
  }), /*#__PURE__*/React.createElement("div", {
    className: classes.content
  }, /*#__PURE__*/React.createElement("div", {
    className: `${classes.row} ${classes.id}`
  }, "ID CODE: ", id.split('@')[0]), /*#__PURE__*/React.createElement("img", {
    className: classes.tokenImage,
    src: image,
    alt: "",
    loading: "lazy"
  }), /*#__PURE__*/React.createElement("div", {
    className: classes.bodyInfo
  }, /*#__PURE__*/React.createElement("div", {
    className: `${classes.row}`
  }, /*#__PURE__*/React.createElement("div", {
    className: `${classes.label} ${classes.assetLabel}`
  }, (attributes !== null && attributes !== void 0 && attributes.A1 || attributes !== null && attributes !== void 0 && attributes.A2 || attributes !== null && attributes !== void 0 && attributes.A3) && attributes !== null && attributes !== void 0 && attributes.M ? "Asset" : " "), /*#__PURE__*/React.createElement("div", {
    className: classes.label
  }, attributes !== null && attributes !== void 0 && attributes.W ? `Weight ${attributes === null || attributes === void 0 ? void 0 : attributes.W}` : " ")), /*#__PURE__*/React.createElement("div", {
    className: `${classes.row} ${classes.assetRow}`
  }, (attributes !== null && attributes !== void 0 && attributes.A1 || attributes !== null && attributes !== void 0 && attributes.A2 || attributes !== null && attributes !== void 0 && attributes.A3) && attributes !== null && attributes !== void 0 && attributes.M ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Asset, {
    asset: attributes === null || attributes === void 0 ? void 0 : attributes.A1,
    multiplier: attributes === null || attributes === void 0 ? void 0 : attributes.M
  }), /*#__PURE__*/React.createElement(Asset, {
    asset: attributes === null || attributes === void 0 ? void 0 : attributes.A2,
    multiplier: attributes === null || attributes === void 0 ? void 0 : attributes.M
  }), /*#__PURE__*/React.createElement(Asset, {
    asset: attributes === null || attributes === void 0 ? void 0 : attributes.A3,
    multiplier: attributes === null || attributes === void 0 ? void 0 : attributes.M
  })) : /*#__PURE__*/React.createElement(React.Fragment, null)), MarketplaceButton)));
};
export default CryptoHeroesModalBody;