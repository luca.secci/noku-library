import React, { useCallback } from "react";
import { IconButton, Dialog, DialogActions, DialogContent, DialogTitle } from "@material-ui/core";
import Button from "../../Input/Button/Button";
import ProgressBar from "../../Output/ProgressBar/ProgressBar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
/**
 * @prop {boolean} isOpen If modal is open then it's true
 * @prop {boolean} isLoading If modal is loading some data then it's true
 * @prop {callback} onClose Called when modal is closed
 * @prop {string} title Custom title string
 * @prop {CSS} titleClassName The title additional classes, this is inside header
 * @prop {string} titleColor The title color
 * @prop {CSS} headerClassName The header additional classes
 * @prop {CSS} dividerClassName The divider additional classes
 * @prop {boolean} showActions Force actions rendering
 * @prop {JSX} actions Custom actions buttons or any valid JSX
 * @prop {CSS} actionClassName The footer additional classes
 * @prop {boolean} showCancel default = true Shows a "Cancel" button
 * @prop {boolean} disableCancel Should the cancel button be disabled
 * @prop {string} cancelText default = "Cancel" Custom optional text for cancel button
 * @prop {string} cancelButtonColor Custom color for the cancel button
 * @prop {boolean} showCloseIcon = false, if true shows a close icon
 * @prop {boolean} hasProgressBar If true shows a linear progress indicator, must specify progress porp
 * @prop {number} progressPercent Current percentage progress (int)
 * @prop {boolean} isFullScreen default = false If true the dialog will be full-screen
 * @prop {CSS} bodyClassName The body additional classes
 * @prop {CSS} actionWrapperClassName The actions wrapper classes
 * @prop {CSS} paperClassName The paper classes
 * @prop {CSS} containerClassName The container classes
 * @prop {boolean} removeNavigatorHeight If the modal should remove the appbar/navigator height
 * @prop {Function} onBackdropClick Called when user click on the backdrop
 * @prop {string} scroll Reference for scroll, can be "paper" or "body"
 * @prop {JSX} children Modal Body JSX
 */

export const Modal = ({
  modalClassName = "",
  isOpen = false,
  isLoading = false,
  onClose = () => {},
  title,
  titleColor = "",
  titleClassName = "",
  headerClassName = "",
  dividerClassName = "",
  showActions = true,
  actions = /*#__PURE__*/React.createElement(React.Fragment, null),
  actionClassName = "",
  actionsBackgroundColor = "",
  showCancel = true,
  disableCancel = false,
  cancelText = "Cancel",
  cancelButtonColor,
  showCloseIcon = false,
  disableCloseIcon = false,
  closeIconButtonClassName = "",
  closeIconClassName = "",
  externalCloseIconButton = false,
  hasProgressBar,
  progressPercent,
  isFullScreen = false,
  bodyClassName = "",
  actionWrapperClassName = "",
  paperClassName = "",
  removeNavigatorHeight = false,
  onBackdropClick,
  scroll = "paper",
  containerClassName = "",
  actionsSpacing = "flex-end",
  children
}) => {
  const handleClose = useCallback((event, reason) => {
    if (reason === "backdropClick" && onBackdropClick) {
      return onBackdropClick(event);
    }

    onClose(event);
  }, [onBackdropClick, onClose]);
  const header = title || showCloseIcon ? /*#__PURE__*/React.createElement(DialogTitle, {
    className: `header ${headerClassName}`
  }, /*#__PURE__*/React.createElement("div", {
    className: `title ${titleColor} ${titleClassName}`
  }, title), showCloseIcon ? /*#__PURE__*/React.createElement(IconButton, {
    disabled: disableCloseIcon,
    classes: {
      root: `close-icon-button ${externalCloseIconButton ? "external" : ""} ${closeIconButtonClassName}`,
      disabled: `disabled`
    },
    onClick: handleClose
  }, /*#__PURE__*/React.createElement(FontAwesomeIcon, {
    className: `close-icon ${closeIconClassName}`,
    icon: "times"
  })) : /*#__PURE__*/React.createElement(React.Fragment, null)) : /*#__PURE__*/React.createElement(React.Fragment, null);
  const divider = hasProgressBar ? /*#__PURE__*/React.createElement(ProgressBar, {
    progressPercent: progressPercent
  }) : /*#__PURE__*/React.createElement("div", {
    className: `divider ${dividerClassName}`
  });
  const body = /*#__PURE__*/React.createElement(DialogContent, {
    className: `body ${bodyClassName}`
  }, /*#__PURE__*/React.createElement("div", {
    className: `loader ${isLoading ? "active" : ""}`
  }, /*#__PURE__*/React.createElement("div", {
    className: "message"
  }, "Loading...")), children);
  const modalActions = showActions ? /*#__PURE__*/React.createElement(DialogActions, {
    className: `actions ${actionsBackgroundColor} ${actionsSpacing} ${actionClassName}`
  }, showCancel ? /*#__PURE__*/React.createElement(Button, {
    className: "cancel-button",
    variant: "flat",
    color: (cancelButtonColor !== null && cancelButtonColor !== void 0 ? cancelButtonColor : actionsBackgroundColor === 'primary') ? 'reverse-primary' : undefined,
    onClick: handleClose,
    disabled: disableCancel
  }, cancelText) : /*#__PURE__*/React.createElement(React.Fragment, null), /*#__PURE__*/React.createElement("div", {
    className: actionWrapperClassName
  }, actions)) : /*#__PURE__*/React.createElement(React.Fragment, null);
  return /*#__PURE__*/React.createElement(Dialog // onBackdropClick={onBackdropClick ?? onClose}
  , {
    open: isOpen,
    onClose: handleClose,
    fullScreen: isFullScreen // className={`noku-library-modal ${removeNavigatorHeight ? "boxed" : ""} ${modalClassName}`}
    ,
    maxWidth: "xl",
    scroll: scroll // PaperProps={{className: `${scroll === "body" ? "overflow-unset" : ""} ${paperClassName}`}}
    ,
    classes: {
      root: `noku-library-modal ${removeNavigatorHeight ? "boxed" : ""} ${modalClassName}`,
      paper: `paper ${paperClassName}`,
      container: containerClassName,
      paperScrollBody: "overflow-unset"
    },
    BackdropProps: {
      className: "backdrop"
    }
  }, header, divider, body, modalActions);
};
export default Modal;