import { DerivedAccountListStorage } from "../customStorage";
const baseKey = "DerivedAccountList";

const getStorageKey = (rootAddress, derivationPath) => `${baseKey}-${derivationPath.toLocaleLowerCase()}-${rootAddress.toLocaleLowerCase()}`;
/**
 * 
 * @param {string} rootAddress 
 * @param {string} derivationPath 
 * @returns {Promise<string[]>}
 */


export const getCurrentDerivedAccountList = async (rootAddress, derivationPath) => {
  var _await$DerivedAccount;

  if (rootAddress === undefined || rootAddress === null) {
    throw new Error('Missing root address');
  }

  if (derivationPath === undefined || derivationPath === null) {
    throw new Error('Missing derivation path');
  }

  const accountList = (_await$DerivedAccount = await DerivedAccountListStorage.getItem(getStorageKey(rootAddress, derivationPath))) !== null && _await$DerivedAccount !== void 0 ? _await$DerivedAccount : [];
  return accountList;
};
/**
 * 
 * @param {string} rootAddress 
 * @param {string} derivationPath 
 * @param {number} accountIndex 
 */

export const getDerivedAccount = async (rootAddress, derivationPath, accountIndex) => {
  if (rootAddress === undefined || rootAddress === null) {
    throw new Error('Missing root address');
  }

  if (derivationPath === undefined || derivationPath === null) {
    throw new Error('Missing derivation path');
  }

  if (accountIndex === undefined || accountIndex === null) {
    throw new Error('Missing account index');
  }

  const accountList = await getCurrentDerivedAccountList(rootAddress, derivationPath);
  return accountList[accountIndex];
};
/**
 * 
 * @param {string} rootAddress 
 * @param {string} derivationPath 
 * @param {number} accountIndex 
 * @param {string} accountAddress 
 * @returns 
 */

export const saveDerivedAccount = async (rootAddress, derivationPath, accountIndex, accountAddress) => {
  if (rootAddress === undefined || rootAddress === null) {
    throw new Error('Missing root address');
  }

  if (derivationPath === undefined || derivationPath === null) {
    throw new Error('Missing derivation path');
  }

  if (accountIndex === undefined || accountIndex === null) {
    throw new Error('Missing account index');
  }

  if (accountAddress === undefined || accountAddress === null) {
    throw new Error('Missing account address');
  }

  const currentAccountList = await getCurrentDerivedAccountList(rootAddress, derivationPath);
  currentAccountList[accountIndex] = accountAddress;
  return await DerivedAccountListStorage.setItem(getStorageKey(rootAddress, derivationPath), currentAccountList);
};