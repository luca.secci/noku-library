import { useEffect, useState } from "react";
/**
 * Returns {width, height} of the window
 */

const useWindowSizes = () => {
  const [windowSizes, setWindowSizes] = useState({
    width: 0,
    height: 0
  });
  useEffect(() => {
    // Set initial value
    const width = window.innerWidth;
    const height = window.innerHeight;
    setWindowSizes({
      width,
      height
    }); // Listen resizes

    window.addEventListener("resize", () => {
      const width = window.innerWidth;
      const height = window.innerHeight;
      setWindowSizes({
        width,
        height
      });
    });
    return () => {
      window.removeEventListener("resize", () => {});
    };
  }, []);
  return windowSizes;
};

export { useWindowSizes };
export default useWindowSizes;