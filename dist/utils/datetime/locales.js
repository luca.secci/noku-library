function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { addMinutes, format } from "date-fns";
import { enGB as en, it, ko } from "date-fns/locale";
const locales = new Map(Object.entries({
  en,
  it,
  ko
}));
export const getDateFnsLocaleFromKey = (searchKey = "en") => locales.get(searchKey);
export const formatWithLocale = (date, formatString, searchKey = "en", additionalOptions = {}) => {
  if (!formatString || !date) {
    throw new Error("Missing date or formatString");
  }

  const locale = getDateFnsLocaleFromKey(searchKey);
  return format(date, formatString, _objectSpread({
    locale
  }, additionalOptions));
};
export const formatUTCWithLocale = (date, formatString, searchKey = "en", additionalOptions = {}) => {
  return formatWithLocale(addMinutes(date, date.getTimezoneOffset()), formatString, searchKey, additionalOptions);
};