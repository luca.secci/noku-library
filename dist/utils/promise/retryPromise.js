import asyncSleep from "../asyncSleep";
/**
 * Retries a promise until it resolves or reaches the max retries.
 * The waiting time is: (2^n) + backoffTime
 * @param {Promise<any>} promise 
 * @param {number} maxRetries 
 * @param {number} backoffTime 
 */

export const retryPromise = async (promise, maxRetries = 5, backoffTime = 500) => {
  let resolved = false;
  let currentRetry = 0;
  let result;

  while (!resolved) {
    try {
      result = await promise;
      resolved = true;
    } catch (error) {
      console.error(error);

      if (currentRetry > maxRetries) {
        throw new Error('Exceeded max retries');
      }

      currentRetry++;
      const waitTime = Math.pow(2, currentRetry) + backoffTime;
      await asyncSleep(waitTime);
    }
  }

  return result;
};