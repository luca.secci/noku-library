// https://github.com/sindresorhus/p-all/blob/main/index.js
export const pAll = async (iterable, options) => {
  const {
    pMap
  } = await import("./p-map");
  return pMap(iterable, element => element(), options);
};
export default pAll;