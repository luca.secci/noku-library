/**
 * Elide part of the text string
 * @param {String} text :   Text to elide
 * @param {Number} from :   (default=8) The char index from which to start the elision
 * @param {Number} to   :   (default=text.length-8) The char index to end the elision
 * @return {String}     :   Text with an elision (example: abc...yxz)
 */
export const textElision = (text, from = 8, to) => {
  let result = "";

  if (text && typeof text === "string") {
    if (!to) {
      to = text.length - 8;
    }

    result = text.substring(0, from) + "..." + text.substring(to, text.length);
  }

  return result;
};
export default textElision;