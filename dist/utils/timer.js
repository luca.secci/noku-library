/**
 * Timer class to measure impact of some code.
 * 
 * Uses performance.now() for higher precision.
 * 
 * Reference: https://developer.mozilla.org/en-US/docs/Web/API/Performance/now
 */
export class Timer {
  constructor(name) {
    this.name = name;
    this.startTime = null;
    this.endTime = null;
  }

  start() {
    this.startTime = performance.now();
  }

  end() {
    if (!this.startTime) {
      throw new Error('Called end() before start()');
    }

    this.endTime = performance.now();
  }

  get differenceMilliseconds() {
    if (!this.startTime) {
      throw new Error('Called printMilliseconds() before start()');
    }

    if (!this.endTime) {
      throw new Error('Called printMilliseconds() before end()');
    }

    return this.endTime - this.startTime;
  }

  get prettyDifference() {
    return `${this.name} took: ${this.differenceMilliseconds} milliseconds`;
  }

}
export default Timer;