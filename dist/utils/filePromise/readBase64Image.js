import pTimeout from 'p-timeout';
/**
 * Reads a base64 image and returns an Image, rejects if file could not be read.
 * @param {string} base64
 * @param {Number} timeout
 * @returns {Promise<HTMLImageElement>}
 */

export const readBase64Image = (base64, timeout = 500) => {
  const image = new Image();
  image.src = base64;
  const loadPromise = new Promise((resolve, reject) => {
    image.addEventListener("load", () => resolve(image));
  });
  return pTimeout(loadPromise, timeout);
};