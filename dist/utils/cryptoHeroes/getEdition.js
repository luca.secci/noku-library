/**
 * Get the year from the edition
 * @param {number} edition The edition number
 */
export const getCryptoHeroesEditionYear = (edition = 0) => edition + 2020;
/**
 * Gets the edition year from the token metadata
 * @param {Object} metadata The token metadata
 */

export const getCryptoHeroesEditionYearFromMetadata = (metadata = {}) => {
  var _metadata$attributes$, _metadata$attributes;

  let edition = (_metadata$attributes$ = metadata === null || metadata === void 0 ? void 0 : (_metadata$attributes = metadata.attributes) === null || _metadata$attributes === void 0 ? void 0 : _metadata$attributes.Q) !== null && _metadata$attributes$ !== void 0 ? _metadata$attributes$ : 0;

  if (typeof edition === "string") {
    edition = parseInt(edition);
  }

  return getCryptoHeroesEditionYear(edition);
};