/**
 * Gets the card number from the token ID
 * Example IDs: 8000001000000251, 80000004000000002, 80000204000000026
 * Example Output: 251, 2, 26
 * @param {string} id The token ID with the address, separated by `@`
 */
export const getCryptoHeroesCardNumber = (idAndAddress = "") => {
  const [id] = idAndAddress.split("@");
  const cardNumber = parseInt(id.slice(8));

  if (isNaN(cardNumber)) {
    return "-";
  }

  return cardNumber;
};