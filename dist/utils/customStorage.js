function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { createInstance } from "localforage";
const defaultOptions = {
  name: "noku-library",
  storeName: "storage",
  description: "Storage for noku applications"
};
/**
 * Creates a custom storage with some predefined options.
 * @param {LocalForageOptions} options 
 * @returns {LocalForage}
 */

export const makeStorage = options => createInstance(_objectSpread(_objectSpread({}, defaultOptions), options));
export const CustomStorage = createInstance(_objectSpread({}, defaultOptions));
export const NFTCacheStorage = createInstance(_objectSpread(_objectSpread({}, defaultOptions), {}, {
  storeName: "nft-cache-storage",
  description: "Storage for NFT metadata"
}));
export const ExternalWalletStorage = createInstance(_objectSpread(_objectSpread({}, defaultOptions), {}, {
  storeName: "external-wallet-storage",
  description: "Storage for external wallets (web3 provider like Metamask/Ledger/Trezor/etc.)"
}));
export const DerivedAccountListStorage = createInstance(_objectSpread(_objectSpread({}, defaultOptions), {}, {
  storeName: "derived-account-list-storage",
  description: "Storage for derived accounts"
}));