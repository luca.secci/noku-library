export const asyncSleep = time => new Promise(resolve => setTimeout(resolve, time));
export default asyncSleep;